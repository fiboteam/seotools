using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using DAO.Interface;


namespace DAO
{
    public static class DataAccess
    {
		//private static readonly string connectionStringName = ConfigurationManager.AppSettings.Get("ConnectionStringName");
		private static readonly string connectionStringName = Properties.Settings.Default.ConnectionStringName;

		/*
		 * Không cần ép kiểu nữa
		 * private static readonly IDaoFactory factory = (IDaoFactory)DaoFactories.GetFactory(connectionStringName);
		 */
		private static readonly IDaoFactory factory = DaoFactories.GetFactory(connectionStringName);

		public static ISEODomain SEODomainDao { get { return factory.SEODomainDao; } }
		public static ILogRefeshSEO LogRefreshSEODao { get { return factory.RefreshSEODao; } }
		public static ISEOKeyword SEOKeywordDao { get { return factory.SEOKeywordDao; } }

		public static IBlockInsertKeywordPlanner BlockInsertKeywordPlannerDao { get { return factory.BlockInsertKeywordPlannerDao; } }
		public static IKeywordPlanner KeywordPlannerDao { get { return factory.KeywordPlannerDao; } }
		public static IDetailBlockInsertPlanner DetailBlockInsertPlannerDao { get { return factory.DetailBlockInsertPlannerDao; } }
		public static IUser UserDao { get { return factory.UserDao; } }
    }
}