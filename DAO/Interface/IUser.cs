using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface IUser : IDao<User>
    {
		User CheckLogin(string username, string password);
		User FindUserBy(string username);
		User GetUserByUserApiId(string userid);
    }
}
