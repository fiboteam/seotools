using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface ISEODomain : IDao<SEODomain>
    {
		IList<SEODomain> GetList(long SEODomainIdOnPartner, long sEODomainIdOnPartner, string domainName, int totalSEOKeyword, int totalSEOKeywordOnTop, string linkQueryKeyword, short sEODomainStatus, string sEODomainStatusStr, int shortCreatedDate, string note);
        int GetTotalPage(long pageSize, long sEODomainIdOnPartner,string domainName,int totalSEOKeyword,int totalSEOKeywordOnTop,string linkQueryKeyword,short sEODomainStatus,string sEODomainStatusStr,int shortCreatedDate,string note);
		bool RefreshData(SEODomain domain);
		string GetRecommend(string domainName, long userid);
    }
}
