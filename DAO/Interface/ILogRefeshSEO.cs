using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAO.Interface
{
	public interface ILogRefeshSEO : IDao<LogRefreshSEO>
    {
		IList<LogRefreshSEO> GetList(long pageSize, long pageNum, long objectID, short statusRefesh, string linkGetData, string dataRefesh, int shortCreatedDate, string note);
        int GetTotalPage(long pageSize, long objectID,short statusRefesh,string linkGetData,string dataRefesh,int shortCreatedDate,string note);
    }
}
