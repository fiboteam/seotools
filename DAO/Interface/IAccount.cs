using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface IAccount : IDao<Account>
    {
		IList<Account> GetList(long pageSize, long pageNum, string userName,string userPass,string fullName,string email,string phone,string address,short status,int shortCreatedDate,string note);
        int GetTotalPage(long pageSize, string userName,string userPass,string fullName,string email,string phone,string address,short status,int shortCreatedDate,string note);
    }
}
