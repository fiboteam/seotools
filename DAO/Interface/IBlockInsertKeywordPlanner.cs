using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface IBlockInsertKeywordPlanner : IDao<BlockInsertKeywordPlanner>
    {
		IList<BlockInsertKeywordPlanner> GetList(long pageSize, long pageNum, string blockName,string note,string remark,int shortCreatedDate);
		/// <summary>
		/// Insert block
		/// </summary>
		/// <param name="blockName">Name of block</param>
		/// <returns>success or fail</returns>
		long InsertWithImport(string blockName,long userid);
    }
}
