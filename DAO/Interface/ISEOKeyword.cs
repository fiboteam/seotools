using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface ISEOKeyword : IDao<SEOKeyword>
    {
		IList<SEOKeyword> GetList(long pageSize, long pageNum, long sEOKeywordIdOnPartner,long sEODomainId,string sEOKeywordName,int pricePerWeekOnPartner,int price,int rankOnGoogle,string linkCheck,short sEOKeywordStatus,string sEOKeywordStatusStr,int shortCreatedDate,string note);
        int GetTotalPage(long pageSize, long sEOKeywordIdOnPartner,long sEODomainId,string sEOKeywordName,int pricePerWeekOnPartner,int price,int rankOnGoogle,string linkCheck,short sEOKeywordStatus,string sEOKeywordStatusStr,int shortCreatedDate,string note);
		bool RefreshData(SEOKeyword keyword);

		List<SEOKeyword> GetAllKeywordAndDomainRelated();

		List<SEOKeyword> GetAllKeywordWithFilter(long domainid, long keywordid);

		int GetRank(string domainName, string keywordName,long userid);
	}
}
