using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface IDetailBlockInsertPlanner : IDao<DetailBlockInsertPlanner>
    {
		IList<DetailBlockInsertPlanner> GetList(long pageSize, long pageNum, long blockID,long keywordPlannerId,string note,string remark,int shortCreatedDate);
    }
}
