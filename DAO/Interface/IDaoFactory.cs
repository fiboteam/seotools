using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface IDaoFactory
    {
		ISEODomain SEODomainDao { get; }
		ILogRefeshSEO RefreshSEODao { get; }
		ISEOKeyword SEOKeywordDao { get; }
		IBlockInsertKeywordPlanner BlockInsertKeywordPlannerDao { get; }
		IKeywordPlanner KeywordPlannerDao { get; }
		IDetailBlockInsertPlanner DetailBlockInsertPlannerDao { get; }
		IUser UserDao { get; }
    }
}