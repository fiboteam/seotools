using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAO.Interface
{
    public interface IUser_SEODomain : IDao<User_SEODomain>
    {
		IList<User_SEODomain> GetList(long pageSize, long pageNum, long userId,long sEODomainId,short status,int shortCreatedDate,string note);
        int GetTotalPage(long pageSize, long userId,long sEODomainId,short status,int shortCreatedDate,string note);
    }
}
