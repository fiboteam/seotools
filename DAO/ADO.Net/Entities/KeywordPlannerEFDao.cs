﻿using DAO.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.ADO.Net.Entities
{
	public class KeywordPlannerEFDao
	{

		public Entity.tblKeywordPlanner GetSingle(long id)
		{
			try
			{
				using(SEODBEntities db=new SEODBEntities())
				{
					//return db.tblKeywordPlanners.Where(kw => kw.KeywordPlannerId == id).First();
					return (from kw in db.tblKeywordPlanners where kw.KeywordPlannerId == id select kw).First();
				}
			}
			catch
			{
				return null;
			}
		}

		public bool UpdateByAdmin(long loginId,long id, long chargeForMaintenance, long setUpCost, string note, bool isPublic)
		{
			try
			{
				using (SEODBEntities db = new SEODBEntities())
				{
					var key = db.tblKeywordPlanners.Where(kw => kw.KeywordPlannerId == id).First();
					if(key!=null)
					{
						key.ChargesForMaintenance = chargeForMaintenance;
						key.SetupCost = setUpCost;
						key.Note = note;
						key.IsPublicToPartner = (byte)(isPublic==true ? 1:0);
						key.UpdatedDate = DateTime.Now;
						key.UserId = loginId;

						db.SaveChanges();
					}
					else
					{
						return false;
					}

					return true;
				}
			}
			catch
			{
				return false;
			}
		}
	}
}
