﻿using DAO.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityFramework.BulkInsert.Extensions;
using System.Transactions;

namespace DAO.ADO.Net.Entities
{
	public class SpecialKeywordEFDao
	{
		/// <summary>
		/// Chưa có thì insert
		/// có rồi thì update
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public bool AddSpecialKeyword(List<tblSpecialKeyword> list)
		{
			try
			{
				using (SEODBEntities entities = new SEODBEntities())
				{
					using (var transactionScope = new TransactionScope())
					{
						for (int i = 0; i < list.Count; i++)
						{
							var item = list[i];
							
							var spec = entities.tblSpecialKeywords.Where(kw => kw.KeywordName1.ToUpper() == item.KeywordName1.ToUpper() || kw.KeywordName2.ToUpper() == item.KeywordName2.ToUpper()).ToList();
							if(spec!=null)
							{
								spec.ForEach(kw=>
								{
									kw.GroupName=item.GroupName;
									kw.SubName = item.SubName;
									kw.CompetitionIndex = item.CompetitionIndex;
								});
								entities.SaveChanges();
								i--;
								list.Remove(item);
							}
						}
						
						transactionScope.Complete();
					}

					if(list.Count>0)
					{
						using (var transactionScope = new TransactionScope())
						{
							entities.BulkInsert(list);
							entities.SaveChanges();
							transactionScope.Complete();
						}
					}
					
				}
				
				return true;
			}
			catch
			{
				return false;
			}
		}

		public List<tblSpecialKeyword> GetListSpecialKeyword(string keyword)
		{
			try
			{
				using (SEODBEntities entities = new SEODBEntities())
				{
					if (string.IsNullOrEmpty(keyword))
					{
						return entities.tblSpecialKeywords.OrderByDescending(kw => kw.UpdatedDate).ToList();
					}
					else
					{
						return entities.tblSpecialKeywords.Where(kw => kw.KeywordName1.ToUpper().Contains(keyword.ToUpper()) || kw.KeywordName2.ToUpper().Contains(keyword.ToUpper())).Select(kw => kw).OrderByDescending(kw => kw.UpdatedDate).ToList();
					}
				}
			}
			catch
			{
				return null;
			}
		}

		public tblSpecialKeyword GetSingle(long kewordId)
		{
			try
			{
				using (SEODBEntities entities = new SEODBEntities())
				{
					return entities.tblSpecialKeywords.Where(kw => kw.SpecialKeywordId == kewordId).FirstOrDefault();
				}
				
			}
			catch
			{
				return null;
			}
		}

		public bool Update(tblSpecialKeyword keywordUpdate)
		{
			try
			{
				using (SEODBEntities entities = new SEODBEntities())
				{
					tblSpecialKeyword kword = (from c in entities.tblSpecialKeywords
											   where c.SpecialKeywordId == keywordUpdate.SpecialKeywordId
										 select c).FirstOrDefault();

					if (kword != null)
					{
						kword.GroupName = keywordUpdate.GroupName;
						kword.SubName = keywordUpdate.SubName;
						kword.CompetitionIndex = keywordUpdate.CompetitionIndex;
						kword.KeywordName1 = keywordUpdate.KeywordName1;
						kword.KeywordName2 = keywordUpdate.KeywordName2;
						kword.Status = keywordUpdate.Status;
						kword.UpdatedDate = keywordUpdate.UpdatedDate;
						kword.UserId = keywordUpdate.UserId;

						entities.SaveChanges();
						return true;
					}
					else
					{
						return false;
					}
				}
				
				//var kword = _seodb.tblSpecialKeywords.Single(kw => kw.SpecialKeywordId == keywordUpdate.SpecialKeywordId);
				//if (kword != null)
				//{
				//	kword.GroupName = keywordUpdate.GroupName;
				//	kword.SubName = keywordUpdate.SubName;
				//	kword.CompetitionIndex = keywordUpdate.CompetitionIndex;
				//	kword.KeywordName1 = keywordUpdate.KeywordName1;
				//	kword.KeywordName2 = keywordUpdate.KeywordName2;
				//	kword.Status = keywordUpdate.Status;

				//	_seodb.SaveChanges();
				//	return true;
				//}

			}
			catch
			{
				return false;
			}
		}

		public tblSpecialKeyword GetSpecialKeywordContainsValue(string keyword)
		{
			try
			{
				using (SEODBEntities entities = new SEODBEntities())
				{
					return entities.tblSpecialKeywords.Where(kw => keyword.ToUpper().Contains(kw.KeywordName1.ToUpper()) || keyword.ToUpper().Contains(kw.KeywordName2.ToUpper())).OrderByDescending(kw=>kw.SpecialKeywordId).First();
				}
			}
			catch
			{
				return null;
			}
		}
	}
}
