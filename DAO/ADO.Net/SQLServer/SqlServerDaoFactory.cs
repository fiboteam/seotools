using DAO.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.AdoNet.SqlServer
{
	public class SqlServerDaoFactory : IDaoFactory
	{
		public ISEODomain SEODomainDao
		{
			get { return new SqlSEODomainDao(); }
		}


		public ILogRefeshSEO RefreshSEODao
		{
			get { return new SqlLogRefeshSEODao(); }
		}


		public ISEOKeyword SEOKeywordDao
		{
			get { return new SqlSEOKeywordDao(); }
		}


		public IBlockInsertKeywordPlanner BlockInsertKeywordPlannerDao
		{
			get { return new SqlBlockInsertKeywordPlannerDao(); }
		}

		public IKeywordPlanner KeywordPlannerDao
		{
			get { return new SqlKeywordPlannerDao(); }
		}

		public IDetailBlockInsertPlanner DetailBlockInsertPlannerDao
		{
			get { return new SqlDetailBlockInsertPlannerDao(); }
		}


		public IUser UserDao
		{
			get { return new SqlUserDao(); }
		}
	}
}