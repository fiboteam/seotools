using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace DAO.AdoNet.SqlServer
{
    public class SqlSEODomainDao : SqlDaoBase<SEODomain>, ISEODomain
    {
        public SqlSEODomainDao()
        {
            TableName = "tblSEODomain";
			EntityIDName = "SEODomainId";
            StoreProcedurePrefix = "spSEODomain_";
        }
        public SqlSEODomainDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public IList<SEODomain> GetList(long SEODomainIdOnPartner, long sEODomainIdOnPartner, string domainName, int totalSEOKeyword, int totalSEOKeywordOnTop, string linkQueryKeyword, short sEODomainStatus, string sEODomainStatusStr, int shortCreatedDate, string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetList";
				object[] parms = { "@seopartneraccountid", SEODomainIdOnPartner, "@seodomainidonpartner", sEODomainIdOnPartner, "@domainname", domainName, "@totalseokeyword", totalSEOKeyword, "@totalseokeywordontop", totalSEOKeywordOnTop, "@linkquerykeyword", linkQueryKeyword, "@seodomainstatus", sEODomainStatus, "@seodomainstatusstr", sEODomainStatusStr, "@shortcreateddate", shortCreatedDate, "@note", note };
				return DbAdapter1.ReadList(sql, Make, true, parms);
			}
            catch (Exception)
            {
                return null;
            }
        }

        public int GetTotalPage(long pageSize, long sEODomainIdOnPartner,string domainName,int totalSEOKeyword,int totalSEOKeywordOnTop,string linkQueryKeyword,short sEODomainStatus,string sEODomainStatusStr,int shortCreatedDate,string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetTotalPage";
				object[] parms = { "@pagesize", pageSize, "@seodomainidonpartner",sEODomainIdOnPartner,"@domainname",domainName,"@totalseokeyword",totalSEOKeyword,"@totalseokeywordontop",totalSEOKeywordOnTop,"@linkquerykeyword",linkQueryKeyword,"@seodomainstatus",sEODomainStatus,"@seodomainstatusstr",sEODomainStatusStr,"@shortcreateddate",shortCreatedDate,"@note",note };
				return DbAdapter1.GetCount(sql, true, parms);
			}
            catch (Exception)
            {
                return -1;
            }

        }

		public bool RefreshData(SEODomain domain)
		{
			string sql = StoreProcedurePrefix + "ReFreshData";
			object[] parms = { "@seopartneraccountid", domain.SEOPartnerAccountId, "@seodomainidonpartner", domain.SEODomainIdOnPartner, "@domainname", domain.DomainName, "@totalseokeyword", domain.TotalSEOKeyword, "@totalseokeywordontop", domain.TotalSEOKeywordOnTop, "@linkquerykeyword", domain.LinkQueryKeyword, "@seodomainstatus", (short)domain.SEODomainStatus, "@seodomainstatusstr", domain.SEODomainStatusStr, "@recommend",domain.Recommendations };
			var id = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

			if (id > 0)
				return true;
			else
				return false;
		}


		public string GetRecommend(string domainName, long userid)
		{
			try
			{
				string sql = StoreProcedurePrefix + "GetRecommend";
				object[] parms = { "@domainName", domainName, "@userid", userid };
				return DbAdapter1.ExcecuteScalar(sql, true, parms).AsString();
			}
			catch (Exception)
			{
				return "";
			}
		}
	}
}
