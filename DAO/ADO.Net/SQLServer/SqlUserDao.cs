using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAO.AdoNet.SqlServer
{
    public class SqlUserDao : SqlDaoBase<User>, IUser
    {
        public SqlUserDao()
        {
            TableName = "tblUser";
            EntityIDName = "UserId";
            StoreProcedurePrefix = "spUser_";
        }
        public SqlUserDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public User FindUserBy(string username)
        {
			try
			{
				string sql = StoreProcedurePrefix + "FindUserBy";
				object[] parms = { "@username", username };
				return DbAdapter1.Read(sql, Make, true, parms);
			}
            catch (Exception)
            {
                return null;
            }
        }

		public User CheckLogin(string username, string password)
		{
			try
			{
				string sql = StoreProcedurePrefix + "CheckLogin";
				object[] parms = { "@username", username, "@password", password };
				return DbAdapter1.Read(sql, Make, true, parms);
			}
			catch (Exception)
			{
				return null;
			}
		}


		public User GetUserByUserApiId(string userid)
		{
			try
			{
				string sql = StoreProcedurePrefix + "GetUserByUserApiId";
				object[] parms = { "@userapiid", userid};
				return DbAdapter1.Read(sql, Make, true, parms);
			}
			catch (Exception)
			{
				return null;
			}
		}
	}
}
