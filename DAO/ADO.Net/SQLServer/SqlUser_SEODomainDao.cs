using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAO.AdoNet.SqlServer
{
    public class SqlUser_SEODomainDao : SqlDaoBase<User_SEODomain>, IUser_SEODomain
    {
        public SqlUser_SEODomainDao()
        {
            TableName = "tblUser_SEODomain";
            EntityIDName = "User_SEODomainId";
            StoreProcedurePrefix = "spUser_SEODomain_";
        }
        public SqlUser_SEODomainDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public IList<User_SEODomain> GetList(long pageSize, long pageNum, long userId,long sEODomainId,short status,int shortCreatedDate,string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetList";
				object[] parms = { "@pagesize", pageSize, "@pagenum", pageNum, "@userid",userId,"@seodomainid",sEODomainId,"@status",status,"@shortcreateddate",shortCreatedDate,"@note",note};
				return DbAdapter1.ReadList(sql, Make, true, parms);
			}
            catch (Exception)
            {
                return null;
            }
        }

        public int GetTotalPage(long pageSize, long userId,long sEODomainId,short status,int shortCreatedDate,string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetTotalPage";
				object[] parms = { "@pagesize", pageSize, "@userid",userId,"@seodomainid",sEODomainId,"@status",status,"@shortcreateddate",shortCreatedDate,"@note",note };
				return DbAdapter1.GetCount(sql, true, parms);
			}
            catch (Exception)
            {
                return -1;
            }

        }
    }
}
