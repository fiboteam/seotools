using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace DAO.AdoNet.SqlServer
{
    public class SqlBlockInsertKeywordPlannerDao : SqlDaoBase<BlockInsertKeywordPlanner>, IBlockInsertKeywordPlanner
    {
        public SqlBlockInsertKeywordPlannerDao()
        {
            TableName = "tblBlockInsertKeywordPlanner";
			EntityIDName = "BlockID";
            StoreProcedurePrefix = "spBlockInsertKeywordPlanner_";
        }
        public SqlBlockInsertKeywordPlannerDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public IList<BlockInsertKeywordPlanner> GetList(long pageSize, long pageNum, string blockName,string note,string remark,int shortCreatedDate)
        {
			//try
			//{
			//	string sql = StoreProcedurePrefix + "GetList";
			//	object[] parms = { "@pagesize", pageSize, "@pagenum", pageNum, "@blockname",blockName,"@note",note,"@remark",remark,"@shortcreateddate",shortCreatedDate};
			//	return DbAdapter1.ReadList(sql, Make, true, parms);
			//}
			//catch (Exception)
			//{
				return null;
			//}
			
        }

		public long InsertWithImport(string blockName,long userid)
		{
			string sql = StoreProcedurePrefix + "AddWithImport";
			object[] parms = { "@BlockName", blockName, "@userid", userid };
			var id = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

			return id;
			//if (id > 0)
			//	return true;
			//else
			//	return false;
		}
	}
}
