using DAO.AdoNet.SqlServer;
using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAO.AdoNet.SqlServer
{
    public class SqlDetailBlockInsertPlannerDao : SqlDaoBase<DetailBlockInsertPlanner>, IDetailBlockInsertPlanner
    {
        public SqlDetailBlockInsertPlannerDao()
        {
            TableName = "tblDetailBlockInsertPlanner";
			EntityIDName = "Id";
            StoreProcedurePrefix = "spDetailBlockInsertPlanner_";
        }
        public SqlDetailBlockInsertPlannerDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public IList<DetailBlockInsertPlanner> GetList(long pageSize, long pageNum, long blockID,long keywordPlannerId,string note,string remark,int shortCreatedDate)
        {
			//try
			//{
			//	string sql = StoreProcedurePrefix + "GetList";
			//	object[] parms = { "@pagesize", pageSize, "@pagenum", pageNum, "@blockid",blockID,"@keywordplannerid",keywordPlannerId,"@note",note,"@remark",remark,"@shortcreateddate",shortCreatedDate};
			//	return DbAdapter1.ReadList(sql, Make, true, parms);
			//}
			//catch (Exception)
			//{
			return null;
			//}
        }

    }
}
