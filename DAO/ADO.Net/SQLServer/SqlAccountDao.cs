using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAO.AdoNet.SqlServer
{
    public class SqlAccountDao : SqlDaoBase<Account>, IAccount
    {
        public SqlAccountDao()
        {
            TableName = "tblAccount";
            EntityIDName = "AccountId";
            StoreProcedurePrefix = "spAccount_";
        }
        public SqlAccountDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public IList<Account> GetList(long pageSize, long pageNum, string userName,string userPass,string fullName,string email,string phone,string address,short status,int shortCreatedDate,string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetList";
				object[] parms = { "@pagesize", pageSize, "@pagenum", pageNum, "@username",userName,"@userpass",userPass,"@fullname",fullName,"@email",email,"@phone",phone,"@address",address,"@status",status,"@shortcreateddate",shortCreatedDate,"@note",note};
				return DbAdapter1.ReadList(sql, Make, true, parms);
			}
            catch (Exception)
            {
                return null;
            }
        }

        public int GetTotalPage(long pageSize, string userName,string userPass,string fullName,string email,string phone,string address,short status,int shortCreatedDate,string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetTotalPage";
				object[] parms = { "@pagesize", pageSize, "@username",userName,"@userpass",userPass,"@fullname",fullName,"@email",email,"@phone",phone,"@address",address,"@status",status,"@shortcreateddate",shortCreatedDate,"@note",note };
				return DbAdapter1.GetCount(sql, true, parms);
			}
            catch (Exception)
            {
                return -1;
            }

        }
    }
}
