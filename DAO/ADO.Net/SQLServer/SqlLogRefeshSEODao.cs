using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.AdoNet.SqlServer
{
	public class SqlLogRefeshSEODao : SqlDaoBase<LogRefreshSEO>, ILogRefeshSEO
    {
        public SqlLogRefeshSEODao()
        {
            TableName = "tblLogRefeshSEO";
            EntityIDName = "LogRefeshSEOId";
            StoreProcedurePrefix = "spLogRefeshSEO_";
        }
        public SqlLogRefeshSEODao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public IList<LogRefreshSEO> GetList(long pageSize, long pageNum, long objectID, short statusRefesh, string linkGetData, string dataRefesh, int shortCreatedDate, string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetList";
				object[] parms = { "@pagesize", pageSize, "@pagenum", pageNum, "@objectid",objectID,"@statusrefesh",statusRefesh,"@linkgetdata",linkGetData,"@datarefesh",dataRefesh,"@shortcreateddate",shortCreatedDate,"@note",note};
				return DbAdapter1.ReadList(sql, Make, true, parms);
			}
            catch (Exception)
            {
                return null;
            }
        }

        public int GetTotalPage(long pageSize, long objectID,short statusRefesh,string linkGetData,string dataRefesh,int shortCreatedDate,string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetTotalPage";
				object[] parms = { "@pagesize", pageSize, "@objectid",objectID,"@statusrefesh",statusRefesh,"@linkgetdata",linkGetData,"@datarefesh",dataRefesh,"@shortcreateddate",shortCreatedDate,"@note",note };
				return DbAdapter1.GetCount(sql, true, parms);
			}
            catch (Exception)
            {
                return -1;
            }

        }
    }
}
