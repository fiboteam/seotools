using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace DAO.AdoNet.SqlServer
{
    public class SqlSEOKeywordDao : SqlDaoBase<SEOKeyword>, ISEOKeyword
    {
        public SqlSEOKeywordDao()
        {
            TableName = "tblSEOKeyword";
            EntityIDName = "SEOKeywordId";
            StoreProcedurePrefix = "spSEOKeyword_";
        }
        public SqlSEOKeywordDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public IList<SEOKeyword> GetList(long pageSize, long pageNum, long sEOKeywordIdOnPartner,long sEODomainId,string sEOKeywordName,int pricePerWeekOnPartner,int price,int rankOnGoogle,string linkCheck,short sEOKeywordStatus,string sEOKeywordStatusStr,int shortCreatedDate,string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetList";
				object[] parms = { "@pagesize", pageSize, "@pagenum", pageNum, "@seokeywordidonpartner",sEOKeywordIdOnPartner,"@seodomainid",sEODomainId,"@seokeywordname",sEOKeywordName,"@priceperweekonpartner",pricePerWeekOnPartner,"@price",price,"@rankongoogle",rankOnGoogle,"@linkcheck",linkCheck,"@seokeywordstatus",sEOKeywordStatus,"@seokeywordstatusstr",sEOKeywordStatusStr,"@shortcreateddate",shortCreatedDate,"@note",note};
				return DbAdapter1.ReadList(sql, Make, true, parms);
			}
            catch (Exception)
            {
                return null;
            }
        }

        public int GetTotalPage(long pageSize, long sEOKeywordIdOnPartner,long sEODomainId,string sEOKeywordName,int pricePerWeekOnPartner,int price,int rankOnGoogle,string linkCheck,short sEOKeywordStatus,string sEOKeywordStatusStr,int shortCreatedDate,string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetTotalPage";
				object[] parms = { "@pagesize", pageSize, "@seokeywordidonpartner",sEOKeywordIdOnPartner,"@seodomainid",sEODomainId,"@seokeywordname",sEOKeywordName,"@priceperweekonpartner",pricePerWeekOnPartner,"@price",price,"@rankongoogle",rankOnGoogle,"@linkcheck",linkCheck,"@seokeywordstatus",sEOKeywordStatus,"@seokeywordstatusstr",sEOKeywordStatusStr,"@shortcreateddate",shortCreatedDate,"@note",note };
				return DbAdapter1.GetCount(sql, true, parms);
			}
            catch (Exception)
            {
                return -1;
            }

        }


		public bool RefreshData(SEOKeyword keyword)
		{
			string sql = StoreProcedurePrefix + "ReFreshData";
			object[] parms = { "@seokeywordidonpartner", keyword.SEOKeywordIdOnPartner, "@seodomainid", keyword.SEODomainId, "@seokeywordname", keyword.SEOKeywordName, "@priceperweekonpartner", keyword.PricePerWeekOnPartner, "@rankongoogle", keyword.RankOnGoogle, "@linkcheck", keyword.LinkCheck, "@seokeywordstatus", (short)keyword.SEOKeywordStatus, "@seokeywordstatusstr", keyword.SEOKeywordStatusStr };
			var id = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

			if (id > 0)
				return true;
			else
				return false;
		}


		public List<SEOKeyword> GetAllKeywordAndDomainRelated()
		{
			try
			{
				string sql = StoreProcedurePrefix + "GetAllKeywordAndDomainRelated";
				//object[] parms = { "@pagesize", pageSize, "@pagenum", pageNum, "@seokeywordidonpartner", sEOKeywordIdOnPartner, "@seodomainid", sEODomainId, "@seokeywordname", sEOKeywordName, "@priceperweekonpartner", pricePerWeekOnPartner, "@price", price, "@rankongoogle", rankOnGoogle, "@linkcheck", linkCheck, "@seokeywordstatus", sEOKeywordStatus, "@seokeywordstatusstr", sEOKeywordStatusStr, "@shortcreateddate", shortCreatedDate, "@note", note };
				return DbAdapter1.ReadList(sql, Make, true, null);
			}
			catch (Exception)
			{
				return null;
			}
		}


		public List<SEOKeyword> GetAllKeywordWithFilter(long domainid, long keywordid)
		{
			try
			{
				string sql = StoreProcedurePrefix + "GetAllKeywordWithFilter";
				object[] parms = { "@domainid", domainid, "@keywordid", keywordid };
				return DbAdapter1.ReadList(sql, Make, true, parms);
			}
			catch (Exception)
			{
				return null;
			}
		}


		public int GetRank(string domainName, string keywordName,long userid)
		{
			try
			{
				string sql = StoreProcedurePrefix + "GetRank";
				object[] parms = { "@domainName", domainName, "@keywordName", keywordName, "@userid", userid };
				return DbAdapter1.ExcecuteScalar(sql, true, parms).AsInt();
			}
			catch (Exception)
			{
				return -1;
			}
		}
	}
}
