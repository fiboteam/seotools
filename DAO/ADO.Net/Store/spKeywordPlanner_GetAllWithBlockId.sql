use SEODB
go
alter procedure [dbo].[spKeywordPlanner_GetAllWithBlockId]
	@BlockID bigint
as
begin
	select tblKeywordPlanner.* from tblDetailBlockInsertPlanner
	inner join tblKeywordPlanner on tblDetailBlockInsertPlanner.KeywordPlannerId=tblKeywordPlanner.KeywordPlannerId
	where BlockID=@BlockID
	order by tblKeywordPlanner.KeywordName asc
end