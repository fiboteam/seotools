use SEODB

go
/*store lấy user từ user api*/
alter procedure [dbo].[spSEOKeyword_GetRank] --'hoangnguyen.edu.vn','seo la gi',2
	@domainName nvarchar(max),
	@keywordName nvarchar(255),
	@userid bigint
as
begin
	select tblSEOKeyword.RankOnGoogle from tblSEOKeyword
	inner join tblSEODomain on tblSEOKeyword.SEODomainId=tblSEODomain.SEODomainId
	inner join tblUser on tblSEODomain.UserId=tblUser.UserId
	where tblUser.UserId=@userid
	and DomainName=@domainName
	and SEOKeywordName=@keywordName
end