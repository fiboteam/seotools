use SEODB

go
alter procedure [dbo].[spSEOKeyword_GetAllKeywordAndDomainRelated]
as
begin
	--select keyword.SEODomainId,domain.DomainName,domain.SEODomainStatus,domain.SEODomainStatusStr,
	--keyword.SEOKeywordId,keyword.SEOKeywordName,keyword.SEOKeywordStatus,keyword.PricePerWeekOnPartner,keyword.RankOnGoogle,keyword.UpdatedDate,keyword.SEOKeywordStatusStr
	--from tblSEOKeyword as keyword
	--inner join tblSEODomain as domain on keyword.SEODomainId=domain.SEODomainId
	--order by domain.SEODomainId,keyword.SEOKeywordId
	
	select SEODomainId,DomainName,tblcrossData.*,tblcrossCount.* from tblSEODomain as tblsource
	cross apply
	(
		select SEOKeywordId,SEOKeywordName,SEOKeywordStatus,PricePerWeekOnPartner,RankOnGoogle,UpdatedDate,SEODomainStatusStr from tblSEOKeyword
		where SEODomainId=tblsource.SEODomainId
	) as tblcrossData
	cross apply
	(
		select COUNT(*) as TotalKeyword from tblSEOKeyword
		where SEODomainId=tblsource.SEODomainId
		group by SEODomainId
	) as tblcrossCount
	order by SEODomainId,SEOkeywordID
	
end