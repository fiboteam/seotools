USE [DomainBank]
GO
/****** Object:  StoredProcedure [dbo].[spClient_AddUpdate]    Script Date: 02/21/2016 19:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

alter  proc [dbo].[spFileDownload_GetFileBy] --"NameJet","Pre-Release",1
	@name nvarchar(255),
	@type nvarchar(255),
	@status tinyint
as
begin
	declare @id bigint

	select top 1 @id=FileDownloadId from tblFileDownload
	where tblFileDownload.Origin=@name
	and Type=@type
	and Status=@status
	order by FileDownloadId asc
	if(@id>0)
	begin
		/*set status file đó là đang đọc*/
		update tblFileDownload set Status=2
		where FileDownloadId=@id

		select * from tblFileDownload
		where FileDownloadId=@id
	end
end


