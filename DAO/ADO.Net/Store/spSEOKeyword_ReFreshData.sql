USE [SEODB]
GO
/****** Object:  StoredProcedure [dbo].[spSEOKeyword_ReFreshData]    Script Date: 05/25/2016 20:54:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spSEOKeyword_ReFreshData]  --1,1000,'asdasdasd',1,1,'',1,''
    -- Add the parameters for the stored procedure here 
    @seokeywordidonpartner bigint, 
    @seodomainid bigint, 
    @seokeywordname nvarchar(1024), 
    @priceperweekonpartner int, 
    @rankongoogle int, 
    @linkcheck nvarchar(1024), 
    @seokeywordstatus tinyint, 
    @seokeywordstatusstr nvarchar(255)
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
	SET NOCOUNT ON; 
	
	if exists (select SEOKeywordId from tblSEOKeyword where SEOKeywordName=@seokeywordname and SEODomainId=@seodomainid)
    begin
		update tblSEOKeyword set 
		SEOKeywordIdOnPartner = case when @seokeywordidonpartner=-1 then SEOKeywordIdOnPartner else @seokeywordidonpartner end, 
		PricePerWeekOnPartner = case when @priceperweekonpartner=-1 then PricePerWeekOnPartner else @priceperweekonpartner end, 
		RankOnGoogle = case when @rankongoogle=-1 then RankOnGoogle else @rankongoogle end , 
		LinkCheck = @linkcheck, 
		SEOKeywordStatus = @seokeywordstatus, 
		SEOKeywordStatusStr = @seokeywordstatusstr, 
		UpdatedDate = GETDATE()
		where SEOKeywordName=@seokeywordname
		and SEODomainId=@seodomainid
    end
    else
    begin
		insert into tblSEOKeyword (SEOKeywordIdOnPartner,PricePerWeekOnPartner,RankOnGoogle,LinkCheck,SEOKeywordStatus,SEOKeywordStatusStr,
		CreatedDate,UpdatedDate,ShortCreatedDate,SEOKeywordName,SEODomainId,Price)
		values (case when @seokeywordidonpartner=-1 then 0 else @seokeywordidonpartner end,
		case when @priceperweekonpartner=-1 then 0 else @priceperweekonpartner end,
		case when @rankongoogle=-1 then 0 else @rankongoogle end,
		@linkcheck,@seokeywordstatus,@seokeywordstatusstr,
		GETDATE(),GETDATE(),CONVERT(varchar(8),GETDATE(),112),@seokeywordname,@seodomainid,0)
    end
   
    SELECT @@ROWCOUNT as SEOKeywordId 
END