USE [SEODB]
GO
/****** Object:  UserDefinedFunction [dbo].[Compare2String]    Script Date: 05/20/2016 11:36:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER function [dbo].[Compare2String] (@string1 nvarchar(max),@string2 nvarchar(max))
returns tinyint
as
begin
	declare @str1 nvarchar(max)=@string1
	declare @str2 nvarchar(max)=@string2
	declare @isEqual tinyint=0;
	
	declare @len int=0;
	
	set @str1=REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(@str1)),',',''),',',''),' ','') 
	set @str2=REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(@str2)),',',''),',',''),' ','') 
	
	set @len=LEN(@str1);
	
	declare @i int =1
	declare @char char;
	declare @haveSpace tinyint=0;
	
	while(@i<=@len)
	begin
		set @char = SUBSTRING(@str1,@i,1)
		
		if(@char=' ' and @haveSpace=0)
		begin
			set @i=@i+1
			set @haveSpace=1
		end
		else if(@char=' ' and @haveSpace=1)
		begin
			set @str1 = STUFF(@str1,@i,LEN(@str1),SUBSTRING(@str1,@i+1,LEN(@str1)))
			--set @i=@i-1
			set @len=@len-1
		end
		else if (@char!=' ')
		begin
			set @i=@i+1
			set @haveSpace=0
		end
	end

	--------------
	
	set @len=LEN(@str2);
	
	set @i =1
	set @char='';
	set @haveSpace=0;
	
	while(@i<=@len)
	begin
		set @char = SUBSTRING(@str2,@i,1)
		
		if(@char=' ' and @haveSpace=0)
		begin
			set @i=@i+1
			set @haveSpace=1
		end
		else if(@char=' ' and @haveSpace=1)
		begin
			set @str2 = STUFF(@str2,@i,LEN(@str2),SUBSTRING(@str2,@i+1,LEN(@str2)))
			--set @i=@i-1
			set @len=@len-1
		end
		else if (@char!=' ')
		begin
			set @i=@i+1
			set @haveSpace=0
		end
	end
	
	if(@str2=@str1)
	begin
		set @isEqual=1
	end
	else
		set @isEqual=0

	
	return(@isEqual)
	--return(@str1)
end


--select STUFF(N'Nguyễn  Minh Hiếu',7,LEN(N'Nguyễn Minh Hiếu'),N'Minh Hiếu')


--select SUBSTRING(N'Nguyễn Minh Hiếu',7,LEN(N'Nguyễn Minh Hiếu'))

--Nguyễn   Minh Hiếu
----Nguyễn  Minh Hiếu
--Nguyễn Minh Hiếu

--select dbo.Compare2String(N'Nguyễn  Minh Hiếu',N'Nguyễn Minh Hiếu')
