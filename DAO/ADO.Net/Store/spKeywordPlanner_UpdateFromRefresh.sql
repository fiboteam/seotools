USE [SEODB]
GO
/****** Object:  StoredProcedure [dbo].[spKeywordPlanner_UpdateFromRefresh]    Script Date: 05/31/2016 10:21:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[spKeywordPlanner_UpdateFromRefresh]
	@keywordName nvarchar(255),
	@AvgMonthlySearches bigint,
	@Competition nvarchar(255),
	@SuggestedBid decimal(10,2),
	@FormatCurrency nvarchar(10),
	@CompetitionIndex decimal(5,2),
	@ClickRate decimal(5,2),
	@TotalCostPerMonth bigint,
	@SEOFibo bigint,
	@ChargesForMaintenance bigint,
	@Setup bigint,
	@HardWorkIndex decimal (5,2),
	@CompetitionInt tinyint,
	@SetupCost bigint,
	@KeywordPlanerStatus tinyint
as
begin
	--Thêm phần này để xác định là có public giá ra hay không
	/*
	 Điều kiện: Không public giá và note là: Liên hệ lại với đối tác để biết chi tiết
	 * keyword < 2 từ
	 * keyword = 2 từ && tìm kiếm > 3000
	 * keyword = 3 từ && tìm kiếm > 9000
	 */
	 declare @note nvarchar(1024)
	 declare @IsPublicToPartner tinyint = 1
	 
	 /*
	 2. Cho báo giá tất cả các từ khóa dưới 30,000,000 vnd (Không giới hạn từ khóa báo giá như cũ nữa).
		Cách từ từ 30,000,000 trở lên => để yêu cầu liên hệ như cũ 
		Em xem khi nào có thể làm xong nội dung này thì báo chị nhen.
		task #3066
	 */
	 
	 if(((len(@keywordName) - len(replace(@keywordName, ' ', '')))<2) or ((len(@keywordName) - len(replace(@keywordName, ' ', '')))=2 and @AvgMonthlySearches>3000) or ((len(@keywordName) - len(replace(@keywordName, ' ', '')))=3 and @AvgMonthlySearches>9000) or (@SetupCost>=30000000))
	 begin
		/*Không public giá*/
		set @note=N'Liên hệ lại với đối tác để biết chi tiết'
		set @IsPublicToPartner = 0
	 end
	 
	 /*
	 2. Cho báo giá tất cả các từ khóa dưới 30,000,000 vnd (Không giới hạn từ khóa báo giá như cũ nữa).
		Cách từ từ 30,000,000 trở lên => để yêu cầu liên hệ như cũ 
		Em xem khi nào có thể làm xong nội dung này thì báo chị nhen.
		task #3066
	 */
	 /*
	 4. Gỡ/Xóa giới hạn báo giá trong tool hiện tại: Hiện tại khóa báo giá tất cả các từ khóa có giá SEt-up từ 20,000,000 trở lên 
	 => nâng giới hạn không báo giá lên "Từ 60,000,000 /từ khóa trở lên"
	 08/06/2016
	 */
	 if(@SetupCost>0 and @SetupCost<60000000 and ((len(@keywordName) - len(replace(@keywordName, ' ', '')))>=2))
	 begin
		/*public giá*/
		set @note=''
		set @IsPublicToPartner = 1
	 end
	 
	 /*
	 5. Không hiển thị giá: Không hiển thị giá đối với các từ khóa có giá Set-up từ 60,000,000 vnđ trở lên 
	 => không báo giá mà hiển thị thông báo ở ghi chú " Từ khóa đặc bie65tg - vui lòng liên hệ để được báo giá trực tiếp".
	 */
	 if(@SetupCost>=60000000)
	 begin
		/*Không public giá*/
		set @note=N'Từ khoá đặc biệt - Vui lòng liên hệ để được báo giá trực tiếp.'
		set @IsPublicToPartner = 0
	 end
	 
	update tblKeywordPlanner set AvgMonthlySearches=@AvgMonthlySearches,
	Competition=@Competition,
	SuggestedBid=@SuggestedBid,
	FormatCurrency=@FormatCurrency,
	KeywordPlanerStatus=@KeywordPlanerStatus,
	UpdatedDate=GETDATE(),CompetitionIndex=@CompetitionIndex,ClickRate=@ClickRate,TotalCostPerMonth=@TotalCostPerMonth,SEOFibo=@SEOFibo,ChargesForMaintenance=@ChargesForMaintenance,
	Setup=@Setup,HardWorkIndex=@HardWorkIndex,CompetitionInt=@CompetitionInt,SetupCost=@SetupCost,
	Note=@note,
	IsPublicToPartner=@IsPublicToPartner
	--where RTRIM(LTRIM(KeywordName))=RTRIM(LTRIM(@keywordName))
	where dbo.Compare2String(@KeywordName,KeywordName)=1
	and KeywordPlanerStatus=2
	
	select @@ROWCOUNT
end