USE [SEODB]
GO
/****** Object:  StoredProcedure [dbo].[spSEODomain_ReFreshData]    Script Date: 05/25/2016 20:39:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spSEODomain_ReFreshData] 
	@seopartneraccountid bigint,
    @seodomainidonpartner bigint, 
    @domainname nvarchar(1024), 
    @totalseokeyword int, 
    @totalseokeywordontop int, 
    @linkquerykeyword nvarchar(1024), 
    @seodomainstatus tinyint, 
    @seodomainstatusstr nvarchar(255),
    @recommend nvarchar(max)
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
    
    if exists (select SEODomainId from tblSEODomain where DomainName=@domainname and SEOPartnerAccountId=@seopartneraccountid)
	begin
		update tblSEODomain set 
		SEODomainIdOnPartner = @seodomainidonpartner, 
		TotalSEOKeyword = @totalseokeyword, 
		TotalSEOKeywordOnTop = @totalseokeywordontop, 
		LinkQueryKeyword = @linkquerykeyword, 
		SEODomainStatus = @seodomainstatus, 
		SEODomainStatusStr = @seodomainstatusstr, 
		UpdatedDate = GETDATE(),
		Recommendations= case when @recommend ='' or @recommend is null then Recommendations else @recommend end
		where DomainName = @domainname 
		and SEOPartnerAccountId=@seopartneraccountid
    end
    else
    begin
		insert into tblSEODomain (SEODomainIdOnPartner,TotalSEOKeyword,TotalSEOKeywordOnTop,LinkQueryKeyword,SEODomainStatus,SEODomainStatusStr,
		CreatedDate,UpdatedDate,Recommendations,DomainName,SEOPartnerAccountId)
		values (@seodomainidonpartner,@totalseokeyword,@totalseokeywordontop,@linkquerykeyword,@seodomainstatus,@seodomainstatusstr,
		GETDATE(),GETDATE(),@recommend,@domainname,@seopartneraccountid)
    end
   
    -- Insert statements for procedure here 
    SELECT @@ROWCOUNT as SEODomainId 
END