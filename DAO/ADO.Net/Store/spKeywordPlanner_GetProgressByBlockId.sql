use SEODB
go
alter procedure [dbo].[spKeywordPlanner_GetProgressByBlockId] --38,2
	@BlockID bigint,
	@userid bigint
as
begin
	declare @total float
	declare @success float
	declare @tbl_keyword table (id bigint)
	
	insert into @tbl_keyword
	select KeywordPlannerId from tblDetailBlockInsertPlanner as detail
	inner join tblBlockInsertKeywordPlanner on detail.BlockID=tblBlockInsertKeywordPlanner.BlockID
	where detail.BlockID=@BlockID
	and UserId=@userid
	
	select @total=COUNT(*) from @tbl_keyword
	
	select @success=COUNT(*) from tblKeywordPlanner
	where KeywordPlannerId in
	(
		select id from @tbl_keyword
	)
	and KeywordPlanerStatus>2
	
	if(@total<=@success)
	begin
		select 100
	end
	else
	begin
		--select (@success/@total)*100
		--select @success
		--select @total
		select  (round((@success)/(@total)*100,0)) 
	end
end

--select round((2*1.0)/(3*1.0),0)

--select (2.0/3.0)*100

--select round(11.5,1)