USE [DomainBank]
GO
/****** Object:  StoredProcedure [dbo].[spClient_AddUpdate]    Script Date: 02/21/2016 19:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

alter  proc [dbo].[spFileDownload_AddUpdate] 
	@ID bigint,
	@Origin nvarchar(255),
	@Type nvarchar(255),
	@HomePage nvarchar(255),
	@LinkDownload nvarchar(255),
	@Query varchar(1025),
	@Title nvarchar(1024),
	@FileName nvarchar(255),
	@LocalPath nvarchar(1024),
	@Status tinyint
as
begin
	if(@ID<0)
	begin
		SET NOCOUNT ON;

		insert into tblFileDownload (Origin,Type,HomePage,LinkDownload,Query,Title,FileName,LocalPath,Status,CreatedDate,UpdatedDate,ShortCreatedDate)
		values (@Origin,@Type,@HomePage,@LinkDownload,@Query,@Title,@FileName,@LocalPath,@Status,getdate(),getdate(),convert(varchar(8),GETDATE(),112))

		set @ID=@@identity
	end
	else
	begin
		update tblFileDownload set Status=@Status,UpdatedDate=getdate()
		where FileDownloadId=@ID

		set @ID=@@ROWCOUNT
	end

	select @ID as Id
end


