USE [SEODB]
GO
/****** Object:  StoredProcedure [dbo].[spKeywordPlanner_AddWithImport]    Script Date: 05/20/2016 11:34:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[spKeywordPlanner_AddWithImport]
	@BlockID bigint,
	@KeywordName nvarchar(255)
as
begin
	declare @id bigint = 0
	--if exists (select 1 from [tblKeywordPlanner] where  REPLACE(RTRIM(LTRIM(KeywordName)),',','')=REPLACE(RTRIM(LTRIM(@KeywordName)),',',''))
	if exists (select 1 from [tblKeywordPlanner] where dbo.Compare2String(@KeywordName,KeywordName)=1)
	begin
		/*neu keyword đa ton tai
		thi add vao detail luon, khong can insert vao table keywordplanner
		*/
		--set @id = (select top 1 KeywordPlannerId from [tblKeywordPlanner] where REPLACE(RTRIM(LTRIM(KeywordName)),',','')=REPLACE(RTRIM(LTRIM(@KeywordName)),',',''))
		set @id = (select top 1 KeywordPlannerId from [tblKeywordPlanner] where dbo.Compare2String(@KeywordName,KeywordName)=1)
	end
	else
	begin
		/*
		nếu chưa có thì add vao table keywordplanner
		*/
		insert into [tblKeywordPlanner] (KeywordName,KeywordPlanerStatus)
		values (REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(@KeywordName)),',',''),' ',''),'.',''),1)
		
		set @id=@@IDENTITY
	end
	insert into [tblDetailBlockInsertPlanner] (BlockID,KeywordPlannerId)
	values (@BlockID,@id)
	
	select @id
end