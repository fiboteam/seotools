USE [fibosms]
GO
/****** Object:  StoredProcedure [dbo].[spClient_AddUpdate]    Script Date: 02/21/2016 19:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

alter  proc [dbo].[spAlertClient_v2_GetSingle] 
@id bigint 
as
begin
	select top 1 tblAlertClient.*,ServiceTypeName,ClientNo from tblAlertClient
	inner join tblClient on tblAlertClient.ClientID=tblClient.ClientID
	inner join tblServiceType on tblAlertClient.ServiceTypeID=tblServiceType.ServiceTypeID
	where ID=@id
end




