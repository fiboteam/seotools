use SEODB
go
alter procedure [dbo].[spKeywordPlanner_GetAllWithFilter]
	@KeywordName nvarchar(255),
	@AvgMonthlySearches bigint,
	@Competition nvarchar(255),
	@SuggestedBid decimal(10,2),
	@KeywordPlanerStatus tinyint
as
begin
	select * from tblKeywordPlanner
	where (KeywordName=@KeywordName or @KeywordName='')
	and (AvgMonthlySearches=@AvgMonthlySearches or @AvgMonthlySearches=-1)
	and (Competition=@Competition or @Competition='')
	and (SuggestedBid=@SuggestedBid or @SuggestedBid=-1)
	and (KeywordPlanerStatus=@KeywordPlanerStatus or @KeywordPlanerStatus=0)
	order by KeywordName desc
end
