use SEODB
go
alter procedure [dbo].[spKeywordPlanner_GetAllKeywordPlannerNameToRefresh]
as
begin
	declare @table_temp table (KeywordPlannerId bigint,KeywordName nvarchar(255))
	------từ khoá mới
	insert into @table_temp
	select KeywordPlannerId,KeywordName from tblKeywordPlanner
	where KeywordPlanerStatus =1
	order by KeywordPlannerId asc 
	
	------từ khoá hết hạn mà chưa update lại status
	insert into @table_temp
	select KeywordPlannerId,KeywordName from tblKeywordPlanner
	where KeywordPlanerStatus = 2
	and DATEDIFF(MINUTE,UpdatedDate,GETDATE())>=5
	order by KeywordPlannerId asc 
	
	update tblKeywordPlanner set UpdatedDate=GETDATE(),KeywordPlanerStatus=2
	where KeywordPlannerId in 
	(
		select KeywordPlannerId from @table_temp
	)
	
	select * from @table_temp
end

--exec spKeywordPlanner_GetAllKeywordPlannerNameToRefresh