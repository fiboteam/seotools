use SEODB

drop table [tblBlockInsertKeywordPlanner]
create table [dbo].[tblBlockInsertKeywordPlanner]
(
	BlockID bigint primary key identity,
	BlockName nvarchar(255),
	Note nvarchar(255),
	Remark nvarchar(255),
	CreatedDate datetime default getdate(),
	UpdatedDate datetime default getdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112)
)



CREATE INDEX index_BlockName
ON [tblBlockInsertKeywordPlanner] (BlockName);

truncate table [tblBlockInsertKeywordPlanner]



drop table [tblKeywordPlanner]
create table [dbo].[tblKeywordPlanner]
(
	KeywordPlannerId bigint primary key identity,
	KeywordName nvarchar(255),
	AvgMonthlySearches bigint, -- lượt tìm (từ api)
	Competition nvarchar(255),  -- cạnh tranh (từ api)
	CompetitionIndex decimal(5,2), -- percent cạnh tranh
	SuggestedBid decimal(10,2), -- giá thầu đề xuất (từ api)
	FormatCurrency nvarchar(10), -- định dạng tiền từ api
	KeywordPlanerStatus tinyint, --trạng thái
	ClickRate decimal(5,2), -- tỉ lệ click (0.05)
	TotalCostPerMonth bigint, -- tong chi phí tháng = AvgMonthlySearches*SuggestedBid*ClickRate
	SEOFibo bigint, -- SEO Fibo (phí duy tri) = TotalCostPerMonth*0.9
	ChargesForMaintenance bigint, --phí duy trì (tính thêm) = MAX(300000,SEOFibo)
	Setup bigint, --setup (phí duy tri) = ChargesForMaintenance*8
	HardWorkIndex decimal (5,2), --chỉ số khó làm = MAX(CompetitionIndex,0.4)
	CompetitionInt tinyint, --từ competition tính ra số = quy dịnh trong cong thức từ Competition
	SetupCost bigint, -- chi phi setup (tính thêm) = ChargesForMaintenance*HardWorkIndex*CompetitionInt
	Note nvarchar(255),
	Remark nvarchar(255),
	CreatedDate datetime default getdate(),
	UpdatedDate datetime default getdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112)
)
alter table [tblKeywordPlanner]
alter column AvgMonthlySearches bigint

alter table [tblKeywordPlanner]
alter column TotalCostPerMonth bigint

alter table [tblKeywordPlanner]
alter column SEOFibo bigint

alter table [tblKeywordPlanner]
alter column ChargesForMaintenance bigint

alter table [tblKeywordPlanner]
alter column Setup bigint

alter table [tblKeywordPlanner]
alter column SetupCost bigint

alter table [tblKeywordPlanner]
add IsPublicToPartner tinyint

CREATE INDEX index_KeywordName
ON [tblKeywordPlanner] (KeywordName);

truncate table [tblKeywordPlanner]

drop table [tblDetailBlockInsertPlanner]
create table [dbo].[tblDetailBlockInsertPlanner]
(
	Id bigint primary key identity,
	BlockID bigint not null,
	KeywordPlannerId bigint not null,
	Note nvarchar(255),
	Remark nvarchar(255),
	CreatedDate datetime default getdate(),
	UpdatedDate datetime default getdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112)
)

truncate table [tblDetailBlockInsertPlanner]


exec [spBlockInsertKeywordPlanner_AddWithImport] '20160401000000' -- 1


exec spBlockInsertKeywordPlanner_GetAll


exec [spKeywordPlanner_AddWithImport] '3','vay tieu dung'



exec [spKeywordPlanner_GetAllWithFilter] '',-1,'',-1,0

exec spKeywordPlanner_GetAllWithBlockId 3

exec spKeywordPlanner_GetAllKeywordPlannerNameToRefresh

select * from [tblBlockInsertKeywordPlanner]

select * from  [tblDetailBlockInsertPlanner]

select * from [tblKeywordPlanner]


exec spKeywordPlanner_UpdateFromRefresh N'lien minh huyen thoai',673000,'Medium',8936,'đ',0.7,0.05,300696416,270626752,270626752,2165014016,0.7,7,8900000,1

select * from tblBlockInsertKeywordPlanner
select * from tblUser

select * from tblAccount

go
drop table tblSpecialKeyword
create table [dbo].[tblSpecialKeyword]
(
	SpecialKeywordId bigint primary key identity,
	GroupName nvarchar(1024) null,
	SubName nvarchar(1024) null,
	CompetitionIndex decimal(5,2) null,
	KeywordName1 nvarchar(100) null,
	KeywordName2 nvarchar(100) null,
	UserId bigint,
	Note nvarchar(1024) null,
	Remark nvarchar(1024) null,
	CreatedDate datetime default getdate(),
	UpdatedDate datetime default getdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112)
)

create index index_KeywordName1
on tblSpecialKeyword (KeywordName1)


create index index_KeywordName2
on tblSpecialKeyword (KeywordName2)

alter table tblSpecialKeyword
add Status tinyint

truncate table tblSpecialKeyword

select * from tblSpecialKeyword

update tblKeywordPlanner set KeywordPlanerStatus=1
where KeywordPlannerId > 3304

update tblKeywordPlanner set KeywordPlanerStatus=1
where KeywordPlannerId in (3304,3305)

select * from tblKeywordPlanner
order by KeywordPlannerId desc
--3365
--3363

--delete from tblKeywordPlanner
--where KeywordPlannerId in (3362,3360,3359)









