use SEODB

/*store lấy user từ user api*/
go
alter procedure [dbo].[spUser_GetUserByUserApiId]
	@userapiid nvarchar(max)
as
begin
	select tblUser.* from tblUser_AspNetUsers
	inner join tblUser on tblUser_AspNetUsers.UserId=tblUser.UserId
	where tblUser_AspNetUsers.AspNetUsersId=@userapiid
	and tblUser_AspNetUsers.Status=1 and tblUser.Status=1
end
