USE [SEODB]
GO
/****** Object:  StoredProcedure [dbo].[spLogRefeshSEO_AddUpdate]    Script Date: 03/04/2016 23:05:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spLogRefeshSEO_AddUpdate]  -1,1,1,1,'','','Tool refresh đã mở'
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
    @PartnerAccountId bigint,
    @objectid bigint, 
    @statusrefesh tinyint, 
    @linkgetdata nvarchar(1024), 
    @datarefesh nvarchar(1024), 
    @note nvarchar(1024) 
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id <= 0) 
    begin 
    insert into tblLogRefreshSEO( 
			PartnerAccountId ,
            ObjectID, 
            StatusRefesh, 
            LinkGetData, 
            DataRefesh, 
            CreatedDate,
            ShortCreatedDate, 
            Note) 
    values(
			@PartnerAccountId,
            @objectid, 
            @statusrefesh, 
            @linkgetdata, 
            @datarefesh, 
            GETDATE(),
            CONVERT(varchar(8),GETDATE(),112), 
            @note
                ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblLogRefreshSEO set 
        PartnerAccountId =@PartnerAccountId,
        ObjectID = @objectid, 
        StatusRefesh = @statusrefesh, 
        LinkGetData = @linkgetdata, 
        DataRefesh = @datarefesh
        where LogRefeshSEOID = @id 
    end 
    -- Insert statements for procedure here 
    SELECT @id as LogRefeshSEOID 
END