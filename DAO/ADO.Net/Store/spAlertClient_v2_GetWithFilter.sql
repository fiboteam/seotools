USE fibosms
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[spAlertClient_v2_GetWithFilter] 50,1,'',-1
	@pagesize int, 
	@pagenum int,
	@clientno varchar(255),
	@status smallint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	END
	select @totalRec= COUNT(*)
	from tblAlertClient 
	inner join tblClient on tblAlertClient.ClientID=tblClient.ClientID
	inner join tblservicetype on tblAlertClient.ServiceTypeID=tblServiceType.ServiceTypeID
	where
		(@status= -1 OR @status=Status)  and
		(@clientno='' or ClientNo=@clientno)
		
	select @totalRec AS TotalRec,* 
	from ( 
	select ROW_NUMBER() OVER(ORDER BY tblAlertClient.id desc) AS RowNum, 
	tblAlertClient.*,ClientNo,ServiceTypeName
	from tblAlertClient 
	inner join tblClient on tblAlertClient.ClientID=tblClient.ClientID
	inner join tblservicetype on tblAlertClient.ServiceTypeID=tblServiceType.ServiceTypeID
	where
		(@status= -1 OR @status=Status)  and
		(@clientno='' or ClientNo=@clientno)
	) tblClientTemp 
	where	RowNum BETWEEN @min AND @max
	order by id desc 
 END
