USE [SEODB]
GO
/****** Object:  StoredProcedure [dbo].[spKeywordPlanner_GetAllWithFilterToView]    Script Date: 05/19/2016 09:24:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[spKeywordPlanner_GetAllWithFilterToView] --'web chat',-1,'',-1,0,2
	@KeywordName nvarchar(255),
	@AvgMonthlySearches bigint,
	@Competition nvarchar(255),
	@SuggestedBid decimal(10,2),
	@KeywordPlanerStatus tinyint,
	@userid bigint
as
begin
		select tblKeywordPlanner.KeywordPlannerId,
	 tblKeywordPlanner.KeywordName,
	 N'vnđ' as FormatCurrency,
	 case when IsPublicToPartner=1 then AvgMonthlySearches else 0 end as AvgMonthlySearches,
	 case when IsPublicToPartner=1 then SetupCost else 0 end as SetupCost,
	 case when IsPublicToPartner=1 then ChargesForMaintenance else 0 end as ChargesForMaintenance,
	 case when IsPublicToPartner=1 then Competition else '' end as Competition,
	 case when IsPublicToPartner=1 then '' else tblKeywordPlanner.Note end as Note
	from tblKeywordPlanner
	inner join tblDetailBlockInsertPlanner on tblKeywordPlanner.KeywordPlannerId=tblDetailBlockInsertPlanner.KeywordPlannerId
	inner join tblBlockInsertKeywordPlanner on tblDetailBlockInsertPlanner.BlockID=tblBlockInsertKeywordPlanner.BlockID
	where (KeywordName=@KeywordName or @KeywordName='')
	and (AvgMonthlySearches=@AvgMonthlySearches or @AvgMonthlySearches=-1)
	and (Competition=@Competition or @Competition='')
	and (SuggestedBid=@SuggestedBid or @SuggestedBid=-1)
	and (KeywordPlanerStatus=@KeywordPlanerStatus or @KeywordPlanerStatus=0)
	and tblBlockInsertKeywordPlanner.UserId=@userid
	order by KeywordName desc
	
	/*
		select tblKeywordPlanner.KeywordPlannerId,
	 tblKeywordPlanner.KeywordName,
	 N'vnđ' as FormatCurrency,
	 --case when ((len(KeywordName) - len(replace(KeywordName, ' ', '')))<2) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=2 and AvgMonthlySearches>3000) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=3 and AvgMonthlySearches>9000) then '' else FormatCurrency end as FormatCurrency,
	 case when ((len(KeywordName) - len(replace(KeywordName, ' ', '')))<2) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=2 and AvgMonthlySearches>3000) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=3 and AvgMonthlySearches>9000)then 0 else AvgMonthlySearches end as AvgMonthlySearches,
	 case when ((len(KeywordName) - len(replace(KeywordName, ' ', '')))<2) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=2 and AvgMonthlySearches>3000) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=3 and AvgMonthlySearches>9000)then 0 else SetupCost end as SetupCost,
	 case when ((len(KeywordName) - len(replace(KeywordName, ' ', '')))<2) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=2 and AvgMonthlySearches>3000) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=3 and AvgMonthlySearches>9000)then 0 else ChargesForMaintenance end as ChargesForMaintenance,
	 case when ((len(KeywordName) - len(replace(KeywordName, ' ', '')))<2) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=2 and AvgMonthlySearches>3000) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=3 and AvgMonthlySearches>9000)then '' else Competition end as Competition,
	 case when ((len(KeywordName) - len(replace(KeywordName, ' ', '')))<2) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=2 and AvgMonthlySearches>3000) or ((len(KeywordName) - len(replace(KeywordName, ' ', '')))=3 and AvgMonthlySearches>9000)then N'Liên hệ lại với đối tác để biết chi tiết' else isnull(tblKeywordPlanner.Note,'') end as Note
	from tblKeywordPlanner
	inner join tblDetailBlockInsertPlanner on tblKeywordPlanner.KeywordPlannerId=tblDetailBlockInsertPlanner.KeywordPlannerId
	inner join tblBlockInsertKeywordPlanner on tblDetailBlockInsertPlanner.BlockID=tblBlockInsertKeywordPlanner.BlockID
	where (KeywordName=@KeywordName or @KeywordName='')
	and (AvgMonthlySearches=@AvgMonthlySearches or @AvgMonthlySearches=-1)
	and (Competition=@Competition or @Competition='')
	and (SuggestedBid=@SuggestedBid or @SuggestedBid=-1)
	and (KeywordPlanerStatus=@KeywordPlanerStatus or @KeywordPlanerStatus=0)
	and tblBlockInsertKeywordPlanner.UserId=@userid
	order by KeywordName desc
	*/
end
