use SEODB
go
alter procedure [dbo].[spBlockInsertKeywordPlanner_AddWithImport]
	@BlockName nvarchar(255),
	@userid bigint
as
begin

	insert into [tblBlockInsertKeywordPlanner] (BlockName,UserId)
	values (@BlockName,@userid)
	
	select @@IDENTITY
end