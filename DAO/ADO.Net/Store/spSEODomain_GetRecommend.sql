use SEODB

go
/*store lấy đề nghị để sale nhanh chóng*/
alter procedure [dbo].[spSEODomain_GetRecommend] --'hoangnguyen.edu.vn',2
	@domainName nvarchar(max),
	@userid bigint
as
begin
	select isnull(tblSEODomain.Recommendations,'') as Recommendations from tblSEODomain
	inner join tblUser on tblSEODomain.UserId=tblUser.UserId
	where tblUser.UserId=@userid
	and DomainName=@domainName
end