USE [SEODB]
GO
/****** Object:  StoredProcedure [dbo].[spSEODomain_GetList]    Script Date: 03/04/2016 14:07:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spSEODomain_GetList] 1,0,'', 0, 0, '', 0, '', 0, ''
	--@pagesize int, 
	--@pagenum int,
	@seopartneraccountid bigint,
	@seodomainidonpartner bigint,
	@domainname nvarchar(1024),
	@totalseokeyword int,
	@totalseokeywordontop int,
	@linkquerykeyword nvarchar(1024),
	@seodomainstatus tinyint,
	@seodomainstatusstr nvarchar(255),
	@shortcreateddate int,
	@note nvarchar(1024)
AS 
BEGIN 
	select *
	from tblSEODomain
	where
		( @seopartneraccountid = 0	or @seopartneraccountid=SEOPartnerAccountId )  and 
		( @seodomainidonpartner = 0	or @seodomainidonpartner=SEODomainIdOnPartner )  and 
		( @domainname = ''	or @domainname=DomainName )  and 
		( @totalseokeyword = 0	or @totalseokeyword=TotalSEOKeyword )  and 
		( @totalseokeywordontop = 0	or @totalseokeywordontop=TotalSEOKeywordOnTop )  and 
		( @linkquerykeyword = ''	or @linkquerykeyword=LinkQueryKeyword )  and 
		( @seodomainstatus = 0	or @seodomainstatus=SEODomainStatus )  and 
		( @seodomainstatusstr = ''	or @seodomainstatusstr=SEODomainStatusStr )  and 
		( @shortcreateddate = 0	or @shortcreateddate=ShortCreatedDate )  and 
		( @note = ''	or @note=Note )   
	order by SEODomainId desc 
 END