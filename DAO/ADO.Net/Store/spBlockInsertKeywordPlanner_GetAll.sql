use SEODB
go
alter procedure [dbo].[spBlockInsertKeywordPlanner_GetAll]
as
begin
	select keyword.*,tblcross.TotalKeyword from tblBlockInsertKeywordPlanner as keyword
	cross apply
	(
		select COUNT(*) as TotalKeyword from tblDetailBlockInsertPlanner
		where BlockID=keyword.BlockID
	)as tblcross
	order by BlockID desc
end