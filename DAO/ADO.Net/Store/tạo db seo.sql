use SEODB
drop table [tblSEODomain]
create table [dbo].[tblSEODomain]
(
	SEODomainId bigint primary key identity,
	SEODomainIdOnPartner bigint not null,
	SEOPartnerAccountId bigint not null,
	DomainName nvarchar(1024) not null,
	TotalSEOKeyword int not null,
	TotalSEOKeywordOnTop int not null,
	LinkQueryKeyword nvarchar(1024),
	SEODomainStatus tinyint not null,
	SEODomainStatusStr nvarchar(255),
	CreatedDate datetime default GEtdate(),
	UpdatedDate datetime default GEtdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112),
	Note nvarchar(1024)
)
insert into tblSEODomain
values(0,1,'thammy.benhvienvanhanh.com',0,0,null,1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(0,1,'hoangnguyen.edu.vn',0,0,null,1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(0,1,'hocthietkewebsite.net',0,0,null,1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(0,1,'www.haravan.com',0,0,null,1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)

alter table [tblSEODomain]
add UserId bigint null

alter table [tblSEODomain]
add Recommendations nvarchar(max) null

select * from tblSEODomain


drop table [tblSEOKeyword]
create table [dbo].[tblSEOKeyword]
(
	SEOKeywordId bigint primary key identity,
	SEOKeywordIdOnPartner bigint  ,
	SEODomainId bigint not null,
	SEOKeywordName nvarchar(1024) not null,
	PricePerWeekOnPartner int not null,
	Price int not null,
	RankOnGoogle int not null,
	LinkCheck nvarchar(1024),
	SEOKeywordStatus tinyint not null,
	SEOKeywordStatusStr nvarchar(255),
	CreatedDate datetime default GEtdate(),
	UpdatedDate datetime default GEtdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112),
	Note nvarchar(1024)
)
insert into tblSEOKeyword
values (null,2,N'hoc seo web o dau tot',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,2,N'tim cho hoc seo tai tphcm',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,2,N'seo la gi',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,3,N'hoc lap trinh web php',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,3,N'hoc thiet ke web',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,3,N'khoa hoc lap trinh web php',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,3,N'khoa hoc thiet ke web',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,4,N'cong ty thiet ke web chuyen nghiep tphcm',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,4,N'cong ty thiet ke web dep gia re tphcm',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,4,N'cong ty thiet ke web dep nhat tphcm',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,4,N'cong ty thiet ke web gia re tphcm',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,4,N'cong ty thiet ke web uy tin tphcm',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,4,N'thiet ke website ban hang gia re tphcm',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,4,N'cong ty thiet ke web nao tot tphcm',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)
,(null,4,N'dich vu thiet ke web gia re uy tin tphcm',0,0,0,'',1,null,GETDATE(),GETDATE(),convert(varchar(8),getdate(),112),null)

select * from tblSEOKeyword


drop table [tblLogRefreshSEO]
create table [dbo].[tblLogRefreshSEO]
(
	LogRefeshSEOID bigint primary key identity,
	PartnerAccountId bigint not null,
	ObjectID bigint not null,
	StatusRefesh tinyint not null,
	LinkGetData nvarchar(1024) not null,
	DataRefesh nvarchar(1024) not null,
	CreatedDate datetime default GEtdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112),
	Note nvarchar(1024)
)

select * from tblLogRefreshSEO

drop table [tblUser]
create table [dbo].[tblUser]
(
	UserId bigint primary key identity,
	UserName varchar(255) not null,
	UserPass varchar(50) not null,
	FullName nvarchar(255) not null,
	Email nvarchar(255) not null,
	Phone varchar(50) not null,
	[Address] nvarchar(1024) not null,
	[Status]tinyint not null,
	Balance int not null,
	CreatedByAccountId bigint not null,
	CreatedDate datetime default GEtdate(),
	UpdatedDate datetime default GEtdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112),
	Note nvarchar(1024)
)
drop table [tblAccount]
create table [dbo].[tblAccount]
(
	AccountId bigint primary key identity,
	UserName varchar(255) not null,
	UserPass varchar(50) not null,
	FullName nvarchar(255) not null,
	Email nvarchar(255) not null,
	Phone varchar(50) not null,
	[Address] nvarchar(1024) not null,
	[Status] tinyint not null,
	CreatedDate datetime default GEtdate(),
	UpdatedDate datetime default GEtdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112),
	Note nvarchar(1024)
)

select * from tblAccount

drop table [tblUser_SEODomain]
create table [dbo].[tblUser_SEODomain]
(
	ID bigint primary key identity,
	UserId bigint not null,
	SEODomainId bigint not null,
	[Status] tinyint not null,
	CreatedDate datetime default GEtdate(),
	UpdatedDate datetime default GEtdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112),
	Note nvarchar(1024)
)

drop table [tblSEOPartnerAccount]
create table [dbo].[tblSEOPartnerAccount]
(
	SEOPartnerAccountId bigint primary key identity,
	UserName nvarchar(255) not null,
	[Password] nvarchar(50) not null,
	Email nvarchar(255),
	Phone varchar(50),
	Balance int,
	CreatedDate datetime default GEtdate(),
	UpdatedDate datetime default GEtdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112),
	Note nvarchar(1024)
)

insert into tblSEOPartnerAccount
values ('M201602265336','M201602265336','chinh.nc@fibo.vn','01656244909',20000000,GETDATE(),GETDATE(),CONVERT(varchar(8),GETDATE(),112),'')

create table [dbo].[tblUser_AspNetUsers]
(
	Id bigint primary key identity,
	UserId bigint,
	AspNetUsersId nvarchar(max),
	[Status] tinyint,
	CreatedDate datetime default GEtdate(),
	UpdatedDate datetime default GEtdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112),
	Note nvarchar(1024)
)

insert into [tblUser_AspNetUsers] (UserId,AspNetUsersId,[Status])
values (2,N'c46588bf-7898-4a9d-9dfc-c3b3282dadb1',1)

select * from tblSEOPartnerAccount

select * from tblUser_SEODomain

select * from tblUser

select * from tblSEODomain

select * from AspNetUsers 

select * from tblUser_AspNetUsers

select * from tblSEOKeyword


