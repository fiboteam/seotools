﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Net;
using System.Net.Http;

namespace SEOWebAPIv40.Security
{
	public class MyAuthorize : AuthorizeAttribute
	{
		private const string BasicAuthResponcesHeader = "WWW-Authenticate";
		private const string BasicAuthResponcesHeaderValue = "Basic";

		public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
		{
			try
			{
				AuthenticationHeaderValue authValue = actionContext.Request.Headers.Authorization;
				if(authValue!=null && !string.IsNullOrWhiteSpace(authValue.Parameter) && authValue.Scheme==BasicAuthResponcesHeaderValue)
				{
					Credential parseCredentials = ParseAuthorizationHeader(authValue.Parameter);

					var myPrincipal = new Principal(parseCredentials.UserName, parseCredentials.Password);
					if(!myPrincipal.IsInRole(Roles))
					{
						actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
						actionContext.Response.Headers.Add(BasicAuthResponcesHeader, BasicAuthResponcesHeaderValue);
						return;
					}

				}
				else
				{
					actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
					actionContext.Response.Headers.Add(BasicAuthResponcesHeader, BasicAuthResponcesHeaderValue);
					return;
				}
			}
			catch(Exception ex)
			{
				actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
				actionContext.Response.Headers.Add(BasicAuthResponcesHeader, BasicAuthResponcesHeaderValue);
			}
		}

		private Credential ParseAuthorizationHeader(string authHeader)
		{
			string[] creadentials = Encoding.ASCII.GetString(Convert.FromBase64String(authHeader)).Split(new[] { ':' });
			if(creadentials.Length!=2||string.IsNullOrEmpty(creadentials[0]) || string.IsNullOrEmpty(creadentials[1]))
			{
				return null;
			}
			return new Credential() { UserName = creadentials[0], Password = creadentials[1] };
		}
	}
}