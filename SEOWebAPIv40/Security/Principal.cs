﻿using DAO;
using DAO.Interface;
using SEOWebAPIv40.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using Utility;

namespace SEOWebAPIv40.Security
{
	public class Principal : IPrincipal
	{
		private readonly IUser _userRepository = DataAccess.UserDao;

		private UserLogin _userLogin;

		public Principal(string username,string password)
		{
			this.Identity = new GenericIdentity(username);
			_userLogin.User = _userRepository.CheckLogin(username, GFunction.GetMD5(password));
		}

		public IIdentity Identity
		{
			get;
			set;
		}

		public bool IsInRole(string role)
		{
			if (_userLogin.User==null)
			{
				return false;
			}
			
			return true;
		}
	}
}