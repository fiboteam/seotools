﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DTO.Objects;

namespace SEOWebAPIv40.Models
{
	public class UserModel
	{
		public long ID { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
	}

	public class UserLogin
	{
		public string UserName { get; set; }
		public string Password { get; set; }

		public User User { get; set; }
	}
}