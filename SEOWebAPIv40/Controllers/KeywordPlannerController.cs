﻿using DAO;
using DAO.Interface;
using DTO.Objects;
using SEOWebAPIv40.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Utility;
using SEOWebAPIv40.Security;
using System.Web.Http.WebHost;
using System.Text.RegularExpressions;

namespace SEOWebAPIv40.Controllers
{
    public class KeywordPlannerController : ApiController
    {
		private readonly IKeywordPlanner _keywordPlannerRepository = DataAccess.KeywordPlannerDao;
		private readonly IBlockInsertKeywordPlanner _blockRepository = DataAccess.BlockInsertKeywordPlannerDao;

		//[MyAuthorize(Roles="customer")]
		[AllowAnonymous]
		public IEnumerable<KeywordPlannerModel> Get(int id)
		{
			var list = _keywordPlannerRepository.GetAllWithBlockId(id, 2);
			List<KeywordPlannerModel> listModel = new List<KeywordPlannerModel>();
			if (list != null)
			{
				return list.Select(ob => new KeywordPlannerModel() { KeywordName = ob.KeywordName, CurrentFormat = ob.FormatCurrency, SetupPrice = ob.SetupCost, ChargesForMaintenance = ob.ChargesForMaintenance, Note = ob.Note }).ToList();
			}
			else
			{
				return null;
			}

			//return listModel;
		}
		[HttpGet]
		public KeywordPlannerModel FindKeyword(string keyword)
		{
			KeywordPlanner ob = new KeywordPlanner();
			ob.KeywordName = keyword;
			ob.AvgMonthlySearches = -1;
			ob.Competition = "";
			ob.SuggestedBid = -1;
			ob.KeywordPlanerStatus = (KeywordPlanerStatus)Enum.ToObject(typeof(KeywordPlanerStatus), 0);
			ob.UserId = 2;

			var list = _keywordPlannerRepository.GetAllWithFilter(ob);
			//List<KeywordPlannerModel> listModel = new List<KeywordPlannerModel>();
			if (list != null)
			{
				return list.Select(o => new KeywordPlannerModel() { KeywordName = o.KeywordName, CurrentFormat = o.FormatCurrency, SetupPrice = o.SetupCost, ChargesForMaintenance = o.ChargesForMaintenance, Note = o.Note }).FirstOrDefault();
			}
			else
			{
				return null;
			}

			//return listModel;
		}

		[HttpGet]
		public int ProgressPercent(long blockId)
		{
			if (blockId > 0)
			{
				var percent = _keywordPlannerRepository.GetProgressByBlockId(blockId, 2);
				return percent;
			}
			else
			{
				return -1;
			}
		}

		public long Post(IEnumerable<string> listKeyword)
		{
			try
			{
				List<string> list = listKeyword.ToList();
				if (listKeyword != null && listKeyword.Count()>0)
				{
					long blockId = _blockRepository.InsertWithImport(DateTime.Now.ToString("yyyyMMdd-HHmmss"), 2);
					if (blockId > 0)
					{
						try
						{
							Task.Factory.StartNew(() =>
							{
								ParallelOptions options = new ParallelOptions() { MaxDegreeOfParallelism = 2 };
								Parallel.For(0, list.Count, options, (i) =>
								{
									KeywordPlanner ob = new KeywordPlanner();

									ob.KeywordName = list[i].TrimEnd().TrimStart();
									if (!string.IsNullOrEmpty(ob.KeywordName) && IsValidKeywordName(ob.KeywordName))
									{
										_keywordPlannerRepository.InsertWithImport(ob, blockId);
									}
								});
							});
							
						}
						catch
						{
						}

					}

					return blockId;
				}
				else
				{
					return -1;
				}

			}
			catch
			{
				return -2;
			}
		}

		private bool IsValidKeywordName(string keyword)
		{
			try
			{
				if (string.IsNullOrEmpty(keyword))
				{
					return false;
				}

				string partern = @"^[\w\s]+$";
				if (Regex.IsMatch(keyword, partern))
				{
					return true;
				}
				return false;
			}
			catch
			{
				return false;
			}
		}
    }
}
