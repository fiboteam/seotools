﻿using DAO;
using DAO.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SEOServices.Code;
using DTO.Objects;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.POIFS.FileSystem;
using System.IO;
using System.Diagnostics;
using Utility;
using System.Threading.Tasks;
using System.Net.Http;
using SEOServices.Models;
using System.Net;
using System.Net.Http.Headers;
using DAO.Entity;
using DAO.ADO.Net.Entities;

namespace SEOServices
{
	public class KeywordPlannerService
	{
		private readonly IKeywordPlanner _keywordPlannerRepository = DataAccess.KeywordPlannerDao;
		private readonly KeywordPlannerEFDao _keywordPlannerEFRepository = new KeywordPlannerEFDao();
		HttpClientHandler handler = new HttpClientHandler
		{
			Credentials = new
				System.Net.NetworkCredential("customer1001", GFunction.GetMD5("123456"))
		};

		public List<KeywordPlannerViewModel> GetAllBy(long blockId)
		{
			try
			{
				var list = _keywordPlannerRepository.GetAllWithBlockId(blockId,2);
				//var list=GetData(blockId);
				//List<KeywordPlannerModel> result;
				//using (var httpClient = new HttpClient())
				//{
				//	var response = httpClient.GetAsync("http://localhost:39164/api/seo/4").Result;
				//	//result=response.Content.ReadAs
				//}

				if (list != null)
				{
					List<KeywordPlannerViewModel> result = new List<KeywordPlannerViewModel>();
					Parallel.For(0, list.Count,new ParallelOptions() { MaxDegreeOfParallelism = 1 }, i =>
					{
						var model = list[i].ConvertToViewModelWithExtenstionProperties<KeywordPlannerViewModel>();
						result.Add(model);
					});

					return result;
				}
				else
				{
					return null;
				}

				//if(list!=null)
				//{
				//	List<KeywordPlannerViewModel> result = new List<KeywordPlannerViewModel>();
				//	Parallel.For(0, list.Count, new ParallelOptions() { MaxDegreeOfParallelism = 1 }, i =>
				//	{
				//		KeywordPlannerViewModel key = new KeywordPlannerViewModel();
				//		key.KeywordName = list[i].KeywordName;
				//		key.ID = list[i].ID;
				//		key.FormatCurrency = list[i].CurrentFormat;
				//		key.Competition = list[i].Competition;
				//		key.ChargesForMaintenance = list[i].ChargesForMaintenance;
				//		key.Note = list[i].Note;
				//		key.SetupCost = list[i].SetupPrice;
				//		//var model = list[i].ConvertToViewModelWithExtenstionProperties<KeywordPlannerViewModel>();
				//		result.Add(key);
				//	});

				//	return result;
				//}
				//else
				//{
				//	return null;
				//}
			}
			catch
			{
				return null;
			}
		}

		public List<KeywordPlannerModel> GetData(long blockId)
		{
			// Define the httpClient object            using (HttpClient client = new HttpClient())
			
				List<KeywordPlannerModel> list = null;
			using (HttpClient client = new HttpClient())
			{
				HttpResponseMessage response = client.GetAsync("http://localhost:39164/api/keywordplanner/" + blockId).Result;
				if (response.IsSuccessStatusCode)
				{
					// Convert the result into business object
					list=response.Content.ReadAsAsync<IEnumerable<KeywordPlannerModel>>().Result.ToList();
				}
				else
				{

				}
			}
			return list;

		}

		public bool Add(List<string> listKeyword)
		{
			try
			{

				using (HttpClient client=new HttpClient())
				{
					// Set the header so it knows we are sending JSON.
					//client.con = "application/json";
					//var response = client.PostAsync("http://localhost:39164/api/keywordplanner", content).Result;
					client.BaseAddress = new Uri("http://localhost:39164");
					//var response = client.PostAsync("api/keywordplanner", listKeyword,).Result; 
					//var response = client.PostAsJsonAsync("api/keywordplanner", listKeyword).Result;

					client.DefaultRequestHeaders.Accept.Clear();
					client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

					var response = client.PostAsJsonAsync("api/keywordplanner", listKeyword).Result;

					if (response.IsSuccessStatusCode)
					{
						if (response.Content.ReadAsAsync<long>().Result>0)
						{
							return true;
						}
						
					}
					else
					{
						
					}
				}
				return false;
			}
			catch
			{
				return false;
			}
		}

		public List<KeywordPlannerViewModel> GetAllBy(KeywordPlannerViewModel model)
		{
			try
			{
				KeywordPlanner ob = model.ConvertToBusinessObject<KeywordPlanner>();
				var list = _keywordPlannerRepository.GetAllWithFilter(ob);
				if (list != null)
				{
					List<KeywordPlannerViewModel> result = new List<KeywordPlannerViewModel>();
					//foreach (var item in list)
					//{
					//	var md = item.ConvertToViewModelWithExtenstionProperties<KeywordPlannerViewModel>();
					//	result.Add(md);
					//}
					Parallel.For(0, list.Count, i =>
					{
						var md = list[i].ConvertToViewModelWithExtenstionProperties<KeywordPlannerViewModel>();
						result.Add(md);
					});
					return result;
				}
				else
				{
					return null;
				}
			}
			catch
			{
				return null;
			}
		}

		public List<KeywordPlannerViewModel> GetAllInFileXLSX(string filepath)
		{
			try
			{
				List<KeywordPlannerViewModel> list = new List<KeywordPlannerViewModel>();
				XSSFWorkbook xssfWorkbook;
				using (FileStream file = new FileStream(filepath, FileMode.Open, FileAccess.Read))
				{
					//xssfWorkbook = new XSSFWorkbook(file);
					//var docs= new POIFSFileSystem(file);
					xssfWorkbook = new XSSFWorkbook(file);
				}

				ISheet sheet = xssfWorkbook.GetSheetAt(0);
				System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
				IRow row=null;
				ICell cellKeyword;
				string keyword;
				while (rows.MoveNext())
				{
					row = (XSSFRow)rows.Current;
					if (row != null)
					{
						cellKeyword = row.GetCell(0);
						keyword = cellKeyword != null ? cellKeyword.StringCellValue.TrimEnd().TrimStart() : string.Empty;
						if (!string.IsNullOrEmpty(keyword))
						{
							list.Add(new KeywordPlannerViewModel() { KeywordName = keyword });
						}
					}
				}

				return list;
			}
			catch
			{
				return null;
			}
		}

		public bool InsertWithImport(KeywordPlannerViewModel model,long blockid)
		{
			KeywordPlanner ob = new KeywordPlanner();
			ob.KeywordName = model.KeywordName;
			return _keywordPlannerRepository.InsertWithImport(ob, blockid);
		}

		public List<string> GetAllKeywordPlannerNameToRefresh()
		{
			var keywordList= _keywordPlannerRepository.GetAllKeywordPlannerNameToRefresh();
			if (keywordList != null)
			{
				return keywordList.Select(keyword => keyword.KeywordName).ToList();
			}
			else
			{
				return null;
			}
		}

		public bool UpdateFromRefresh(KeywordPlannerViewModel keywordPlannerViewModel)
		{
			KeywordPlanner ob=keywordPlannerViewModel.ConvertToBusinessObject<KeywordPlanner>();
			return _keywordPlannerRepository.UpdateFromRefresh(ob);
		}

		public void OpenFileInExplorer(string fullpath)
		{
			string windir = Environment.GetEnvironmentVariable("windir");
			if (string.IsNullOrEmpty(windir.Trim()))
			{
				windir = "C:\\Windows\\";
			}
			if (!windir.EndsWith("\\"))
			{
				windir += "\\";
			}

			FileInfo fileToLocate = null;
			fileToLocate = new FileInfo(fullpath);

			ProcessStartInfo pi = new ProcessStartInfo(windir + "explorer.exe");
			pi.Arguments = "/select, \"" + fileToLocate.FullName + "\"";
			pi.WindowStyle = ProcessWindowStyle.Normal;
			pi.WorkingDirectory = windir;

			//Start Process
			Process.Start(pi);
		}

		public bool CreateFileXLSX(long _id, string filepath, string sheetName)
		{
			try
			{
				List<KeywordPlanner> list = null;
				if(_id>0)
				{
					list = _keywordPlannerRepository.GetAllWithBlockId(_id,2).ToList();
				}
				else if(_id==0)
				{
					KeywordPlanner model = new KeywordPlanner();
					model.KeywordName = "";
					model.AvgMonthlySearches = -1;
					model.Competition = "";
					model.SuggestedBid = -1;
					model.KeywordPlanerStatus = (KeywordPlanerStatus)Enum.ToObject(typeof(KeywordPlanerStatus), 0);
					list = _keywordPlannerRepository.GetAllWithFilter(model).ToList();
				}
				
				var workbook = new XSSFWorkbook();

				var sheet = workbook.CreateSheet(sheetName);
				IDataFormat dataFormatCustom = workbook.CreateDataFormat();

				var datetimeCellStyle = workbook.CreateCellStyle();
				ICreationHelper createHelper = workbook.GetCreationHelper();
				short dateFormat = createHelper.CreateDataFormat().GetFormat("yyyy-MM-dd HH:mm:ss");
				datetimeCellStyle.DataFormat = dateFormat;

				var currencyCellStyle = workbook.CreateCellStyle();
				ICreationHelper currencyHelper = workbook.GetCreationHelper();
				short currencyFormat = currencyHelper.CreateDataFormat().GetFormat("#,##");
				currencyCellStyle.DataFormat = currencyFormat;
				currencyCellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
				currencyCellStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

				// Create the style object
				var centerCellStyle = workbook.CreateCellStyle();
				centerCellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
				centerCellStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

				//// Create the style object
				//var fullcellCellStyle = workbook.CreateCellStyle();
				////fullcellCellStyle.wi

				// Add header labels
				var rowIndex = 0;
				int cellIndex = 0;
				var row = sheet.CreateRow(rowIndex);
				row.CreateCell(cellIndex).SetCellValue("Từ khoá");

				cellIndex++;
				
				//row.CreateCell(cellIndex).SetCellValue("Số lần tìm trung bình hàng tháng");

				//cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Cạnh tranh");

				cellIndex++;
				//row.CreateCell(cellIndex).SetCellValue("Giá thầu được đề suất");

				//cellIndex++;
				//row.CreateCell(cellIndex).SetCellValue("Tỉ lệ click");

				//cellIndex++;
				//row.CreateCell(cellIndex).SetCellValue("Tổng chi phí 1 tháng");

				//cellIndex++;
				//row.CreateCell(cellIndex).SetCellValue("SEO Fibo");

				//cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Phí duy trì");

				cellIndex++;
				//row.CreateCell(cellIndex).SetCellValue("Setup");

				//cellIndex++;
				//row.CreateCell(cellIndex).SetCellValue("Chỉ số làm khó");

				//cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Giá setup");

				cellIndex++;
				//row.CreateCell(cellIndex).SetCellValue("Ngày update");

				//cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Mệnh giá");

				cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Ghi chú");

				cellIndex++;
				rowIndex++;
				// Add data rows
				foreach (KeywordPlanner keyword in list)
				{
					int cell = 0;
					row = sheet.CreateRow(rowIndex);
					row.CreateCell(cell).SetCellValue(keyword.KeywordName);
					cell++;
					//row.CreateCell(cell).SetCellValue(keyword.AvgMonthlySearches);
					//row.Cells[cell].CellStyle = currencyCellStyle;
					//cell++;
					row.CreateCell(cell).SetCellValue(keyword.Competition);
					row.Cells[cell].CellStyle = centerCellStyle;
					cell++;
					//row.CreateCell(cell).SetCellValue(keyword.CompetitionIndex);
					//row.Cells[cell].CellStyle = centerCellStyle;
					//cell++;
					//row.CreateCell(cell).SetCellValue(keyword.SuggestedBid);
					//row.Cells[cell].CellStyle = currencyCellStyle;
					//cell++;
					//row.CreateCell(cell).SetCellValue(keyword.ClickRate);
					//cell++;
					//row.CreateCell(cell).SetCellValue(keyword.TotalCostPerMonth);
					//row.Cells[cell].CellStyle = currencyCellStyle;
					//cell++;
					//row.CreateCell(cell).SetCellValue(keyword.SEOFibo);
					//row.Cells[cell].CellStyle = currencyCellStyle;
					//cell++;
					row.CreateCell(cell).SetCellValue(keyword.ChargesForMaintenance);
					row.Cells[cell].CellStyle = currencyCellStyle;
					cell++;
					//row.CreateCell(cell).SetCellValue(keyword.Setup);
					//row.Cells[cell].CellStyle = currencyCellStyle;
					//cell++;
					//row.CreateCell(cell).SetCellValue(keyword.HardWorkIndex);
					//cell++;
					row.CreateCell(cell).SetCellValue(keyword.SetupCost);
					row.Cells[cell].CellStyle = currencyCellStyle;
					cell++;
					//row.CreateCell(cell).SetCellValue(keyword.UpdatedDate);
					//row.Cells[cell].CellStyle = datetimeCellStyle;
					//cell++;
					row.CreateCell(cell).SetCellValue(keyword.FormatCurrency);
					row.Cells[cell].CellStyle = centerCellStyle;
					cell++;
					row.CreateCell(cell).SetCellValue(keyword.Note);
					//row.Cells[cell].CellStyle = centerCellStyle;
					cell++;
					rowIndex++;
				}
				//int indexcolumn = 0;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				// Save the Excel spreadsheet to a file on the web server's file system
				using (var fileData = new FileStream(filepath, FileMode.Create))
				{
					workbook.Write(fileData);
				}
				//workbook.Write()
				return true;
			}
			catch
			{
				return false;
			}
		}

		public tblKeywordPlanner GetSingle(long id)
		{
			try
			{
				return _keywordPlannerEFRepository.GetSingle(id);
			}
			catch
			{
				return null;
			}
		}

		public bool UpdateByAdmin(long loginId,long id, long chargeForMaintenance, long setUpCost, string note, bool isPublic)
		{
			try
			{
				return _keywordPlannerEFRepository.UpdateByAdmin(loginId,id, chargeForMaintenance,setUpCost,note,isPublic);
			}
			catch
			{
				return false;
			}
		}
	}
}
