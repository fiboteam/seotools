﻿using SEOViewerTool.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SEOServices.Models
{
    public class Model
    {
        public Model()
        {
			ID = -1;
            Paging = new Paging();
        }
		[Browsable(false)]
        public long ID { get; set; }
		[Browsable(false)]
        public Paging Paging { get; set; }
		[Browsable(false)]
        public string  FromD { get; set; }
		[Browsable(false)]
        public string ToD { get; set; }
		[Browsable(false)]
        public int CurrentPage { get; set; }
		[DisplayName("Ngày tạo")]
		[Browsable(false)]
		public DateTime? CreatedDate { get; set; }
		[DisplayName("Ngày update")]
		[DisplayFormat(DataFormatString = "HH:mm:ss yyyy-MM-dd"), Display(Name = "Date 2")]

		[Browsable(false)]
		public DateTime? UpdatedDate { get; set; }
		[DisplayName("Từ ngày")]
		[Browsable(false)]
        public DateTime? FromDate {
            get
            {
                if (string.IsNullOrEmpty(FromD))
                    FromD = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
				IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
                return DateTime.ParseExact(FromD, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
        }
		[DisplayName("Đến ngày")]
		[Browsable(false)]
        public DateTime? ToDate
        {
            get
            {
                if(string.IsNullOrEmpty(ToD))
                    ToD = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
				IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
                return DateTime.ParseExact(ToD, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1);
            }
        }

    }
}