﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SEOServices.Models
{
	public class BlockKeywordPlannerViewModel:Model
	{
		public string BlockName { get; set; }

		public string Note { get; set; }

		public string Remark { get; set; }

		public int ShortCreatedDate { get; set; }

		public int TotalKeyword { get; set; }
	}
}
