﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Utility;

namespace SEOServices.Models
{
	public class SEOKeywordViewModel:Model
	{
		[Browsable(false)]
		public long SEODomainId { get; set; }
		[DisplayName("Tên miền")]
		public string DomainName { get; set; }
		[Browsable(false)]
		public string LinkCheck { get; set; }
		[DisplayName("Trạng thái tên miền")]
		public SEODomainStatus SEODomainStatus { get; set; }
		[Browsable(false)]
		public string SEODomainStatusStr { get; set; }
		[DisplayName("Từ khoá")]
		public string SEOKeywordName { get; set; }
		[DisplayName("Trạng thái từ khoá")]
		public SEOKeywordStatus SEOKeywordStatus { get; set; }
		[DisplayName("Giá trên partner")]
		public int PricePerWeekOnPartner { get; set; }
		[Browsable(false)]
		public int Price { get; set; }
		[DisplayName("Thứ hạn trên Google")]
		public int RankOnGoogle { get; set; }
		[Browsable(false)]
		public string SEOKeywordStatusStr { get; set; }
	}
}
