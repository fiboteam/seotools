﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Text;
using Utility;

namespace SEOServices.Models
{
	public class KeywordPlannerViewModel:Model
	{
		[DisplayName("Keyword")]
		public string KeywordName { get; set; }
		[DisplayName("Mệnh giá")]
		public string FormatCurrency { get; set; }
		[DisplayName("Số lần tìm trung bình hàng tháng")]
		[DataType(DataType.Currency)]
		//[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:C0}")]
		[Browsable(false)]
		public long AvgMonthlySearches { get; set; }
		[DisplayName("Cạnh tranh")]
		[Browsable(false)]
		public float CompetitionIndex { get; set; }
		[DisplayName("Giá thầu đề xuất")]
		[Browsable(false)]
		public float SuggestedBid { get; set; }
		[DisplayName("Tỷ lệ click")]
		[Browsable(false)]
		public float ClickRate { get; set; }
		[DisplayName("Tổng chi phi 1 tháng")]
		[Browsable(false)]
		public long TotalCostPerMonth { get; set; }
		[DisplayName("SEO Fibo")]
		[Browsable(false)]
		public long SEOFibo { get; set; }
		[DisplayName("Phí duy trì")]
		public long ChargesForMaintenance { get; set; }
		[DisplayName("Setup")]
		[Browsable(false)]
		public long Setup { get; set; }
		[DisplayName("Chỉ số làm khó")]
		[Browsable(false)]
		public float HardWorkIndex { get; set; }
		[DisplayName("Chỉ số setup")]
		[Browsable(false)]
		public short CompetitionInt { get; set; }
		[DisplayName("Giá setup")]
		public long SetupCost { get; set; }
		[DisplayName("Cạnh tranh")]
		public string Competition { get; set; }
		[Browsable(false)]
		public KeywordPlanerStatus KeywordPlanerStatus { get; set; }
		[DisplayName("Ghi chú")]
		public string Note { get; set; }
		[Browsable(false)]
		public string Remark { get; set; }
		[Browsable(false)]
		public int ShortCreatedDate { get; set; }
		[Browsable(false)]
		public short IsPublicToPartner { get; set; }

		public KeywordPlannerViewModel()
		{
			try
			{
				ClickRate = float.Parse(ConfigurationManager.AppSettings.Get("ClickRate").ToString());
			}
			catch
			{
				ClickRate = 0;
			}
		}

		public readonly SpecialKeywordService _specialService = new SpecialKeywordService();
		/// <summary>
		/// tinh1 cac gia tri tu data refresh dc api google
		/// </summary>
		public void CreateValueFromDataRefresh()
		{
			try
			{
				var specialKeyword = _specialService.GetSpecialKeywordContainsValue(KeywordName);
				if (specialKeyword!=null)
				{
					//tinh CompetitionIndex
					switch (Competition.ToLower())
					{
						default:
						case "low":
							CompetitionInt = 4;
							break;
						case "medium":
							CompetitionInt = 7;
							break;
						case "high":
							CompetitionInt = 10;
							break;
					}
					CompetitionIndex = (float)specialKeyword.CompetitionIndex;
				}
				else
				{
					//tinh CompetitionIndex
					switch (Competition.ToLower())
					{
						default:
						case "low":
							CompetitionInt = 4;
							CompetitionIndex = float.Parse(ConfigurationManager.AppSettings.Get("CompetitionLow").ToString());
							break;
						case "medium":
							CompetitionInt = 7;
							CompetitionIndex = float.Parse(ConfigurationManager.AppSettings.Get("CompetitionMedium").ToString());
							break;
						case "high":
							CompetitionInt = 10;
							CompetitionIndex = float.Parse(ConfigurationManager.AppSettings.Get("CompetitionHight").ToString());
							break;
					}
				}

				TotalCostPerMonth = (long)(AvgMonthlySearches * SuggestedBid * ClickRate);
				SEOFibo = (long)(TotalCostPerMonth * float.Parse(ConfigurationManager.AppSettings.Get("PercentSetup").ToString()));


				string[] element = KeywordName.Split(' ');
				long chargesMin = 0;
				long setupCostMin = 0;
				long setupCostMax = 0;

				if (element.Length < 3)
				{
					chargesMin = long.Parse(ConfigurationManager.AppSettings.Get("ChargesMinLenSmaller3").ToString());
					setupCostMin = long.Parse(ConfigurationManager.AppSettings.Get("SetupCostMinLenSmaller3").ToString());
					setupCostMax = long.Parse(ConfigurationManager.AppSettings.Get("SetupCostMaxLenSmaller3").ToString());
				}
				else if (element.Length == 3)
				{
					chargesMin = long.Parse(ConfigurationManager.AppSettings.Get("ChargesMinLenEquar3").ToString());
					setupCostMin = long.Parse(ConfigurationManager.AppSettings.Get("SetupCostMinLenEquar3").ToString());
					setupCostMax = long.Parse(ConfigurationManager.AppSettings.Get("SetupCostMaxLenEquar3").ToString());
				}
				else if (element.Length == 4)
				{
					chargesMin = long.Parse(ConfigurationManager.AppSettings.Get("ChargesMinLenEquar4").ToString());
					setupCostMin = long.Parse(ConfigurationManager.AppSettings.Get("SetupCostMinLenEquar4").ToString());
					setupCostMax = long.Parse(ConfigurationManager.AppSettings.Get("SetupCostMaxLenEquar4").ToString());
				}
				else if (element.Length >= 5)
				{
					chargesMin = long.Parse(ConfigurationManager.AppSettings.Get("ChargesMinLenFrom5").ToString());
					setupCostMin = long.Parse(ConfigurationManager.AppSettings.Get("SetupCostMinLenFrom5").ToString());
					setupCostMax = long.Parse(ConfigurationManager.AppSettings.Get("SetupCostMaxLenFrom5").ToString());
				}

				ChargesForMaintenance = Math.Max(chargesMin, SEOFibo);
				Setup = ChargesForMaintenance * int.Parse(ConfigurationManager.AppSettings.Get("IndexSetup").ToString());
				HardWorkIndex = Math.Max(float.Parse(ConfigurationManager.AppSettings.Get("CompetitionMinimum").ToString()), CompetitionIndex);
				//SetupCost = (int)(ChargesForMaintenance * HardWorkIndex * CompetitionInt);
				SetupCost = (long)(ChargesForMaintenance * HardWorkIndex * int.Parse(ConfigurationManager.AppSettings.Get("SetupCostRate").ToString()));

				if (setupCostMin > 0 && setupCostMax > 0)
				{
					if (SetupCost <= setupCostMin)
					{
						SetupCost = setupCostMin;
					}
					else if (setupCostMax <= SetupCost)
					{
						SetupCost = setupCostMax;
					}
				}
				else if (setupCostMin > 0 && setupCostMax <= 0)
				{
					if (SetupCost <= setupCostMin)
					{
						SetupCost = setupCostMin;
					}
				}

				///Cộng thêm cho giá setup
				///31/05/2016
				///task #3066
				long setupAdd = long.Parse(ConfigurationManager.AppSettings.Get("SetupAdd").ToString());
				SetupCost += setupAdd;

				///Gia co dinh khi khong co day du thong tin tu google
				///2. Cài mặc định cho từ khóa không có số liệu từ Google (Từ khóa chưa có mức cạnh tranh, không có giá thầu) :
				///+ Giá set up : 2,500,000 vnd/từ khóa
				///+ Giá duy trì: 250,000 vnd/từ khóa
				///08/06/2016
				if(SuggestedBid<=0 || AvgMonthlySearches<=0)
				{
					SetupCost = 2500000;
					ChargesForMaintenance = 250000;
				}

				///3. Gỡ/Xóa bỏ mặc định Max cho từ khóa có "03 từ" (Hiện tại đang mặc định giá set up max là 15,000,000).
				///08/06/2016
				///Cái này thay doi số trong app.config là được
				

				//Thêm phần này để xác định là có public giá ra hay không
				/*
				 Điều kiện: Không public giá và note là: Liên hệ lại với đối tác để biết chi tiết
				 * keyword < 2 từ
				 * keyword = 2 từ && tìm kiếm > 3000
				 * keyword = 3 từ && tìm kiếm > 9000
				 * Đã chuyễn sang sp
				 */

				KeywordPlanerStatus = KeywordPlanerStatus.RefreshSuccess;
			}
			catch
			{
				KeywordPlanerStatus = KeywordPlanerStatus.RefreshFail;
			}
		}
	}

	public class KeywordPlannerModel
	{
		public long ID { get; set; }
		public string KeywordName { get; set; }
		public string Competition { get; set; }
		public long SetupPrice { get; set; }
		public long ChargesForMaintenance { get; set; }
		public string CurrentFormat { get; set; }
		public string Note { get; set; }
	}
}
