﻿using DAO;
using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SEOServices.Models;
using SEOServices.Code;
using System.IO;
using Utility;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Globalization;
using System.Diagnostics;

namespace SEOServices
{
    public class SEOKeywordService
    {
		private readonly ISEOKeyword _seoKeywordRepository = DataAccess.SEOKeywordDao;


		public List<SEOKeyword> GetAllKeywordAndDomainRelated()
		{
			return _seoKeywordRepository.GetAllKeywordAndDomainRelated();
		}

		public List<SEOKeywordViewModel> GetAllKeywordWithFilter(long domainid, long keywordid)
		{
			try
			{
				var list = _seoKeywordRepository.GetAllKeywordWithFilter(domainid, keywordid);
				if (list != null)
				{
					List<SEOKeywordViewModel> result = new List<SEOKeywordViewModel>();
					foreach (var item in list)
					{
						var model = item.ConvertToViewModelWithExtenstionProperties<SEOKeywordViewModel>();
						result.Add(model);
					}
					return result;
				}
				else
				{
					return null;
				}
			}
			catch
			{
				return null;
			}
		}

		public bool CreateFileXLSX(long domainid, long keywordid, string filepath, string sheetName)
		{
			try
			{
				List<SEOKeyword> list =_seoKeywordRepository.GetAllKeywordWithFilter(domainid, keywordid);
				var workbook = new XSSFWorkbook();

				var sheet = workbook.CreateSheet(sheetName);
				IDataFormat dataFormatCustom = workbook.CreateDataFormat();

				var datetimeCellStyle = workbook.CreateCellStyle();
				ICreationHelper createHelper = workbook.GetCreationHelper();
				short dateFormat = createHelper.CreateDataFormat().GetFormat("yyyy-MM-dd HH:mm:ss");
				datetimeCellStyle.DataFormat = dateFormat;

				var currencyCellStyle = workbook.CreateCellStyle();
				ICreationHelper currencyHelper = workbook.GetCreationHelper();
				short currencyFormat = currencyHelper.CreateDataFormat().GetFormat("#,##");
				currencyCellStyle.DataFormat = currencyFormat;
				currencyCellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
				currencyCellStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

				// Create the style object
				var centerCellStyle = workbook.CreateCellStyle();
				centerCellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
				centerCellStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

				//// Create the style object
				//var fullcellCellStyle = workbook.CreateCellStyle();
				////fullcellCellStyle.wi

				// Add header labels
				var rowIndex = 0;
				int cellIndex = 0;
				var row = sheet.CreateRow(rowIndex);
				row.CreateCell(cellIndex).SetCellValue("Tên miền");

				cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Trạng thái tên miền");

				cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Từ khoá");

				cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Trạng thái từ khoá");

				cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Giá trên partner");

				cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Thứ hạn trên Google");

				cellIndex++;
				row.CreateCell(cellIndex).SetCellValue("Ngày update");

				cellIndex++;
				rowIndex++;
				// Add data rows
				foreach (SEOKeyword domain in list)
				{
					int cell = 0;
					row = sheet.CreateRow(rowIndex);
					row.CreateCell(cell).SetCellValue(domain.ExtenstionProperties["DomainName"].ToString());

					cell++;
					row.CreateCell(cell).SetCellValue(domain.ExtenstionProperties["SEODomainStatusStr"].ToString());
					row.Cells[cell].CellStyle = centerCellStyle;
					cell++;
					row.CreateCell(cell).SetCellValue(domain.SEOKeywordName);

					cell++;
					row.CreateCell(cell).SetCellValue(domain.SEOKeywordStatusStr);
					row.Cells[cell].CellStyle = centerCellStyle;
					cell++;
					row.CreateCell(cell).SetCellValue(domain.PricePerWeekOnPartner);
					row.Cells[cell].CellStyle = currencyCellStyle;

					cell++;
					row.CreateCell(cell).SetCellValue(domain.RankOnGoogle);
					row.Cells[cell].CellStyle = centerCellStyle;

					cell++;
					row.CreateCell(cell).SetCellValue(domain.UpdatedDate);
					row.Cells[cell].CellStyle = datetimeCellStyle;

					
					cell++;
					rowIndex++;
				}
				//int indexcolumn = 0;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				//sheet.AutoSizeColumn(indexcolumn);
				//indexcolumn++;
				// Save the Excel spreadsheet to a file on the web server's file system
				using (var fileData = new FileStream(filepath, FileMode.Create))
				{
					workbook.Write(fileData);
				}
				//workbook.Write()
				return true;
			}
			catch
			{
				return false;
			}
		}

		public void OpenFileInExplorer(string path)
		{
			string windir = Environment.GetEnvironmentVariable("windir");
			if (string.IsNullOrEmpty(windir.Trim()))
			{
				windir = "C:\\Windows\\";
			}
			if (!windir.EndsWith("\\"))
			{
				windir += "\\";
			}

			FileInfo fileToLocate = null;
			fileToLocate = new FileInfo(path);

			ProcessStartInfo pi = new ProcessStartInfo(windir + "explorer.exe");
			pi.Arguments = "/select, \"" + fileToLocate.FullName + "\"";
			pi.WindowStyle = ProcessWindowStyle.Normal;
			pi.WorkingDirectory = windir;

			//Start Process
			Process.Start(pi);
		}
	}
}
