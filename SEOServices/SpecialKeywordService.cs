﻿using DAO.ADO.Net.Entities;
using DAO.Entity;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SEOServices
{
	public class SpecialKeywordService
	{
		private readonly SpecialKeywordEFDao _specialKeywordDao = new SpecialKeywordEFDao();
		public List<tblSpecialKeyword> GetAllInFileXLSX(string _fileName,long userid,ref int totalFail)
		{
			try
			{
				List<tblSpecialKeyword> list = new List<tblSpecialKeyword>();
				XSSFWorkbook xssfWorkbook;
				using (FileStream file = new FileStream(_fileName, FileMode.Open, FileAccess.Read))
				{
					xssfWorkbook = new XSSFWorkbook(file);
				}

				ISheet sheet = xssfWorkbook.GetSheetAt(0);
				System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
				IRow row = null;
				ICell cellGroup;
				ICell cellSub;
				ICell cellCompetition;
				ICell cellName1;
				ICell cellName2;
				string tempValue;

				while (rows.MoveNext())
				{
					row = (XSSFRow)rows.Current;
					if (row != null)
					{
						tblSpecialKeyword specialKeyword = new tblSpecialKeyword();
						specialKeyword.UserId = userid;
						int cellindex = 0;
						cellGroup = row.GetCell(cellindex);
						cellindex++;
						cellSub = row.GetCell(cellindex);
						cellindex++;
						cellCompetition = row.GetCell(cellindex);
						cellindex++;
						cellName1 = row.GetCell(cellindex);
						cellindex++;
						cellName2 = row.GetCell(cellindex);

						tempValue = cellGroup != null ? cellGroup.StringCellValue.TrimEnd().TrimStart() : string.Empty;
						specialKeyword.GroupName = tempValue;

						tempValue = cellSub != null ? cellSub.StringCellValue.TrimEnd().TrimStart() : string.Empty;
						specialKeyword.SubName = tempValue;

						try
						{
							if (cellCompetition.CellType == CellType.Numeric)
							{
								specialKeyword.CompetitionIndex = cellCompetition != null ? Convert.ToDecimal(cellCompetition.NumericCellValue) : 0;
							}
							else
							{
								tempValue = cellCompetition != null ? cellCompetition.StringCellValue.TrimEnd().TrimStart() : string.Empty;
								specialKeyword.CompetitionIndex = decimal.Parse(tempValue);
							}

						}
						catch
						{
							specialKeyword.CompetitionIndex = 0;
						}

						tempValue = cellName1 != null ? cellName1.StringCellValue.TrimEnd().TrimStart() : string.Empty;
						specialKeyword.KeywordName1 = tempValue;

						tempValue = cellName2 != null ? cellName2.StringCellValue.TrimEnd().TrimStart() : string.Empty;
						specialKeyword.KeywordName2 = tempValue;


						if ((!string.IsNullOrEmpty(specialKeyword.KeywordName1) || !string.IsNullOrEmpty(specialKeyword.KeywordName2)) && specialKeyword.CompetitionIndex>0)
						{
							list.Add(specialKeyword);
						}
						else
						{
							totalFail += 1;
						}
					}
				}

				return list;
			}
			catch
			{
				return null;
			}
		}

		public bool Add(List<tblSpecialKeyword> listSpecialKeyword)
		{
			try
			{
				return _specialKeywordDao.AddSpecialKeyword(listSpecialKeyword);
			}
			catch
			{
				return false;
			}
		}

		public List<tblSpecialKeyword> GetListSpecialKeyword(string keyword)
		{
			try
			{
				return _specialKeywordDao.GetListSpecialKeyword(keyword);
			}
			catch
			{
				return null;
			}
		}

		public tblSpecialKeyword GetSingle(long kewordId)
		{
			try
			{
				return _specialKeywordDao.GetSingle(kewordId);
			}
			catch
			{
				return null;
			}
		}

		public bool Update(tblSpecialKeyword keywordUpdate)
		{
			try
			{
				return _specialKeywordDao.Update(keywordUpdate);
			}
			catch
			{
				return false;
			}
		}

		public tblSpecialKeyword GetSpecialKeywordContainsValue(string KeywordName)
		{
			try
			{
				return _specialKeywordDao.GetSpecialKeywordContainsValue(KeywordName);
			}
			catch
			{
				return null;
			}
		}
	}
}
