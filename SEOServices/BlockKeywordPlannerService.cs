﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAO.Interface;
using DTO.Objects;
using DAO;
using SEOServices.Models;
using SEOServices.Code;

namespace SEOServices
{
	public class BlockKeywordPlannerService
	{
		private readonly IBlockInsertKeywordPlanner _blockRepository = DataAccess.BlockInsertKeywordPlannerDao;

		public List<BlockKeywordPlannerViewModel> GetAllBlock()
		{
			try
			{
				var list = _blockRepository.GetAll();
				if(list!=null)
				{
					List<BlockKeywordPlannerViewModel> result = new List<BlockKeywordPlannerViewModel>();
					foreach (var item in list)
					{
						var model = item.ConvertToViewModelWithExtenstionProperties<BlockKeywordPlannerViewModel>();
						result.Add(model);
					}
					return result;
				}
				else
				{
					return null;
				}
			}
			catch
			{
				return null;
			}
		}

		public long InsertBlockImport(string blockName)
		{
			return _blockRepository.InsertWithImport(blockName,2);
		}
	}
}
