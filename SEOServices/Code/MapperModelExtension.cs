﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Reflection;
using System.Web;

using DTO.Objects;
using SEOServices.Models;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace SEOServices.Code
{
    
    public static class MapperModelExtension
    {
        /// <summary>
        /// Lấy dữ liệu cho object tự động từ model. Tự động cập nhật UpdatedDate 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="businessobj">BusinessObject cần lấy dữ liệu</param>
        /// <param name="viewModel">Model dùng để lấy dữ liệu</param>
        /// <returns></returns>
        public static ObjectBase GetDataFromModel<T>(this ObjectBase businessobj, T viewModel)
        {

            foreach (PropertyInfo infoobj in businessobj.GetType().GetProperties())
            {
                PropertyInfo infomodel = Array.Find(viewModel.GetType().GetProperties(), t => t.Name == infoobj.Name);
                if (infomodel != null && infoobj.Name != "ID")
                {
                    object value = infomodel.GetValue(viewModel, null);
                    if (value != null)
                    {
                        infoobj.SetValue(businessobj, TypeDescriptor.GetConverter(infomodel.PropertyType).ConvertFrom(value.ToString()), null);
                    }
                }
                else if(infoobj.Name=="UpdatedDate")
                {
                    infoobj.SetValue(businessobj, DateTime.Now, null);
                }

            }
            return businessobj;
        }

        

        /// <summary>
        /// Convert Model thành BussinessObject.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static T ConvertToBusinessObject<T>(this Model viewModel)
        {
            Type typeobj = typeof(T);
            Type typemodel = viewModel.GetType();
            T entity = (T)Activator.CreateInstance(typeobj);
            foreach (PropertyInfo infomodel in typemodel.GetProperties())
            {
                PropertyInfo infoobj = Array.Find(typeobj.GetProperties(), t => t.Name == infomodel.Name);
                if (infoobj != null)
                {
                    object value = infomodel.GetValue(viewModel, null);
                    if (value != null)
                    {
                        try
                        {
                            infoobj.SetValue(entity, TypeDescriptor.GetConverter(infomodel.PropertyType).ConvertFrom(value.ToString()), null);
                        }
                        catch (Exception)
                        {
                        }
                        
                    }
                }
            }
            return entity;
        }


        public static T ConvertToViewModel<T>(this ObjectBase businessobj)
        {
            Type typemodel = typeof(T);
            Type typeobj = businessobj.GetType();
            T entity = (T)Activator.CreateInstance(typemodel);

            foreach(var i in typemodel.GetProperties())
            {
                var infoobj = typeobj.GetProperty(i.Name);
                if (infoobj == null)
                    continue;

                object value = infoobj.GetValue(businessobj, null);
                i.SetValue(entity, value, null);
            }

            //foreach (PropertyInfo infoobj in typeobj.GetProperties())
            //{
            //    PropertyInfo infomodel = Array.Find(typemodel.GetProperties(), t => t.Name == infoobj.Name);
            //    if (infomodel != null )
            //    {
            //        object value = infoobj.GetValue(businessobj, null);
            //        if (value != null)
            //        {
            //            infomodel.SetValue(entity, TypeDescriptor.GetConverter(infoobj.PropertyType).ConvertFrom(value.ToString()), null);
            //        }
            //    }
            //}
            return entity;
        }

		public static T ConvertToViewModelWithExtenstionProperties<T>(this ObjectBase businessobj)
		{
			Type typemodel = typeof(T);
			Type typeobj = businessobj.GetType();
			T entity = (T)Activator.CreateInstance(typemodel);

			var valueExt = typeobj.GetProperty("ExtenstionProperties");
			if (valueExt != null)
			{
				var dic = (IEnumerable<KeyValuePair<string, object>>)valueExt.GetValue(businessobj, null);
				foreach (var item in dic)
				{
					var key = typemodel.GetProperty(item.Key);
					if(key!=null)
					{
						try
						{
							//key.SetValue(entity, item.Value, null);
							key.SetValue(entity, TypeDescriptor.GetConverter(key.PropertyType).ConvertFrom(item.Value.ToString()), null);
						}
						catch
						{

						}
					}
				}
			}

			foreach (var i in typemodel.GetProperties())
			{
				
				var infoobj = typeobj.GetProperty(i.Name);
				if (infoobj == null)
					continue;

				object value = infoobj.GetValue(businessobj, null);
				i.SetValue(entity, value, null);
			}

			return entity;
		}
    }

}


