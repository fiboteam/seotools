﻿using DAO;
using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utility;

namespace SEOTool
{
	public partial class FrmMain : Form
	{
		/// <summary>
		/// Web chính
		/// </summary>
		private long _idPartnerAccount;
		private NoronWeb _web;
		//private List<SEODomain> _listSEODomain;
		private readonly ISEODomain _seoDomainDao = DataAccess.SEODomainDao;
		private readonly ILogRefeshSEO _logRefreshSEO = DataAccess.LogRefreshSEODao;
		private readonly ISEOKeyword _seoKeywordDao = DataAccess.SEOKeywordDao;

		public delegate void WriteMessageToViewDelegate(RichTextBox rtxt,string message);
		public WriteMessageToViewDelegate myWriteMessageToView;

		public FrmMain()
		{
			InitializeComponent();
			
		}

		private void FrmMain_Load(object sender, EventArgs e)
		{
			_idPartnerAccount = long.Parse(ConfigurationManager.AppSettings.Get("IDPartnerAccount"));
			myWriteMessageToView = new WriteMessageToViewDelegate(WriteMessageToView);

			double minutes = 60;
			double.TryParse(txtIntervalRefreshSEODomain.Text, out minutes);
			timeRefreshSEODomain.Interval = (int)TimeSpan.FromMinutes(minutes).TotalMilliseconds;
			timeRefreshSEODomain.Enabled = false;

			_web = new NoronWeb();
			_web.WriteMessageToViewEvent += WriteMessageToViewForPartner;
			_web.GoToHomePage();

			bgwRefreshSEODomain.RunWorkerCompleted += bgwRefreshSEODomain_RunCompleted;
			bgwRefreshSEOKeyword.RunWorkerCompleted += bgwRefreshSEOKeyword_RunCompleted;

			WriteLogRefreshData(_idPartnerAccount, StatusRefesh.Success, "", "", "Tool refresh đã mở");
		}
	

		private void FrmMain_FormClosed(object sender, FormClosedEventArgs e)
		{
			_web.Close();
			_web.Quit();
			WriteLogRefreshData(_idPartnerAccount, StatusRefesh.Success, "", "", "Tool refresh đã đóng");
		}

		private void btnLogin_Click(object sender, EventArgs e)
		{
			_web.Login();
			WriteLogRefreshData(_idPartnerAccount, StatusRefesh.Success, "", "", "Tool refresh đã Login web partner");
		}

		private void BtnLogout_Click(object sender, EventArgs e)
		{
			_web.Logout();
			WriteLogRefreshData(_idPartnerAccount, StatusRefesh.Success, "", "", "Tool refresh đã Logout web partner");
		}

		private void WriteMessageSync(RichTextBox rtxt, string message)
		{
			this.BeginInvoke(myWriteMessageToView, new object[] { rtxt, message });
		}

		private void WriteMessageToView(RichTextBox rtxt, string message)
		{
			rtxt.AppendText(message + "\n\n");
		}

		private void WriteMessageToViewForPartner( string message)
		{
			
			if (InvokeRequired)
			{
				this.Invoke(new Action<string>(WriteMessageToViewForPartner), new object[] { message });
				return;
			}
			rtxtView.AppendText(message + "\n\n");
		}

		private void button1_Click(object sender, EventArgs e)
		{
			_web.ShowSites("all");
			dgvMain.DataSource = null;
			dgvMain.Columns.Clear();
			dgvMain.DataSource=	_web.GetAllSites();
		}

		private void dgvMain_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
		{
			if(e.RowIndex>=0)
			{
				SEODomain domainClicked = dgvMain.Rows[e.RowIndex].DataBoundItem as SEODomain ;
				if(domainClicked!=null)
				{
					_web.ShowKeywordOfSite(domainClicked.LinkQueryKeyword, "all");
					dgvMain.DataSource = null;
					dgvMain.Columns.Clear();
					dgvMain.DataSource = _web.GetAllKeywordOfSite();
				}
			}
		}

		private void bgwRefreshSEODomain_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				WriteMessageSync(rtxtViewDomain, "Lấy SEO Domain từ Web partner");
				
				if (_web.ShowSites("all"))
				{
					var listSEODomain = _web.GetAllSites();
					if (listSEODomain != null )
					{
						WriteMessageSync(rtxtViewDomain, "Lấy được : " + listSEODomain.Count() + " SEO Domain từ Web partner.");
						//ghi logrefresh
						WriteLogRefreshData(_idPartnerAccount, StatusRefesh.Success, "", listSEODomain.Count().ToString(), "Lấy được SEO Domain từ Web partner.");

						#region refresh data
						WriteMessageSync(rtxtViewDomain, "Thực hiện refresh data SEO Domain");
						List<Task> listTaskRefreshSEODomain = new List<Task>();
						foreach (SEODomain seo in listSEODomain)
						{
							listTaskRefreshSEODomain.Add(Task.Factory.StartNew(() => RefreshSEODomain(seo)));
						}
						Task.WaitAll(listTaskRefreshSEODomain.ToArray());
						WriteMessageSync(rtxtViewDomain, "Thực hiện refresh data SEO Domain success.");
						#endregion refresh data

					}
					else
					{
						//ghi logrefresh
						WriteLogRefreshData(_idPartnerAccount, StatusRefesh.Fail, "", "0", "Không lấy được SEO Domain từ web partner.");
						WriteMessageSync(rtxtViewDomain, "Không lấy được SEO Domain từ web partner.");
					}
				}
				else
				{
					e.Result = null;
				}
			}
			catch(Exception ex)
			{
				WriteMessageSync(rtxtViewDomain, "Lấy SEO Domain từ Web partner gặp lỗi. "+ex.ToString());
				WriteLogRefreshData(_idPartnerAccount, StatusRefesh.Fail, "",ex.Message, "Lấy SEO Domain từ Web partner gặp lỗi.");
				e.Cancel = true;
			}
		}

		private void RefreshSEODomain(SEODomain seo)
		{
			
			string statusrefresh = "";
			string log = "";
			if(_seoDomainDao.RefreshData(seo))
			{
				statusrefresh = "success";
				log = string.Format("SEO Domain '{0}', AccountPartnerId '{8}',IDOnPartner '{1}', LinkQuery '{2}', KeywordActive '{3}', KeywordTop '{4}', Status '{5}', StatusStr '{6}',StatusRefresh '{7}'", seo.DomainName,
	   seo.SEODomainIdOnPartner, seo.LinkQueryKeyword, seo.TotalSEOKeyword, seo.TotalSEOKeywordOnTop, (short)seo.SEODomainStatus, seo.SEODomainStatusStr, statusrefresh, seo.SEOPartnerAccountId);

				WriteLogRefreshData(seo.SEODomainIdOnPartner, StatusRefesh.Success, "", log, "Refresh SEO Domain");
			}
			else
			{
				statusrefresh = "fail";
				 log = string.Format("SEO Domain '{0}', AccountPartnerId '{8}',IDOnPartner '{1}', LinkQuery '{2}', KeywordActive '{3}', KeywordTop '{4}', Status '{5}', StatusStr '{6}',StatusRefresh '{7}'", seo.DomainName,
				seo.SEODomainIdOnPartner, seo.LinkQueryKeyword, seo.TotalSEOKeyword, seo.TotalSEOKeywordOnTop, (short)seo.SEODomainStatus, seo.SEODomainStatusStr, statusrefresh,seo.SEOPartnerAccountId);
				
				WriteLogRefreshData(seo.SEODomainIdOnPartner, StatusRefesh.Fail, "", log, "Refresh SEO Domain");
			}
			
			WriteMessageSync(rtxtViewDomain, log);
		}

		private void bgwRefreshSEODomain_RunCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (!bgwRefreshSEOKeyword.IsBusy)
			{
				bgwRefreshSEOKeyword.RunWorkerAsync();
			}
			//else
			//{
			//	WriteMessageSync(rtxtViewKeyword, "dang bận");
			//}
		}

	

		private void btnSave_Click(object sender, EventArgs e)
		{
			double minutes= 60;
			double.TryParse(txtIntervalRefreshSEODomain.Text, out minutes);
			timeRefreshSEODomain.Interval = (int)TimeSpan.FromMinutes(minutes).TotalMilliseconds;
		}

		private void btnStart_Click(object sender, EventArgs e)
		{
			timeRefreshSEODomain.Enabled = true;
		}
		private void timeRefreshSEODomain_Tick(object sender, EventArgs e)
		{
			if (_web.IsLogin)
			{
				if (!bgwRefreshSEODomain.IsBusy && !bgwRefreshSEOKeyword.IsBusy)
				{
					bgwRefreshSEODomain.RunWorkerAsync();
				}
			}
			else
			{
				WriteMessageSync(rtxtViewDomain, "Web chưa login. yêu cầu login!");
			}
		}

		private void bgwRefreshSEOKeyword_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				WriteMessageSync(rtxtViewKeyword, "Lấy SEO Domain từ Fibo để thực hiện refresh SEO Keyword");
				var listSEODomain = _seoDomainDao.GetList(_idPartnerAccount, 0, "", 0, 0, "", 0, "", 0, "").ToList();
				if (listSEODomain != null)
				{
					WriteMessageSync(rtxtViewKeyword, "Lấy được" + listSEODomain.Count + " SEO Domain từ Fibo để thực hiện refresh SEO Keyword");
					WriteLogRefreshData(_idPartnerAccount, StatusRefesh.Success, "", listSEODomain.Count.ToString(), "Lấy được SEO Domain từ Fibo để thực hiện refresh SEO Keyword");
					//List<Task> listTaskRefreshSEOKeyword = new List<Task>();
					foreach (SEODomain domain in listSEODomain)
					{
						if (_web.ShowKeywordOfSite(domain.LinkQueryKeyword, "all"))
						{
							var listSEOKeyword = _web.GetAllKeywordOfSite();
							if(listSEOKeyword!=null)
							{
								WriteMessageSync(rtxtViewKeyword, "Lấy được " + listSEOKeyword.Count + " SEO keyword của Domain " + domain.DomainName+ " từ Web Partner.");
								WriteLogRefreshData(domain.SEODomainIdOnPartner, StatusRefesh.Success, "", listSEOKeyword.Count.ToString(), "Lấy được " + listSEOKeyword.Count + " SEO keyword của Domain " + domain.DomainName + " từ Web Partner.");
								foreach (SEOKeyword keyword in listSEOKeyword)
								{
									keyword.SEODomainId = domain.ID;
									//listTaskRefreshSEOKeyword.Add(Task.Factory.StartNew(() => RefreshSEOKeyword(keyword)));
									Task.Factory.StartNew(() => RefreshSEOKeyword(keyword));
								}
							}
							else
							{
								WriteMessageSync(rtxtViewKeyword, "Không Lấy được SEO keyword của Domain " + domain.DomainName + " từ Web Partner.");
								WriteLogRefreshData(domain.SEODomainIdOnPartner, StatusRefesh.Fail, "", "0", "Không Lấy được SEO keyword của Domain " + domain.DomainName + " từ Web Partner.");
							}
						}
					}
					//Task.WaitAll(listTaskRefreshSEOKeyword.ToArray());
				}
				else
				{
					WriteMessageSync(rtxtViewKeyword, "Không lấy được SEO Domain từ Fibo để thực hiện refresh SEO Keyword");
					//ghi logrefresh
					WriteLogRefreshData(_idPartnerAccount, StatusRefesh.Fail, "", "0", "Không lấy được SEO Domain từ Fibo để thực hiện refresh SEO Keyword");
				}
			}
			catch(Exception ex)
			{
				WriteMessageSync(rtxtViewKeyword, "Lấy SEO Domain từ Fibo để thực hiện refresh SEO Keyword gặp lỗi. " + ex.ToString());
				WriteLogRefreshData(_idPartnerAccount, StatusRefesh.Fail, "", ex.Message, "Lấy SEO Domain từ Fibo để thực hiện refresh SEO Keyword gặp lỗi. ");
				e.Cancel = true;
			}
		}

		private void RefreshSEOKeyword(SEOKeyword seo)
		{
			string log = "";
			string statusUpdate = "";
			if (_seoKeywordDao.RefreshData(seo))
			{
				statusUpdate = "success";
				log = string.Format("SEO Keyword '{0}', SEODomainID '{7}',IDOnPartner '{1}', LinkCheck '{2}', RankOnGoole '{3}', PricePerWeek '{4}', Status '{5}', StatusStr '{6}', StatusRefresh '{8}'", seo.SEOKeywordName,
				seo.SEOKeywordIdOnPartner, seo.LinkCheck, seo.RankOnGoogle, seo.PricePerWeekOnPartner, (short)seo.SEOKeywordStatus, seo.SEOKeywordStatusStr, seo.SEODomainId,statusUpdate);
				
				WriteLogRefreshData(seo.SEOKeywordIdOnPartner, StatusRefesh.Success, "", log, "Refresh SEO Keyword");
			}
			else
			{
				statusUpdate = "fail";
				log = string.Format("SEO Keyword '{0}', SEODomainID '{7}',IDOnPartner '{1}', LinkCheck '{2}', RankOnGoole '{3}', PricePerWeek '{4}', Status '{5}', StatusStr '{6}', StatusRefresh '{8}'", seo.SEOKeywordName,
				seo.SEOKeywordIdOnPartner, seo.LinkCheck, seo.RankOnGoogle, seo.PricePerWeekOnPartner, (short)seo.SEOKeywordStatus, seo.SEOKeywordStatusStr, seo.SEODomainId, statusUpdate);
				
				WriteLogRefreshData(seo.SEOKeywordIdOnPartner, StatusRefesh.Fail, "", log, "Refresh SEO Keyword");
			}
			//ghi logrefresh
			WriteMessageSync(rtxtViewKeyword, log);
		}

		private void bgwRefreshSEOKeyword_RunCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			
		}

	

		private void btnStopSeoDomain_Click(object sender, EventArgs e)
		{
			timeRefreshSEODomain.Enabled = false;
		}
		private void WriteLogRefreshData(long ObjectID, StatusRefesh statusRefesh, string LinkGetData, string DataRefesh, string Note)
		{
			LogRefreshSEO log = new LogRefreshSEO();
			log.PartnerAccountId = _idPartnerAccount;
			log.ObjectID=ObjectID;
			log.StatusRefesh = statusRefesh;
			log.LinkGetData = LinkGetData;
			log.DataRefesh = DataRefesh;
			log.Note = Note;
			Task.Factory.StartNew(()=>_logRefreshSEO.Insert(log));
		}
	}
}
