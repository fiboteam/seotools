﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOTool
{
	public class WebUser
	{
		public string Name { get; set; }
		public string HomePage { get; set; }
		public string LoginPage { get; set; }
		public string LogoutPage { get; set; }
		public long IDAccountPartner { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string ShowSites { get; set; }
		public string TemplateGetIdDomain { get; set; }
		public string TemplateGetIdKeyword { get; set; }
		public string UrlGetRecommend { get; set; }
	}
}
