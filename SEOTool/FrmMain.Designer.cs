﻿namespace SEOTool
{
	partial class FrmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btnLogin = new System.Windows.Forms.Button();
			this.BtnLogout = new System.Windows.Forms.Button();
			this.rtxtView = new System.Windows.Forms.RichTextBox();
			this.dgvMain = new System.Windows.Forms.DataGridView();
			this.button1 = new System.Windows.Forms.Button();
			this.bgwRefreshSEODomain = new System.ComponentModel.BackgroundWorker();
			this.timeRefreshSEODomain = new System.Windows.Forms.Timer(this.components);
			this.txtIntervalRefreshSEODomain = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnStart = new System.Windows.Forms.Button();
			this.splitContainerMain = new System.Windows.Forms.SplitContainer();
			this.splitContainerLeft = new System.Windows.Forms.SplitContainer();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.btnStopSeoDomain = new System.Windows.Forms.Button();
			this.rtxtViewDomain = new System.Windows.Forms.RichTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this.rtxtViewKeyword = new System.Windows.Forms.RichTextBox();
			this.bgwRefreshSEOKeyword = new System.ComponentModel.BackgroundWorker();
			((System.ComponentModel.ISupportInitialize)(this.dgvMain)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).BeginInit();
			this.splitContainerMain.Panel1.SuspendLayout();
			this.splitContainerMain.Panel2.SuspendLayout();
			this.splitContainerMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).BeginInit();
			this.splitContainerLeft.Panel1.SuspendLayout();
			this.splitContainerLeft.Panel2.SuspendLayout();
			this.splitContainerLeft.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnLogin
			// 
			this.btnLogin.Location = new System.Drawing.Point(6, 19);
			this.btnLogin.Name = "btnLogin";
			this.btnLogin.Size = new System.Drawing.Size(75, 23);
			this.btnLogin.TabIndex = 0;
			this.btnLogin.Text = "Login";
			this.btnLogin.UseVisualStyleBackColor = true;
			this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
			// 
			// BtnLogout
			// 
			this.BtnLogout.Location = new System.Drawing.Point(6, 48);
			this.BtnLogout.Name = "BtnLogout";
			this.BtnLogout.Size = new System.Drawing.Size(75, 23);
			this.BtnLogout.TabIndex = 1;
			this.BtnLogout.Text = "Logout";
			this.BtnLogout.UseVisualStyleBackColor = true;
			this.BtnLogout.Click += new System.EventHandler(this.BtnLogout_Click);
			// 
			// rtxtView
			// 
			this.rtxtView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtView.Location = new System.Drawing.Point(3, 25);
			this.rtxtView.Name = "rtxtView";
			this.rtxtView.Size = new System.Drawing.Size(267, 322);
			this.rtxtView.TabIndex = 2;
			this.rtxtView.Text = "";
			// 
			// dgvMain
			// 
			this.dgvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dgvMain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dgvMain.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
			this.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvMain.Location = new System.Drawing.Point(23, 46);
			this.dgvMain.Name = "dgvMain";
			this.dgvMain.Size = new System.Drawing.Size(234, 46);
			this.dgvMain.TabIndex = 3;
			this.dgvMain.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMain_CellMouseDoubleClick);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(6, 76);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 4;
			this.button1.Text = "Show all site";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// bgwRefreshSEODomain
			// 
			this.bgwRefreshSEODomain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwRefreshSEODomain_DoWork);
			// 
			// timeRefreshSEODomain
			// 
			this.timeRefreshSEODomain.Tick += new System.EventHandler(this.timeRefreshSEODomain_Tick);
			// 
			// txtIntervalRefreshSEODomain
			// 
			this.txtIntervalRefreshSEODomain.Location = new System.Drawing.Point(190, 13);
			this.txtIntervalRefreshSEODomain.Name = "txtIntervalRefreshSEODomain";
			this.txtIntervalRefreshSEODomain.Size = new System.Drawing.Size(43, 20);
			this.txtIntervalRefreshSEODomain.TabIndex = 5;
			this.txtIntervalRefreshSEODomain.Text = "60";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(178, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "Delay refresh SEO Domain (minutes)";
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(243, 11);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(47, 23);
			this.btnSave.TabIndex = 7;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnStart
			// 
			this.btnStart.Location = new System.Drawing.Point(6, 33);
			this.btnStart.Name = "btnStart";
			this.btnStart.Size = new System.Drawing.Size(75, 23);
			this.btnStart.TabIndex = 8;
			this.btnStart.Text = "Start";
			this.btnStart.UseVisualStyleBackColor = true;
			this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
			// 
			// splitContainerMain
			// 
			this.splitContainerMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerMain.Location = new System.Drawing.Point(0, 0);
			this.splitContainerMain.Name = "splitContainerMain";
			// 
			// splitContainerMain.Panel1
			// 
			this.splitContainerMain.Panel1.Controls.Add(this.splitContainerLeft);
			// 
			// splitContainerMain.Panel2
			// 
			this.splitContainerMain.Panel2.Controls.Add(this.groupBox3);
			this.splitContainerMain.Panel2.Controls.Add(this.label3);
			this.splitContainerMain.Panel2.Controls.Add(this.rtxtViewKeyword);
			this.splitContainerMain.Size = new System.Drawing.Size(884, 456);
			this.splitContainerMain.SplitterDistance = 579;
			this.splitContainerMain.TabIndex = 9;
			// 
			// splitContainerLeft
			// 
			this.splitContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerLeft.Location = new System.Drawing.Point(0, 0);
			this.splitContainerLeft.Name = "splitContainerLeft";
			// 
			// splitContainerLeft.Panel1
			// 
			this.splitContainerLeft.Panel1.Controls.Add(this.label4);
			this.splitContainerLeft.Panel1.Controls.Add(this.rtxtView);
			this.splitContainerLeft.Panel1.Controls.Add(this.groupBox1);
			// 
			// splitContainerLeft.Panel2
			// 
			this.splitContainerLeft.Panel2.Controls.Add(this.groupBox2);
			this.splitContainerLeft.Panel2.Controls.Add(this.rtxtViewDomain);
			this.splitContainerLeft.Panel2.Controls.Add(this.label2);
			this.splitContainerLeft.Size = new System.Drawing.Size(579, 456);
			this.splitContainerLeft.SplitterDistance = 273;
			this.splitContainerLeft.TabIndex = 0;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(3, 9);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(87, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Log Web partner";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.btnLogin);
			this.groupBox1.Controls.Add(this.BtnLogout);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Location = new System.Drawing.Point(3, 353);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(267, 100);
			this.groupBox1.TabIndex = 11;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Thao tác lên web partner";
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.btnStopSeoDomain);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Controls.Add(this.txtIntervalRefreshSEODomain);
			this.groupBox2.Controls.Add(this.btnStart);
			this.groupBox2.Controls.Add(this.btnSave);
			this.groupBox2.Location = new System.Drawing.Point(3, 353);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(296, 103);
			this.groupBox2.TabIndex = 12;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Options";
			// 
			// btnStopSeoDomain
			// 
			this.btnStopSeoDomain.Location = new System.Drawing.Point(87, 33);
			this.btnStopSeoDomain.Name = "btnStopSeoDomain";
			this.btnStopSeoDomain.Size = new System.Drawing.Size(75, 23);
			this.btnStopSeoDomain.TabIndex = 9;
			this.btnStopSeoDomain.Text = "Stop";
			this.btnStopSeoDomain.UseVisualStyleBackColor = true;
			this.btnStopSeoDomain.Click += new System.EventHandler(this.btnStopSeoDomain_Click);
			// 
			// rtxtViewDomain
			// 
			this.rtxtViewDomain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtViewDomain.Location = new System.Drawing.Point(3, 25);
			this.rtxtViewDomain.Name = "rtxtViewDomain";
			this.rtxtViewDomain.Size = new System.Drawing.Size(296, 322);
			this.rtxtViewDomain.TabIndex = 11;
			this.rtxtViewDomain.Text = "";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(68, 13);
			this.label2.TabIndex = 9;
			this.label2.Text = "SEO Domain";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.dgvMain);
			this.groupBox3.Location = new System.Drawing.Point(5, 353);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(293, 103);
			this.groupBox3.TabIndex = 13;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Options";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(9, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(73, 13);
			this.label3.TabIndex = 10;
			this.label3.Text = "SEO Keyword";
			// 
			// rtxtViewKeyword
			// 
			this.rtxtViewKeyword.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtViewKeyword.Location = new System.Drawing.Point(3, 25);
			this.rtxtViewKeyword.Name = "rtxtViewKeyword";
			this.rtxtViewKeyword.Size = new System.Drawing.Size(295, 322);
			this.rtxtViewKeyword.TabIndex = 9;
			this.rtxtViewKeyword.Text = "";
			// 
			// bgwRefreshSEOKeyword
			// 
			this.bgwRefreshSEOKeyword.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwRefreshSEOKeyword_DoWork);
			// 
			// FrmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(884, 456);
			this.Controls.Add(this.splitContainerMain);
			this.Name = "FrmMain";
			this.Text = "Refresh SEO Tool";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMain_FormClosed);
			this.Load += new System.EventHandler(this.FrmMain_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgvMain)).EndInit();
			this.splitContainerMain.Panel1.ResumeLayout(false);
			this.splitContainerMain.Panel2.ResumeLayout(false);
			this.splitContainerMain.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).EndInit();
			this.splitContainerMain.ResumeLayout(false);
			this.splitContainerLeft.Panel1.ResumeLayout(false);
			this.splitContainerLeft.Panel1.PerformLayout();
			this.splitContainerLeft.Panel2.ResumeLayout(false);
			this.splitContainerLeft.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).EndInit();
			this.splitContainerLeft.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnLogin;
		private System.Windows.Forms.Button BtnLogout;
		private System.Windows.Forms.RichTextBox rtxtView;
		private System.Windows.Forms.DataGridView dgvMain;
		private System.Windows.Forms.Button button1;
		private System.ComponentModel.BackgroundWorker bgwRefreshSEODomain;
		private System.Windows.Forms.Timer timeRefreshSEODomain;
		private System.Windows.Forms.TextBox txtIntervalRefreshSEODomain;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.SplitContainer splitContainerMain;
		private System.Windows.Forms.RichTextBox rtxtViewKeyword;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.SplitContainer splitContainerLeft;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RichTextBox rtxtViewDomain;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.ComponentModel.BackgroundWorker bgwRefreshSEOKeyword;
		private System.Windows.Forms.Button btnStopSeoDomain;
	}
}

