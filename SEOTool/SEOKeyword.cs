﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOTool
{
	public class SEOKeyword_
	{
		private string templateGetIdKeyword;
		public long SEOKeywordId { get; set; }
		public long SEOKeywordIdOnPartner { get; set; }
		public long SEODomainId { get; set; }
		public string SEOKeywordName { get; set; }
		public int PricePerWeekOnPartner { get; set; }
		public int Price { get; set; }
		public int RankOnGoogle { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime UpdatedDate { get; set; }
		public string Note { get; set; }
		public string LinkCheck { get; set; }
		public string SEOKeywordStatus { get; set; }
		public long CreatedById { get; set; }
		public SEOKeyword_(string templateGetIdKeyword)
		{
			this.templateGetIdKeyword = templateGetIdKeyword;
		}
		public SEOKeyword_()
		{
			this.templateGetIdKeyword = "";
		}

	}
}
