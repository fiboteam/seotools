﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOTool
{
	public interface IWeb
	{
		/// <summary>
		/// Mở trang chủ
		/// </summary>
		bool GoToHomePage();
		/// <summary>
		/// Login
		/// </summary>
		bool Login();
		/// <summary>
		/// Logout
		/// </summary>
		bool Logout();
		/// <summary>
		/// Thoát Chrome
		/// </summary>
		bool Close();
		/// <summary>
		/// Thoát ChromeDriver.exe
		/// </summary>
		bool Quit();
		/// <summary>
		/// Đến một đường link nào đó
		/// </summary>
		/// <param name="url"></param>
		bool GoToUrl(string url);
		bool Click(string xpath);
		bool SendKey(string xpath, string values);
	}
}
