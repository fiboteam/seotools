﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium.Support.UI;

namespace SEOTool
{
	public class WebBase : IWeb
	{
		protected ChromeDriver WebDriver { get; set; }
		protected WebUser WebUser { get; set; }
		public bool IsLogin { get; set; }

		public delegate void WriteMessageToView(string Message);
		public event WriteMessageToView WriteMessageToViewEvent;

		public delegate void WritePropressToView(int percent);
		public event WritePropressToView WritePropressToViewEvent;

		public WebBase()
		{
			///Khởi tạo
			WebDriver = new ChromeDriver(Directory.GetCurrentDirectory());
			///Lấy thông tin web
			var jOb = JObject.Parse(File.ReadAllText(Directory.GetCurrentDirectory() + @"\Config.json"));
			JArray users = (JArray)jOb["WebUser"];
			WebUser = users.ToObject<List<WebUser>>().First();
		}

		protected void WriteMessageToViewHandle(string Message)
		{
			if (WriteMessageToViewEvent != null)
				WriteMessageToViewEvent(Message);
		}
		protected void WritePropressToViewHandle(int percent)
		{
			if (WritePropressToViewEvent != null)
				WritePropressToViewEvent(percent);
		}

		public bool GoToHomePage()
		{
			try
			{
				WriteMessageToViewHandle("Go to homepage: " + WebUser.HomePage);
				///Open web
				WebDriver.Navigate().GoToUrl(WebUser.HomePage);
				return true;
			}
			catch
			{
				return false;
			}

		}

		public virtual bool Login()
		{
			throw new NotImplementedException();
		}

		public virtual bool Logout()
		{
			throw new NotImplementedException();
		}

		public bool Close()
		{
			try
			{
				WebDriver.Close();
				return true;
			}
			catch
			{
				return false;
			}
		}
		public bool Quit()
		{
			try
			{
				WebDriver.Quit();
				WebDriver.Dispose();
				return true;
			}
			catch
			{
				return false;
			}
		}

		public bool GoToUrl(string url)
		{
			try
			{
				WriteMessageToViewHandle("Go to url: " + url);
				WebDriver.Navigate().GoToUrl(url);
				return true;
			}
			catch
			{
				return false;
			}
			
		}


		public bool Click(string xpath)
		{
			try
			{
				//IWebElement element = WebDriver.FindElementByXPath(xpath);
				//if (element != null)
				//{
				//	element.Click();
				//}

				WebDriverWait wait = new WebDriverWait(WebDriver, new TimeSpan(0, 0, 10));
				wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath)));

				IWebElement element = WebDriver.FindElementByXPath(xpath);
				element.Click();

				WriteMessageToViewHandle("click success: " + xpath);
				return true;
			}
			catch
			{
				WriteMessageToViewHandle("click fail: " + xpath);
				return false;
			}
		}


		public bool SendKey(string xpath, string values)
		{
			try
			{
				//IWebElement element = WebDriver.FindElementByXPath(xpath);
				//if (element != null)
				//{
				//	element.SendKeys(values);
				//}

				WebDriverWait wait = new WebDriverWait(WebDriver, new TimeSpan(0, 0, 10));
				wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath)));

				IWebElement element = WebDriver.FindElementByXPath(xpath);
				element.SendKeys(values);

				WriteMessageToViewHandle("Sendkey success: " + xpath);
				return true;
			}
			catch
			{
				WriteMessageToViewHandle("Sendkey fail: " + xpath);
				return false;
			}
		}
	}
}
