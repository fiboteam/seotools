﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SEOTool
{
	public class SEODomain_
	{
		private string templateGetIdDomainOnPartner;
		public long SEODomainId { get; set; }
		public long SEODomainIdOnPartner 
		{ 
			get
			{
				if (string.IsNullOrEmpty(LinkQueryKeyword))
				{
					return 0;
				}
				else
				{
					Regex regex = new Regex(templateGetIdDomainOnPartner.Replace("{0}",@"([\d]+)"),RegexOptions.IgnoreCase);
					Match match = regex.Match(LinkQueryKeyword);
					if(match!=null)
					{
						long id = long.Parse(match.Groups[1].Value);
						return id;
					}
					else
					{
						return 0;
					}
				}
			}
			set
			{

			}
		}
		public string DomainName { get; set; }
		public int TotalSEOKeyword { get; set; }
		public int TotalSEOKeywordOnTop { get; set; }
		public string LinkQueryKeyword { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime UpdatedDate { get; set; }
		public string Note { get; set; }
		public string SEODomainStatus { get; set; }
		public long CreatedById { get; set; }
		public SEODomain_(string templateGetIdDomainOnPartner)
		{
			this.templateGetIdDomainOnPartner = templateGetIdDomainOnPartner;
		}
	}
}
