﻿using DAO;
using DAO.Interface;
using DTO.Objects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utility;

namespace SEOTool
{
	public class NoronWeb : WebBase
	{
		
		public NoronWeb()
			: base()
		{
			GetSEODomainToRefresh();
		}
		public override bool Login()
		{
			GoToUrl(WebUser.LoginPage);
			if(Click(@"//li[@id='login-link']/span[@class='pseudo-link']"))
			{
				if(SendKey(@"//form[@id='loginForm']/input[@id='username']", WebUser.UserName))
				{
					if(SendKey(@"//form[@id='loginForm']/input[@id='password']", WebUser.Password))
					{
						if(Click(@"//form[@id='loginForm']/input[@type='submit']"))
						{
							WriteMessageToViewHandle("Login success");
							IsLogin = true;
							return true;
						}
					}
				}
			}
			WriteMessageToViewHandle("Login fail");
			IsLogin = false;
			return false;
		}

		public override bool Logout()
		{
			try
			{
				GoToUrl(WebUser.LogoutPage);
				GoToUrl(WebUser.LoginPage);
				WriteMessageToViewHandle("Logout success");
				IsLogin = false;
				return true;
			}
			catch
			{
				WriteMessageToViewHandle("Logout fail");
				return false;
			}
			
		}
		public bool ShowSites(string status)
		{
			return GoToUrl(WebUser.ShowSites+status);
		}
		public List<SEODomain> GetAllSites()
		{
			List<SEODomain> listSEODomain = new List<SEODomain>();
			try
			{
				WebDriverWait wait = new WebDriverWait(WebDriver, new TimeSpan(0, 0, 10));
				wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(@"//table[@class='tab']/tbody/tr[not(contains(@align,'left')) and not(contains(@class,'delim'))]")));

				List<IWebElement> list = WebDriver.FindElementsByXPath(@"//table[@class='tab']/tbody/tr[not(contains(@align,'left')) and not(contains(@class,'delim'))]").ToList();
				
				foreach (IWebElement e in list)
				{
					try
					{
						SEODomain domain = new SEODomain();//WebUser.TemplateGetIdDomain
						domain.SEOPartnerAccountId = WebUser.IDAccountPartner;
						domain.DomainName = e.FindElement(By.XPath("./td[1]/h3[1]/a[1]")).Text;
						domain.SEODomainStatusStr = e.FindElement(By.XPath("./td[4]/b[1]")).Text;
						domain.SEODomainStatus = domain.SEODomainStatusStr.ToEnum<SEODomainStatus>();
						domain.TotalSEOKeyword = int.Parse(e.FindElement(By.XPath("./td[2]/b[1]/a[1]")).Text);
						domain.TotalSEOKeywordOnTop = int.Parse(e.FindElement(By.XPath("./td[3]/b[1]/a[1]")).Text);
						//domain.LinkQueryKeyword = e.FindElement(By.XPath("td[2]/b/a")).GetAttribute("href");
						string str = e.FindElement(By.XPath("./td[2]/b[1]/a[1]")).GetAttribute("href");
						str = Regex.Replace(str, "=(.{1,})$", delegate(Match match)
						{
							string value = match.Groups[1].Value;
							return "=";
						});
						domain.LinkQueryKeyword = str;
						domain.SEODomainIdOnPartner = GetSEODomainIdOnPartner(domain.LinkQueryKeyword, WebUser.TemplateGetIdDomain);

						listSEODomain.Add(domain);
					}
					catch(Exception ex)
					{
						WriteMessageToViewHandle("[" + e.Text + "] Get Domain data from partner error: " + ex.Message);
						continue;
					}
				}

				string linkGetRecommend = "";
				foreach (SEODomain domain in listSEODomain)
				{
					linkGetRecommend = string.Format(WebUser.UrlGetRecommend, domain.SEODomainIdOnPartner);
					if(GoToUrl(linkGetRecommend))
					{
						WebDriverWait waitRecommend = new WebDriverWait(WebDriver, new TimeSpan(0, 0, 10));
						waitRecommend.Until(ExpectedConditions.ElementIsVisible(By.XPath(@"/html/body/div[@id='recommendation']/form[@id='frm']/table/tbody/tr[2]/td/table/tbody/tr[2]/td/pre")));

						var contextRecommend = WebDriver.FindElementByXPath(@"/html/body/div[@id='recommendation']/form[@id='frm']/table/tbody/tr[2]/td/table/tbody/tr[2]/td/pre");

						if(contextRecommend!=null)
						{
							domain.Recommendations = contextRecommend.Text;
						}
						else
						{
							domain.Recommendations = "";
						}
					}
				}

				return listSEODomain;
			}
			catch
			{
				return null;
			}
		}
		private long GetSEODomainIdOnPartner(string LinkQueryKeyword, string templateGetIdDomainOnPartner)
		{
			
			if (string.IsNullOrEmpty(LinkQueryKeyword))
			{
				return 0;
			}
			else
			{
				Regex regex = new Regex(templateGetIdDomainOnPartner.Replace("{0}", @"([\d]+)"), RegexOptions.IgnoreCase);
				Match match = regex.Match(LinkQueryKeyword);
				if (match != null)
				{
					long id = long.Parse(match.Groups[1].Value);
					return id;
				}
				else
				{
					return 0;
				}
			}
		}
		public bool ShowKeywordOfSite(string link,string status)
		{
			return GoToUrl(link + status);
		}
		public List<SEOKeyword> GetAllKeywordOfSite()
		{
			List<SEOKeyword> listSEOKeyword = new List<SEOKeyword>();
			try
			{
				WebDriverWait wait = new WebDriverWait(WebDriver, new TimeSpan(0, 0, 10));
				wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(@"//table[contains(@class,'tab')]/tbody/tr[not(contains(@align,'left')) and not(contains(@class,'delim')) and contains(@align,'center')]")));

				List<IWebElement> list = WebDriver.FindElementsByXPath(@"//table[contains(@class,'tab')]/tbody/tr[not(contains(@align,'left')) and not(contains(@class,'delim')) and contains(@align,'center')]").ToList();
				foreach (IWebElement e in list)
				{
					try
					{
						SEOKeyword seoKeyword = new SEOKeyword();
						//seoKeyword.SEOKeywordIdOnPartner = long.Parse(e.FindElement(By.XPath("./td[1]/input")).GetAttribute("value"));
						//seoKeyword.SEOKeywordStatusStr = e.FindElement(By.XPath("td[2]/img")).GetAttribute("title");
						//seoKeyword.SEOKeywordStatus = seoKeyword.SEOKeywordStatusStr.ToEnum<SEOKeywordStatus>();
						//seoKeyword.SEOKeywordName = e.FindElement(By.XPath("td[3]/a")).Text;
						//if (seoKeyword.SEOKeywordName == "tim cho hoc seo tai tphcm")
						//{
						//	int ads = 3;
						//}
						//seoKeyword.LinkCheck = e.FindElement(By.XPath("td[3]/a")).GetAttribute("href");
						//string strRank = e.FindElement(By.XPath("td[4]/span")).Text;
						//int rank = 0;
						//int.TryParse(strRank, out rank);
						//seoKeyword.RankOnGoogle = rank;
						//string price = e.FindElement(By.XPath("td[5 and @class='price']")).Text;
						//int p = 0;
						//int.TryParse(Regex.Replace(price,@"[\s,.]",""), out p);

						//seoKeyword.PricePerWeekOnPartner = p;

						//long longValueTemp = -1;
						//int intValueTemp = -1;
						//string stringValueTemp = "";

						///Id trên hệ thống của partner
						seoKeyword.SEOKeywordIdOnPartner = e.FindElement(By.XPath("./td[1]/input[1]")).GetAttribute("value").AsLong(-1);
						///Trạng thái trên partner
						seoKeyword.SEOKeywordStatusStr = e.FindElement(By.XPath("./td[2]/img[1]")).GetAttribute("title");
						///Trạng thái convert to enum
						seoKeyword.SEOKeywordStatus = seoKeyword.SEOKeywordStatusStr.ToEnum<SEOKeywordStatus>();
						///Tên keyword
						seoKeyword.SEOKeywordName = e.FindElement(By.XPath("./td[3]/a[1]")).Text;
						///Link xem thông tin keyword
						seoKeyword.LinkCheck = e.FindElement(By.XPath("./td[3]/a[1]")).GetAttribute("href");
						///Thứ hạng
						string strRank = e.FindElement(By.XPath("./td[4]/span[1]")).Text;
						var listChil = e.FindElements(By.XPath("./td[4]/span[1]/span"));
						foreach (var item in listChil)
						{
							strRank = strRank.Replace(item.Text, "");
						}
						seoKeyword.RankOnGoogle = strRank.AsInt(-1);
						///Giá trên partner
						string price = e.FindElement(By.XPath("./td[5 and @class='price']")).Text;
						price = Regex.Replace(price, @"[\s,.]", "");
						seoKeyword.PricePerWeekOnPartner = price.AsInt(-1);
						listSEOKeyword.Add(seoKeyword);
					}
					catch(Exception ex)
					{
						WriteMessageToViewHandle("[" + e.Text + "] Get Keyword data from partner error: " + ex.Message);
						continue;
					}
				}

				return listSEOKeyword;
			}
			catch
			{
				return null;
			}
		}
		
		public void GetSEODomainToRefresh()
		{
			
		}
	}
}
