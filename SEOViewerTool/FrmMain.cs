﻿using DAO.Entity;
using DTO.Objects;
using SEOServices;
using SEOServices.Models;
using SEOViewerTool.Frm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utility;

namespace SEOViewerTool
{
	public partial class FrmMain : RibbonForm
	{
		private readonly SEOKeywordService _seoKeywordService = new SEOKeywordService();
		private readonly KeywordPlannerService _keywordPlannerService = new KeywordPlannerService();
		private readonly BlockKeywordPlannerService _blockKeywordPlannerService = new BlockKeywordPlannerService();
		private readonly SpecialKeywordService _specialKeywordService = new SpecialKeywordService();
		/// <summary>
		/// Treeview click vào domain hay keyword
		/// </summary>
		private string _type;
		/// <summary>
		/// Treeview click vào id nào
		/// </summary>
		private long _id;
		/// <summary>
		/// Danh sach keyword planner import tu file
		/// </summary>
		private List<KeywordPlannerViewModel> listKeywordPlannerImport;

		private long LoginId = 1;
		public FrmMain()
		{
			InitializeComponent();
		}

		private void FrmMain_Load(object sender, EventArgs e)
		{
			//string[] array = new string[] { "luyện thi ielts ở đâu", " Tieng Anh tai chinh ngan hang ", "\"đề thi ielts\"", "(CEF) là gì?", "Trại hè tiếng Anh…","tu khoa ? co dau","asdasd,asdd" };
			//foreach (var item in array)
			//{
			//	if(IsValidKeywordName(item))
			//	{

			//	}
			//	else
			//	{

			//	}
			//}
		}

		private bool IsValidKeywordName(string keyword)
		{
			try
			{
				if(string.IsNullOrEmpty(keyword))
				{
					return false;
				}

				string partern = @"^[\w\s]+$";
				if (Regex.IsMatch(keyword, partern))
				{
					return true;
				}
				return false;
			}
			catch
			{
				return false;
			}
		}

		private void ribbonButtonReport_Click(object sender, EventArgs e)
		{
			var listKeyword = _seoKeywordService.GetAllKeywordAndDomainRelated();
			var root = CreateTreeNodeFrom(listKeyword);
			if(root!=null)
			{
				treeViewKeywordAndDomain.Nodes.Clear();
				treeViewKeywordAndDomain.Nodes.Add(root);
			}
		}

		private TreeNode CreateTreeNodeFrom(List<SEOKeyword> list)
		{
			try
			{
				TreeNode root=null;
				if (list != null)
				{
					root = new TreeNode(string.Format("All SEO keyword ({0})", list.Count));
					root.Tag = 0;
					root.Name = "Domain";
					TreeNode nodeDomain = null;
					TreeNode nodeKeyword = null;
					long idDomain = 0;

					foreach (var item in list)
					{
						if(idDomain!=item.SEODomainId)
						{
							idDomain = item.SEODomainId;

							if (nodeDomain != null)
							{
								root.Nodes.Add(nodeDomain);
							}

							nodeDomain = new TreeNode(string.Format("{0} ({1})", item.ExtenstionProperties["DomainName"].ToString(), item.ExtenstionProperties["TotalKeyword"].ToString()));
							nodeDomain.Tag = item.SEODomainId;
							nodeDomain.Name = "Domain";
							//nodeDomain.te
						}

						nodeKeyword = new TreeNode(item.SEOKeywordName);
						nodeKeyword.Tag = item.ID;
						nodeKeyword.Name = "Keyword";

						nodeDomain.Nodes.Add(nodeKeyword);
					}

					if (nodeDomain != null)
					{
						root.Nodes.Add(nodeDomain);
					}
				}
				
				return root;
			}
			catch
			{
				return null;
			}
		}

		private void treeViewKeywordAndDomain_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			var hitTest = e.Node.TreeView.HitTest(e.Location);
			if (hitTest.Location == TreeViewHitTestLocations.PlusMinus)
				return;

			if (e.Node.IsExpanded)
				e.Node.Collapse();
			else
			{
				e.Node.Expand();
				//ShowDataWith(e.Node.Name,long.Parse(e.Node.Tag.ToString()));

				switch (e.Node.Name)
				{
					case "Keyword":
					case "Domain":
						ShowDataWith(e.Node.Name, long.Parse(e.Node.Tag.ToString()));
						break;
					//case "All Block":
					case"Block":
						ShowKeywordPlannerDataWith(e.Node.Name, long.Parse(e.Node.Tag.ToString()));
						break;
				}
			}
		}

		private void ShowKeywordPlannerDataWith(string type, long idBlock)
		{
			List<KeywordPlannerViewModel> list = null;
			if (idBlock == 0)
			{
				KeywordPlannerViewModel model=new KeywordPlannerViewModel();
				model.KeywordName = "";
				model.AvgMonthlySearches = -1;
				model.Competition = "";
				model.SuggestedBid = -1;
				model.KeywordPlanerStatus = (KeywordPlanerStatus)Enum.ToObject(typeof(KeywordPlanerStatus), 0);
				list = _keywordPlannerService.GetAllBy(model);
			}
			else
			{
				switch (type)
				{
					case "Block":
						list = _keywordPlannerService.GetAllBy(idBlock);
						break;
					
				}
			}

			if (list != null)
			{
				dataGridViewMain.Columns.Clear();
				dataGridViewMain.DataSource = null;
				dataGridViewMain.DataSource = list;
				int index = 0;
				dataGridViewMain.Columns["KeywordName"].DisplayIndex = index;
				index++;
				//dataGridViewMain.Columns["AvgMonthlySearches"].DisplayIndex = index;
				//index++;
				dataGridViewMain.Columns["Competition"].DisplayIndex = index;
				index++;
				dataGridViewMain.Columns["ChargesForMaintenance"].DisplayIndex = index;
				dataGridViewMain.Columns["ChargesForMaintenance"].DefaultCellStyle.Format = "#,###";
				index++;
				dataGridViewMain.Columns["SetupCost"].DisplayIndex = index;
				dataGridViewMain.Columns["SetupCost"].DefaultCellStyle.Format = "#,###";
				index++;
				dataGridViewMain.Columns["FormatCurrency"].DisplayIndex = index;
				index++;
				dataGridViewMain.Columns["Note"].DisplayIndex = index;
				index++;

				DataGridViewButtonColumn btnEditKeywordplanner = new DataGridViewButtonColumn();
				btnEditKeywordplanner.HeaderText = "Điều chỉnh giá";
				btnEditKeywordplanner.Text = "Điều chỉnh giá";
				btnEditKeywordplanner.Name = "btnEditKeywordplanner";
				btnEditKeywordplanner.UseColumnTextForButtonValue = true;

				dataGridViewMain.Columns.Add(btnEditKeywordplanner);

				rbtnExport.Enabled = true;
				_type = type;
				_id = idBlock;
			}
			else
			{
				rbtnExport.Enabled = false;
			}
		}
		private void ShowDataWith(string type, long id)
		{
			List<SEOKeywordViewModel> list = null;
			if(id==0)
			{
				list = _seoKeywordService.GetAllKeywordWithFilter(0, 0);
			}
			else
			{
				switch(type)
				{
					case "Keyword":
						list = _seoKeywordService.GetAllKeywordWithFilter(0, id);
						break;
					case "Domain":
						list = _seoKeywordService.GetAllKeywordWithFilter(id, 0);
						break;
				}
			}

			if(list!=null)
			{
				dataGridViewMain.Columns.Clear();
				dataGridViewMain.DataSource = list;
				ribbonButtonExport.Enabled = true;
				_type = type;
				_id = id;
			}
			else
			{
				ribbonButtonExport.Enabled = false;
			}
		}

		private void ribbonButtonExport_Click(object sender, EventArgs e)
		{
			long domainID=0;
			long keywordId=0;
			string fullpath = string.Format("{0}\\Export\\{1}_SEOKeywords.xlsx", System.Environment.CurrentDirectory, DateTime.Now.ToString("yyyyMMddHHmmss"));
			if (!Directory.Exists(string.Format("{0}\\Export",System.Environment.CurrentDirectory)))
			{
				Directory.CreateDirectory(string.Format("{0}\\Export",System.Environment.CurrentDirectory));
			}
			switch(_type)
				{
					case "Keyword":
						keywordId=_id;
						break;
					case "Domain":
						domainID=_id;
						break;
				}

			if (_seoKeywordService.CreateFileXLSX(domainID, keywordId, fullpath, "Keyword export"))
			{
				_seoKeywordService.OpenFileInExplorer(fullpath);
			}
		}

		private void ribbonButtonGetAllBlock_Click(object sender, EventArgs e)
		{
			var listKeywordPlanner = _blockKeywordPlannerService.GetAllBlock();
			var root = CreateTreeNodeFrom(listKeywordPlanner);
			if (root != null)
			{
				treeViewKeywordAndDomain.Nodes.Clear();
				treeViewKeywordAndDomain.Nodes.Add(root);
			}
		}

		private TreeNode CreateTreeNodeFrom(List<BlockKeywordPlannerViewModel> list)
		{
			try
			{
				TreeNode root = null;
				if (list != null)
				{
					root = new TreeNode(string.Format("All Block ({0})", list.Count));
					root.Tag = 0;
					root.Name = "All Block";
					TreeNode nodeDomain = null;
					TreeNode nodeKeyword = null;
					long idDomain = 0;

					foreach (var item in list)
					{
						//if (idDomain != item.ID)
						//{
						//	idDomain = item.ID;

						//	if (nodeDomain != null)
						//	{
						//		root.Nodes.Add(nodeDomain);
						//	}

							nodeDomain = new TreeNode(string.Format("{0} ({1})", item.BlockName , item.TotalKeyword));
							nodeDomain.Tag = item.ID;
							nodeDomain.Name = "Block";
							root.Nodes.Add(nodeDomain);
						//}

						//nodeKeyword = new TreeNode(item.SEOKeywordName);
						//nodeKeyword.Tag = item.ID;
						//nodeKeyword.Name = "Keyword";

						//nodeDomain.Nodes.Add(nodeKeyword);
					}

					//if (nodeDomain != null)
					//{
					//	root.Nodes.Add(nodeDomain);
					//}
				}

				return root;
			}
			catch
			{
				return null;
			}
		}

		private void rbtnImportKWP_Click(object sender, EventArgs e)
		{
			if (bgwInsertKeywordPlanner.IsBusy)
			{
				MessageBox.Show("Đang insert keyword planner, không thể import tiếp!");
			}
			else
			{
				FrmImportKeywordPlanner frm = new FrmImportKeywordPlanner();
				frm.ShowDialog();
				listKeywordPlannerImport = frm.KeywordPlanners;
				if (listKeywordPlannerImport != null && listKeywordPlannerImport.Count > 0)
				{
					long idBlock = _blockKeywordPlannerService.InsertBlockImport(string.Format("{0}", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
					if (idBlock > 0)
					{
						bgwInsertKeywordPlanner.RunWorkerAsync(idBlock);
					}
					else
					{
						MessageBox.Show("Tạo block thất bại, không thể import!");
					}
				}
				else
				{
					MessageBox.Show("Không đọc được bất bỳ keyword nào.");
				}
			}
		}

		private void bgwInsertKeywordPlanner_DoWork(object sender, DoWorkEventArgs e)
		{
			long blockId = (long)e.Argument;
			ParallelOptions options = new ParallelOptions();
			options.MaxDegreeOfParallelism = 1;
			Parallel.For(0, listKeywordPlannerImport.Count, options, i =>
			{
				_keywordPlannerService.InsertWithImport(listKeywordPlannerImport[i], blockId);
			});
			//_keywordPlannerService.Add(listKeywordPlannerImport.Select(i => i.KeywordName).ToList());
		}

		private void rbtnExport_Click(object sender, EventArgs e)
		{
			string fullpath = string.Format("{0}\\Export\\{1}_KeywordPlanner.xlsx", System.Environment.CurrentDirectory, DateTime.Now.ToString("yyyyMMddHHmmss"));
			if (!Directory.Exists(string.Format("{0}\\Export", System.Environment.CurrentDirectory)))
			{
				Directory.CreateDirectory(string.Format("{0}\\Export", System.Environment.CurrentDirectory));
			}
			

			if (_keywordPlannerService.CreateFileXLSX(_id,fullpath, "KeywordPlanner export"))
			{
				_keywordPlannerService.OpenFileInExplorer(fullpath);
			}
		}

		private void rbbtnAddSingleKeywordPlanner_Click(object sender, EventArgs e)
		{
			string keyword = rbtxtKeywordPlanner.TextBoxText.TrimEnd().TrimStart();
			if(string.IsNullOrEmpty(keyword))
			{
				MessageBox.Show("Chưa nhập keyword");
			}
			else
			{
				string blockName=string.Format("{0}", DateTime.Now.ToString("yyyyMMdd-HHmmss"));
				long idBlock = _blockKeywordPlannerService.InsertBlockImport(blockName);
				if (idBlock > 0)
				{
					KeywordPlannerViewModel keywordPlanner = new KeywordPlannerViewModel();
					keywordPlanner.KeywordName = keyword;
					//if(_keywordPlannerService.Add(new List<string>(){keyword}))
					if(_keywordPlannerService.InsertWithImport(keywordPlanner, idBlock))
					{
						MessageBox.Show("Thêm keyword thành công.");
					}
					else
					{
						MessageBox.Show("Thêm keyword thất bại!");
					}
				}
				else
				{
					MessageBox.Show("Tạo block thất bại, không thể import!");
				}
			}
		}

		private void rbbtnTest_Click(object sender, EventArgs e)
		{
			try
			{
				HttpClient client = new HttpClient();
				HttpResponseMessage response = client.GetAsync("http://localhost:39164/api/keywordplanner/" + 5).Result;
				if (response.IsSuccessStatusCode)
				{
					// Convert the result into business object
					var keywordplanner = response.Content.ReadAsAsync<IEnumerable<KeywordPlannerModel>>().Result;

				}
				else
				{
					MessageBox.Show(response.IsSuccessStatusCode.ToString() + response.Content.ToString());
					
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void ribbtnImportSpecKeyword_Click(object sender, EventArgs e)
		{
			FrmImportSpecialKeyword frm = new FrmImportSpecialKeyword(1);
			frm.ShowDialog(this);

			var list = _specialKeywordService.GetListSpecialKeyword(string.Empty);

			LoadDataSpecialKeyword(list);
		}

		private void ribbtnSearchSpecKeyword_Click(object sender, EventArgs e)
		{
			string keyword = ribtxtSpecialKeywordName.TextBoxText.TrimEnd().TrimStart();
			var list=_specialKeywordService.GetListSpecialKeyword (keyword??"");

			LoadDataSpecialKeyword(list);
		}

		private void dataGridViewMain_CellClick(object sender, DataGridViewCellEventArgs e)
		{

			try
			{
				// Ignore clicks that are not on button cells. 
				if (e.RowIndex < 0 
					//|| e.ColumnIndex != dataGridViewMain.Columns["btnChangeStatus"].Index
					) 
					return;

				//// Retrieve the Employee object from the "Assigned To" cell.
				//var keyword = dataGridViewMain.Rows[e.RowIndex].DataBoundItem as tblSpecialKeyword;

				//if (keyword != null)
				//{
				//	FrmEditSpecialKeyword frm = new FrmEditSpecialKeyword(keyword.SpecialKeywordId);
				//	frm.ShowDialog(this);
					
				//	var list=_specialKeywordService.GetListSpecialKeyword (string.Empty);
				//	LoadDataSpecialKeyword(list);
				//}

				switch(dataGridViewMain.Columns[e.ColumnIndex].Name)
				{
					case "btnChangeStatus":

						// Retrieve the Employee object from the "Assigned To" cell.
						var keyword = dataGridViewMain.Rows[e.RowIndex].DataBoundItem as tblSpecialKeyword;

						if (keyword != null)
						{
							FrmEditSpecialKeyword frm = new FrmEditSpecialKeyword(LoginId,keyword.SpecialKeywordId);
							frm.ShowDialog(this);

							var list = _specialKeywordService.GetListSpecialKeyword(string.Empty);
							LoadDataSpecialKeyword(list);
						}

						break;
					case "btnEditKeywordplanner":

						// Retrieve the Employee object from the "Assigned To" cell.
						var kwPlanner = dataGridViewMain.Rows[e.RowIndex].DataBoundItem as KeywordPlannerViewModel;

						if (kwPlanner != null)
						{

							FrmEditKeywordPlanner frm = new FrmEditKeywordPlanner(LoginId,kwPlanner.ID);
							frm.ShowDialog(this);

							//var list = _specialKeywordService.GetListSpecialKeyword(string.Empty);
							//LoadDataSpecialKeyword(list);
							//MessageBox.Show(kwPlanner.KeywordName);
						}

						break;
				}
			}
			catch
			{
				
			}
			
		}

		private void LoadDataSpecialKeyword(List<tblSpecialKeyword> list)
		{
			dataGridViewMain.Columns.Clear();
			dataGridViewMain.DataSource = null;

			if (list != null)
			{
				dataGridViewMain.Columns.Clear();
				dataGridViewMain.DataSource = null;
				dataGridViewMain.DataSource = list;

				dataGridViewMain.Columns["SpecialKeywordId"].Visible = false;
				dataGridViewMain.Columns["UserId"].Visible = false;
				dataGridViewMain.Columns["Note"].Visible = false;
				dataGridViewMain.Columns["Remark"].Visible = false;
				dataGridViewMain.Columns["CreatedDate"].Visible = false;
				dataGridViewMain.Columns["ShortCreatedDate"].Visible = false;

				dataGridViewMain.Columns["UpdatedDate"].DefaultCellStyle.Format = "dd/MM/yyyy HH:mm:ss";

				//int index = 0;
				//dataGridViewMain.Columns["KeywordName"].DisplayIndex = index;
				//index++;
				////dataGridViewMain.Columns["AvgMonthlySearches"].DisplayIndex = index;
				////index++;
				//dataGridViewMain.Columns["Competition"].DisplayIndex = index;
				//index++;
				//dataGridViewMain.Columns["ChargesForMaintenance"].DisplayIndex = index;
				//dataGridViewMain.Columns["ChargesForMaintenance"].DefaultCellStyle.Format = "#,###";
				//index++;
				//dataGridViewMain.Columns["SetupCost"].DisplayIndex = index;
				//dataGridViewMain.Columns["SetupCost"].DefaultCellStyle.Format = "#,###";
				//index++;
				//dataGridViewMain.Columns["FormatCurrency"].DisplayIndex = index;
				//index++;
				//dataGridViewMain.Columns["Note"].DisplayIndex = index;
				//index++;
				//dataGridViewMain.Columns.Add(new DataGridViewColumn())
				DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
				btn.HeaderText = "Edit";
				btn.Text = "Edit";
				btn.Name = "btnChangeStatus";
				btn.UseColumnTextForButtonValue = true;

				dataGridViewMain.Columns.Add(btn);
			}
		}
		
	}
}
