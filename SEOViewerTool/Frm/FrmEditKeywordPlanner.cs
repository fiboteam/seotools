﻿using SEOServices;
using SEOServices.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SEOViewerTool.Frm
{
	public partial class FrmEditKeywordPlanner : Form
	{
		private readonly long _id;
		private readonly long _loginId;
		private readonly KeywordPlannerService _keywordPlannerService = new KeywordPlannerService();
		public FrmEditKeywordPlanner(long loginId,long id)
		{
			InitializeComponent();
			this._id = id;
			this._loginId = loginId;
			var kw=_keywordPlannerService.GetSingle(_id);
			if (kw != null)
			{
				txtChargeMaitain.Text = kw.ChargesForMaintenance.ToString();
				txtSetupCost.Text = kw.SetupCost.ToString();
				rtxtNote.Text = kw.Note;
				txtKeyword.Text = kw.KeywordName;
				ckbIsPublic.Checked = kw.IsPublicToPartner == 1 ? true : false;
			}
			else
			{
				return;
			}

		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			errorProvider1.Clear();

			long chargeForMaintenance = 0;
			long setUpCost = 0;
			bool isPublic = ckbIsPublic.Checked;
			string note = rtxtNote.Text;

			if(!long.TryParse(txtChargeMaitain.Text,out chargeForMaintenance))
			{
				errorProvider1.SetError(txtChargeMaitain, "Số tiền không hợp lệ.");
				return;
			}
			else
			{
				if(chargeForMaintenance<0)
				{
					errorProvider1.SetError(txtChargeMaitain, "Số tiền phải lớn hơn 0");
					return;
				}
			}

			if (!long.TryParse(txtSetupCost.Text, out setUpCost))
			{
				errorProvider1.SetError(txtSetupCost, "Số tiền không hợp lệ.");
				return;
			}
			else
			{
				if (setUpCost < 0)
				{
					errorProvider1.SetError(txtSetupCost, "Số tiền phải lớn hơn 0");
					return;
				}
			}

			if (_keywordPlannerService.UpdateByAdmin(_loginId, _id, chargeForMaintenance, setUpCost, note, isPublic))
			{
				MessageBox.Show("Update keyword planner thành công.");
				this.Close();
			}
			else
			{
				MessageBox.Show("Update keyword planner thất bại.");
			}
		}
	}
}
