﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using SEOServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SEOServices.Models;

namespace SEOViewerTool.Frm
{
	public partial class FrmImportKeywordPlanner : Form
	{
		private readonly KeywordPlannerService _keywordPlannerService = new KeywordPlannerService();
		private readonly BlockKeywordPlannerService _blockKeywordPlannerService = new BlockKeywordPlannerService();

		public string FileName { get; set; }
		public bool RequistRefesh { get; set; }
		public List<KeywordPlannerViewModel> KeywordPlanners { get; set; }
		public FrmImportKeywordPlanner()
		{
			InitializeComponent();
			RequistRefesh = false;
			FileName = string.Empty;
		}

		private void btnChooseFile_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();

			// Set filter options and filter index.
			dialog.Filter = "xlsx Files (.xlsx)|*.xlsx";
			dialog.FilterIndex = 1;

			dialog.Multiselect = false;

			// Call the ShowDialog method to show the dialog box.
			DialogResult result = dialog.ShowDialog();

			// Process input if the user clicked OK.
			if (result == DialogResult.OK)
			{
				FileName = dialog.FileName;
				rtxtMain.Text = FileName;
			}
			else
			{
				FileName = "";
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnImport_Click(object sender, EventArgs e)
		{
			if(string.IsNullOrEmpty(FileName))
			{
				MessageBox.Show("Chưa chọn file import!");
			}
			else
			{
				if(ImportKeywordPlannerFormFileExcel())
				{
					RequistRefesh = true;
					this.Close();
				}
				else
				{
					MessageBox.Show("Đọc file thất bại");
				}
				
			}
		}
		private bool ImportKeywordPlannerFormFileExcel()
		{
			try
			{
				KeywordPlanners = _keywordPlannerService.GetAllInFileXLSX(FileName);


				if (KeywordPlanners != null && KeywordPlanners.Count > 0)
				{
					MessageBox.Show(string.Format("Đọc được {0} keywords", KeywordPlanners.Count));
				}
				else
				{
					MessageBox.Show("Không đọc được bất bỳ keyword nào.");
				}

				return true;
				
			}
			catch(Exception e)
			{
				return false;
			}
		}
	}
}
