﻿using DAO.Entity;
using SEOServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SEOViewerTool.Frm
{
	public partial class FrmEditSpecialKeyword : Form
	{
		private readonly SpecialKeywordService _specialKeywordService = new SpecialKeywordService();
		private readonly long id;
		private readonly long loginId;

		public FrmEditSpecialKeyword(long loginId, long kewordId)
		{
			this.id = kewordId;
			this.loginId = loginId;
			InitializeComponent();
			cbbStatus.DataSource = Enum.GetValues(typeof(SpecialKeywordStatus));

			var specialKeyword=_specialKeywordService.GetSingle(id);
			if (specialKeyword != null)
			{
				txtGroup.Text = specialKeyword.GroupName;
				txtSub.Text = specialKeyword.SubName;
				txtCompetition.Text = specialKeyword.CompetitionIndex.ToString();
				txtName1.Text = specialKeyword.KeywordName1;
				txtName2.Text = specialKeyword.KeywordName2;
				cbbStatus.SelectedItem = specialKeyword.Status;
			}
			else
			{
				this.Close();
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				string group = txtGroup.Text.TrimEnd().TrimStart();
				string sub = txtSub.Text.TrimEnd().TrimStart();
				string competition = txtCompetition.Text.TrimEnd().TrimStart();
				string name1 = txtName1.Text.TrimEnd().TrimStart();
				string name2 = txtName2.Text.TrimEnd().TrimStart();
				SpecialKeywordStatus status;
				Enum.TryParse<SpecialKeywordStatus>(cbbStatus.SelectedValue.ToString(), out status);

				errorProvider1.Clear();

				tblSpecialKeyword keywordUpdate = new tblSpecialKeyword()
				{
					SpecialKeywordId=id,
					GroupName = group,
					SubName = sub,
					CompetitionIndex = decimal.Parse(competition),
					KeywordName1 = name1,
					KeywordName2 = name2,
					Status = status,
					UpdatedDate=DateTime.Now,
					UserId=loginId
				};

				if (string.IsNullOrEmpty(keywordUpdate.KeywordName1) && string.IsNullOrEmpty(keywordUpdate.KeywordName2))
				{
					//MessageBox.Show("Keyword không được rỗng");
					errorProvider1.SetError(txtName1, "Keyword không được rỗng");
					errorProvider1.SetError(txtName2, "Keyword không được rỗng");
					return;
				}

				if (keywordUpdate.CompetitionIndex<=0)
				{
					//MessageBox.Show("Cạnh tranh không được nhỏ hơn 0");
					errorProvider1.SetError(txtCompetition, "Cạnh tranh không được nhỏ hơn 0");
					return;
				}



				if(_specialKeywordService.Update(keywordUpdate))
				{
					MessageBox.Show("Save success");
					this.Close();
				}
				else
				{
					MessageBox.Show("Save error");
				}

			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void txtGroup_TextChanged(object sender, EventArgs e)
		{

		}

		private void label1_Click(object sender, EventArgs e)
		{

		}

		private void label2_Click(object sender, EventArgs e)
		{

		}

		private void label3_Click(object sender, EventArgs e)
		{

		}

		private void label4_Click(object sender, EventArgs e)
		{

		}

		private void label5_Click(object sender, EventArgs e)
		{

		}

		private void txtSub_TextChanged(object sender, EventArgs e)
		{

		}

		private void label6_Click(object sender, EventArgs e)
		{

		}

		private void txtCompetition_TextChanged(object sender, EventArgs e)
		{

		}

		private void txtName1_TextChanged(object sender, EventArgs e)
		{

		}

		private void txtName2_TextChanged(object sender, EventArgs e)
		{

		}

		private void cbbStatus_SelectedIndexChanged(object sender, EventArgs e)
		{

		}
	}
}
