﻿namespace SEOViewerTool.Frm
{
	partial class FrmEditSpecialKeyword
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btnSave = new System.Windows.Forms.Button();
			this.txtGroup = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.txtSub = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txtCompetition = new System.Windows.Forms.TextBox();
			this.txtName1 = new System.Windows.Forms.TextBox();
			this.txtName2 = new System.Windows.Forms.TextBox();
			this.cbbStatus = new System.Windows.Forms.ComboBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.SuspendLayout();
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(130, 240);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 0;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// txtGroup
			// 
			this.txtGroup.Location = new System.Drawing.Point(130, 12);
			this.txtGroup.Name = "txtGroup";
			this.txtGroup.Size = new System.Drawing.Size(165, 20);
			this.txtGroup.TabIndex = 1;
			this.txtGroup.TextChanged += new System.EventHandler(this.txtGroup_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(36, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Group";
			this.label1.Click += new System.EventHandler(this.label1_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 50);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(26, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Sub";
			this.label2.Click += new System.EventHandler(this.label2_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 85);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(62, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Competition";
			this.label3.Click += new System.EventHandler(this.label3_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 124);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(84, 13);
			this.label4.TabIndex = 5;
			this.label4.Text = "Keyword có dấu";
			this.label4.Click += new System.EventHandler(this.label4_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 167);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(102, 13);
			this.label5.TabIndex = 6;
			this.label5.Text = "Keyword không dấu";
			this.label5.Click += new System.EventHandler(this.label5_Click);
			// 
			// txtSub
			// 
			this.txtSub.Location = new System.Drawing.Point(130, 46);
			this.txtSub.Name = "txtSub";
			this.txtSub.Size = new System.Drawing.Size(165, 20);
			this.txtSub.TabIndex = 7;
			this.txtSub.TextChanged += new System.EventHandler(this.txtSub_TextChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 201);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(55, 13);
			this.label6.TabIndex = 8;
			this.label6.Text = "Trạng thái";
			this.label6.Click += new System.EventHandler(this.label6_Click);
			// 
			// txtCompetition
			// 
			this.txtCompetition.Location = new System.Drawing.Point(130, 81);
			this.txtCompetition.Name = "txtCompetition";
			this.txtCompetition.Size = new System.Drawing.Size(165, 20);
			this.txtCompetition.TabIndex = 9;
			this.txtCompetition.TextChanged += new System.EventHandler(this.txtCompetition_TextChanged);
			// 
			// txtName1
			// 
			this.txtName1.Location = new System.Drawing.Point(130, 120);
			this.txtName1.Name = "txtName1";
			this.txtName1.Size = new System.Drawing.Size(165, 20);
			this.txtName1.TabIndex = 10;
			this.txtName1.TextChanged += new System.EventHandler(this.txtName1_TextChanged);
			// 
			// txtName2
			// 
			this.txtName2.Location = new System.Drawing.Point(130, 163);
			this.txtName2.Name = "txtName2";
			this.txtName2.Size = new System.Drawing.Size(165, 20);
			this.txtName2.TabIndex = 11;
			this.txtName2.TextChanged += new System.EventHandler(this.txtName2_TextChanged);
			// 
			// cbbStatus
			// 
			this.cbbStatus.FormattingEnabled = true;
			this.cbbStatus.Location = new System.Drawing.Point(130, 197);
			this.cbbStatus.Name = "cbbStatus";
			this.cbbStatus.Size = new System.Drawing.Size(100, 21);
			this.cbbStatus.TabIndex = 12;
			this.cbbStatus.SelectedIndexChanged += new System.EventHandler(this.cbbStatus_SelectedIndexChanged);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(220, 240);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 13;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.button2_Click);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// FrmEditSpecialKeyword
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(314, 275);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.cbbStatus);
			this.Controls.Add(this.txtName2);
			this.Controls.Add(this.txtName1);
			this.Controls.Add(this.txtCompetition);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.txtSub);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtGroup);
			this.Controls.Add(this.btnSave);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "FrmEditSpecialKeyword";
			this.Text = "Edit Special keyword";
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.TextBox txtGroup;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtSub;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtCompetition;
		private System.Windows.Forms.TextBox txtName1;
		private System.Windows.Forms.TextBox txtName2;
		private System.Windows.Forms.ComboBox cbbStatus;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ErrorProvider errorProvider1;
	}
}