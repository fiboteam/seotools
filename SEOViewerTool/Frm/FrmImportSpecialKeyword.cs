﻿using DAO.Entity;
using SEOServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SEOViewerTool.Frm
{
	public partial class FrmImportSpecialKeyword : Form
	{
		private readonly SpecialKeywordService _specialKeywordService = new SpecialKeywordService();

		private string _fileName;

		private int totalFail;

		private long userId;

		public bool RequistRefesh;

		public List<tblSpecialKeyword> listSpecialKeyword;

		public FrmImportSpecialKeyword(long userId)
		{
			InitializeComponent();
			RequistRefesh = false;
			totalFail = 0;
			this.userId = userId;
		}



		

		private void btnChooseFile_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();

			// Set filter options and filter index.
			dialog.Filter = "xlsx Files (.xlsx)|*.xlsx";
			dialog.FilterIndex = 1;

			dialog.Multiselect = false;

			// Call the ShowDialog method to show the dialog box.
			DialogResult result = dialog.ShowDialog();

			// Process input if the user clicked OK.
			if (result == DialogResult.OK)
			{
				_fileName = dialog.FileName;
				rtxtMain.Text = _fileName;
			}
			else
			{
				_fileName = "";
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			_fileName = string.Empty;
			this.Close();
		}

		private void btnImport_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(_fileName))
			{
				MessageBox.Show("Chưa chọn file import!");
			}
			else
			{
				if (ReadSpecialKeywordFromFile())
				{
					
					if(listSpecialKeyword!=null)
					{
						int totalspec = listSpecialKeyword.Count;

						if(_specialKeywordService.Add(listSpecialKeyword))
						{
							string message = string.Format("Import {0} keywords, Update {2} keywords và {1} không hợp lệ từ file.", listSpecialKeyword != null ? listSpecialKeyword.Count : 0, totalFail, totalspec - listSpecialKeyword.Count);
							MessageBox.Show(message);
							RequistRefesh = true;
							this.Close();
						}
					}
				}
				else
				{
					MessageBox.Show("Đọc file thất bại");
				}

			}
		}

		private bool ReadSpecialKeywordFromFile()
		{
			try
			{
				totalFail = 0;
				listSpecialKeyword = _specialKeywordService.GetAllInFileXLSX(_fileName,userId,ref totalFail);
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}
