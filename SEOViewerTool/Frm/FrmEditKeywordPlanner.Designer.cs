﻿namespace SEOViewerTool.Frm
{
	partial class FrmEditKeywordPlanner
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btnCancel = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.txtSetupCost = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.txtChargeMaitain = new System.Windows.Forms.TextBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.rtxtNote = new System.Windows.Forms.RichTextBox();
			this.ckbIsPublic = new System.Windows.Forms.CheckBox();
			this.txtKeyword = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(228, 274);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 27;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(20, 236);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(53, 13);
			this.label6.TabIndex = 22;
			this.label6.Text = "Public giá";
			// 
			// txtSetupCost
			// 
			this.txtSetupCost.Location = new System.Drawing.Point(110, 81);
			this.txtSetupCost.Name = "txtSetupCost";
			this.txtSetupCost.Size = new System.Drawing.Size(193, 20);
			this.txtSetupCost.TabIndex = 21;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(20, 120);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(44, 13);
			this.label3.TabIndex = 18;
			this.label3.Text = "Ghi chú";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(20, 85);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 13);
			this.label2.TabIndex = 17;
			this.label2.Text = "Giá setup";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(20, 51);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(55, 13);
			this.label1.TabIndex = 16;
			this.label1.Text = "Phí duy trì";
			// 
			// txtChargeMaitain
			// 
			this.txtChargeMaitain.Location = new System.Drawing.Point(110, 47);
			this.txtChargeMaitain.Name = "txtChargeMaitain";
			this.txtChargeMaitain.Size = new System.Drawing.Size(193, 20);
			this.txtChargeMaitain.TabIndex = 15;
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(147, 274);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 14;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// rtxtNote
			// 
			this.rtxtNote.Location = new System.Drawing.Point(110, 120);
			this.rtxtNote.Name = "rtxtNote";
			this.rtxtNote.Size = new System.Drawing.Size(193, 96);
			this.rtxtNote.TabIndex = 28;
			this.rtxtNote.Text = "";
			// 
			// ckbIsPublic
			// 
			this.ckbIsPublic.AutoSize = true;
			this.ckbIsPublic.Location = new System.Drawing.Point(110, 235);
			this.ckbIsPublic.Name = "ckbIsPublic";
			this.ckbIsPublic.Size = new System.Drawing.Size(205, 17);
			this.ckbIsPublic.TabIndex = 29;
			this.ckbIsPublic.Text = "Nếu không chọn giá sẽ không hiện ra";
			this.ckbIsPublic.UseVisualStyleBackColor = true;
			// 
			// txtKeyword
			// 
			this.txtKeyword.Location = new System.Drawing.Point(110, 12);
			this.txtKeyword.Name = "txtKeyword";
			this.txtKeyword.ReadOnly = true;
			this.txtKeyword.Size = new System.Drawing.Size(193, 20);
			this.txtKeyword.TabIndex = 30;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(20, 15);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(48, 13);
			this.label4.TabIndex = 31;
			this.label4.Text = "Keyword";
			// 
			// FrmEditKeywordPlanner
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(315, 324);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtKeyword);
			this.Controls.Add(this.ckbIsPublic);
			this.Controls.Add(this.rtxtNote);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.txtSetupCost);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtChargeMaitain);
			this.Controls.Add(this.btnSave);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "FrmEditKeywordPlanner";
			this.Text = "Edit keyword planner";
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtSetupCost;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtChargeMaitain;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.ErrorProvider errorProvider1;
		private System.Windows.Forms.CheckBox ckbIsPublic;
		private System.Windows.Forms.RichTextBox rtxtNote;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtKeyword;
	}
}