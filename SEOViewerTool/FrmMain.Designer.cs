﻿using System.Windows.Forms;
namespace SEOViewerTool
{
	partial class FrmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Ribbon ribbon1;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
			System.Windows.Forms.RibbonButton rbbtnTest;
			this.ribbon1 = new System.Windows.Forms.Ribbon();
			this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
			this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
			this.sdfsdfdsf = new System.Windows.Forms.RibbonPanel();
			this.ribbonTabKeywordPlanner = new System.Windows.Forms.RibbonTab();
			this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
			this.rpageKeywordPlanner = new System.Windows.Forms.RibbonPanel();
			this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
			this.rbtxtKeywordPlanner = new System.Windows.Forms.RibbonTextBox();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.treeViewKeywordAndDomain = new System.Windows.Forms.TreeView();
			this.toolStripDockLeft = new System.Windows.Forms.ToolStrip();
			this.dataGridViewMain = new System.Windows.Forms.DataGridView();
			this.bgwInsertKeywordPlanner = new System.ComponentModel.BackgroundWorker();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.ribbonTabSpecialKeyword = new System.Windows.Forms.RibbonTab();
			this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
			this.ribtxtSpecialKeywordName = new System.Windows.Forms.RibbonTextBox();
			this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
			this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.ribbonButtonReport = new System.Windows.Forms.RibbonButton();
			this.ribbonButtonExport = new System.Windows.Forms.RibbonButton();
			this.ribbonButtonGetAllBlock = new System.Windows.Forms.RibbonButton();
			this.rbtnAddKWP = new System.Windows.Forms.RibbonButton();
			this.rbtnImportKWP = new System.Windows.Forms.RibbonButton();
			this.rbtnExport = new System.Windows.Forms.RibbonButton();
			this.rbbtnAddSingleKeywordPlanner = new System.Windows.Forms.RibbonButton();
			this.ribbtnSearchSpecKeyword = new System.Windows.Forms.RibbonButton();
			this.ribbtnImportSpecKeyword = new System.Windows.Forms.RibbonButton();
			rbbtnTest = new System.Windows.Forms.RibbonButton();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.toolStripDockLeft.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.SuspendLayout();
			// 
			// ribbon1
			// 
			this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.ribbon1.Location = new System.Drawing.Point(0, 0);
			this.ribbon1.Minimized = false;
			this.ribbon1.Name = "ribbon1";
			// 
			// 
			// 
			this.ribbon1.OrbDropDown.BorderRoundness = 8;
			this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
			this.ribbon1.OrbDropDown.Name = "";
			this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(0, 72);
			this.ribbon1.OrbDropDown.TabIndex = 0;
			this.ribbon1.OrbImage = null;
			this.ribbon1.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F);
			this.ribbon1.Size = new System.Drawing.Size(672, 133);
			this.ribbon1.TabIndex = 0;
			this.ribbon1.Tabs.Add(this.ribbonTab1);
			this.ribbon1.Tabs.Add(this.ribbonTabKeywordPlanner);
			this.ribbon1.Tabs.Add(this.ribbonTabSpecialKeyword);
			this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 26, 20, 0);
			this.ribbon1.ThemeColor = System.Windows.Forms.RibbonTheme.Black;
			// 
			// ribbonTab1
			// 
			this.ribbonTab1.Panels.Add(this.ribbonPanel1);
			this.ribbonTab1.Panels.Add(this.sdfsdfdsf);
			this.ribbonTab1.Text = "SEO Viewer";
			// 
			// ribbonPanel1
			// 
			this.ribbonPanel1.Items.Add(this.ribbonButtonReport);
			this.ribbonPanel1.Text = "";
			// 
			// sdfsdfdsf
			// 
			this.sdfsdfdsf.Items.Add(this.ribbonButtonExport);
			this.sdfsdfdsf.Items.Add(rbbtnTest);
			this.sdfsdfdsf.Text = "";
			// 
			// ribbonTabKeywordPlanner
			// 
			this.ribbonTabKeywordPlanner.Panels.Add(this.ribbonPanel3);
			this.ribbonTabKeywordPlanner.Panels.Add(this.rpageKeywordPlanner);
			this.ribbonTabKeywordPlanner.Panels.Add(this.ribbonPanel4);
			this.ribbonTabKeywordPlanner.Text = "Keyword Planner";
			// 
			// ribbonPanel3
			// 
			this.ribbonPanel3.Items.Add(this.ribbonButtonGetAllBlock);
			this.ribbonPanel3.Text = "Block";
			// 
			// rpageKeywordPlanner
			// 
			this.rpageKeywordPlanner.Items.Add(this.rbtnAddKWP);
			this.rpageKeywordPlanner.Items.Add(this.rbtnExport);
			this.rpageKeywordPlanner.Text = "Keyword Planner";
			// 
			// ribbonPanel4
			// 
			this.ribbonPanel4.Items.Add(this.rbtxtKeywordPlanner);
			this.ribbonPanel4.Items.Add(this.rbbtnAddSingleKeywordPlanner);
			this.ribbonPanel4.Text = "Add one";
			// 
			// rbtxtKeywordPlanner
			// 
			this.rbtxtKeywordPlanner.Text = "Keyword";
			this.rbtxtKeywordPlanner.TextBoxText = "";
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 133);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.treeViewKeywordAndDomain);
			this.splitContainer1.Panel1.Controls.Add(this.toolStripDockLeft);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.dataGridViewMain);
			this.splitContainer1.Size = new System.Drawing.Size(672, 283);
			this.splitContainer1.SplitterDistance = 175;
			this.splitContainer1.TabIndex = 1;
			// 
			// treeViewKeywordAndDomain
			// 
			this.treeViewKeywordAndDomain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeViewKeywordAndDomain.Location = new System.Drawing.Point(0, 25);
			this.treeViewKeywordAndDomain.Name = "treeViewKeywordAndDomain";
			this.treeViewKeywordAndDomain.Size = new System.Drawing.Size(175, 258);
			this.treeViewKeywordAndDomain.TabIndex = 1;
			this.treeViewKeywordAndDomain.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewKeywordAndDomain_NodeMouseClick);
			// 
			// toolStripDockLeft
			// 
			this.toolStripDockLeft.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
			this.toolStripDockLeft.Location = new System.Drawing.Point(0, 0);
			this.toolStripDockLeft.Name = "toolStripDockLeft";
			this.toolStripDockLeft.Size = new System.Drawing.Size(175, 25);
			this.toolStripDockLeft.TabIndex = 0;
			this.toolStripDockLeft.Text = "toolStrip1";
			// 
			// dataGridViewMain
			// 
			this.dataGridViewMain.AllowUserToAddRows = false;
			this.dataGridViewMain.AllowUserToDeleteRows = false;
			this.dataGridViewMain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dataGridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewMain.Location = new System.Drawing.Point(0, 0);
			this.dataGridViewMain.Name = "dataGridViewMain";
			this.dataGridViewMain.ReadOnly = true;
			this.dataGridViewMain.Size = new System.Drawing.Size(493, 283);
			this.dataGridViewMain.TabIndex = 0;
			this.dataGridViewMain.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMain_CellClick);
			// 
			// bgwInsertKeywordPlanner
			// 
			this.bgwInsertKeywordPlanner.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwInsertKeywordPlanner_DoWork);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// ribbonTabSpecialKeyword
			// 
			this.ribbonTabSpecialKeyword.Panels.Add(this.ribbonPanel2);
			this.ribbonTabSpecialKeyword.Panels.Add(this.ribbonPanel5);
			this.ribbonTabSpecialKeyword.Text = "Special Keyword";
			// 
			// ribbonPanel2
			// 
			this.ribbonPanel2.Items.Add(this.ribtxtSpecialKeywordName);
			this.ribbonPanel2.Items.Add(this.ribbtnSearchSpecKeyword);
			this.ribbonPanel2.Text = "View";
			// 
			// ribtxtSpecialKeywordName
			// 
			this.ribtxtSpecialKeywordName.Text = "Keyword";
			this.ribtxtSpecialKeywordName.TextBoxText = "";
			// 
			// ribbonPanel5
			// 
			this.ribbonPanel5.Items.Add(this.ribbtnImportSpecKeyword);
			this.ribbonPanel5.Text = "Add new";
			// 
			// toolStripButton1
			// 
			this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton1.Image = global::SEOViewerTool.Properties.Resources.notes;
			this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton1.Name = "toolStripButton1";
			this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
			this.toolStripButton1.Text = "toolStripButton1";
			// 
			// ribbonButtonReport
			// 
			this.ribbonButtonReport.Image = global::SEOViewerTool.Properties.Resources.check__2_;
			this.ribbonButtonReport.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButtonReport.SmallImage")));
			this.ribbonButtonReport.Text = "Report";
			this.ribbonButtonReport.Click += new System.EventHandler(this.ribbonButtonReport_Click);
			// 
			// ribbonButtonExport
			// 
			this.ribbonButtonExport.Enabled = false;
			this.ribbonButtonExport.Image = global::SEOViewerTool.Properties.Resources._interface;
			this.ribbonButtonExport.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButtonExport.SmallImage")));
			this.ribbonButtonExport.Text = "Export";
			this.ribbonButtonExport.Click += new System.EventHandler(this.ribbonButtonExport_Click);
			// 
			// rbbtnTest
			// 
			rbbtnTest.Image = global::SEOViewerTool.Properties.Resources.notes;
			rbbtnTest.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnTest.SmallImage")));
			rbbtnTest.Text = "Test";
			rbbtnTest.Visible = false;
			rbbtnTest.Click += new System.EventHandler(this.rbbtnTest_Click);
			// 
			// ribbonButtonGetAllBlock
			// 
			this.ribbonButtonGetAllBlock.Image = global::SEOViewerTool.Properties.Resources.check__2_;
			this.ribbonButtonGetAllBlock.MinimumSize = new System.Drawing.Size(60, 0);
			this.ribbonButtonGetAllBlock.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButtonGetAllBlock.SmallImage")));
			this.ribbonButtonGetAllBlock.Text = "All Block";
			this.ribbonButtonGetAllBlock.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
			this.ribbonButtonGetAllBlock.Click += new System.EventHandler(this.ribbonButtonGetAllBlock_Click);
			// 
			// rbtnAddKWP
			// 
			this.rbtnAddKWP.DropDownItems.Add(this.rbtnImportKWP);
			this.rbtnAddKWP.Image = global::SEOViewerTool.Properties.Resources.add;
			this.rbtnAddKWP.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnAddKWP.SmallImage")));
			this.rbtnAddKWP.Style = System.Windows.Forms.RibbonButtonStyle.DropDown;
			this.rbtnAddKWP.Text = "Add";
			// 
			// rbtnImportKWP
			// 
			this.rbtnImportKWP.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
			this.rbtnImportKWP.Image = ((System.Drawing.Image)(resources.GetObject("rbtnImportKWP.Image")));
			this.rbtnImportKWP.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnImportKWP.SmallImage")));
			this.rbtnImportKWP.Text = "Import";
			this.rbtnImportKWP.Click += new System.EventHandler(this.rbtnImportKWP_Click);
			// 
			// rbtnExport
			// 
			this.rbtnExport.Enabled = false;
			this.rbtnExport.Image = global::SEOViewerTool.Properties.Resources._interface;
			this.rbtnExport.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnExport.SmallImage")));
			this.rbtnExport.Text = "Export";
			this.rbtnExport.Click += new System.EventHandler(this.rbtnExport_Click);
			// 
			// rbbtnAddSingleKeywordPlanner
			// 
			this.rbbtnAddSingleKeywordPlanner.Image = global::SEOViewerTool.Properties.Resources.interface__1_;
			this.rbbtnAddSingleKeywordPlanner.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnAddSingleKeywordPlanner.SmallImage")));
			this.rbbtnAddSingleKeywordPlanner.Text = "Add";
			this.rbbtnAddSingleKeywordPlanner.Click += new System.EventHandler(this.rbbtnAddSingleKeywordPlanner_Click);
			// 
			// ribbtnSearchSpecKeyword
			// 
			this.ribbtnSearchSpecKeyword.Image = global::SEOViewerTool.Properties.Resources.magnifier;
			this.ribbtnSearchSpecKeyword.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbtnSearchSpecKeyword.SmallImage")));
			this.ribbtnSearchSpecKeyword.Text = "Search";
			this.ribbtnSearchSpecKeyword.Click += new System.EventHandler(this.ribbtnSearchSpecKeyword_Click);
			// 
			// ribbtnImportSpecKeyword
			// 
			this.ribbtnImportSpecKeyword.Image = global::SEOViewerTool.Properties.Resources.interface__1_;
			this.ribbtnImportSpecKeyword.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbtnImportSpecKeyword.SmallImage")));
			this.ribbtnImportSpecKeyword.Text = "Import";
			this.ribbtnImportSpecKeyword.Click += new System.EventHandler(this.ribbtnImportSpecKeyword_Click);
			// 
			// FrmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(672, 416);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.ribbon1);
			this.Name = "FrmMain";
			this.Text = "SEO Viewer";
			this.Load += new System.EventHandler(this.FrmMain_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.PerformLayout();
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.toolStripDockLeft.ResumeLayout(false);
			this.toolStripDockLeft.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private RibbonTab ribbonTab1;
		private RibbonPanel ribbonPanel1;
		private RibbonButton ribbonButtonReport;
		private RibbonPanel sdfsdfdsf;
		private RibbonButton ribbonButtonExport;
		private SplitContainer splitContainer1;
		private ToolStrip toolStripDockLeft;
		private ToolStripButton toolStripButton1;
		private TreeView treeViewKeywordAndDomain;
		private DataGridView dataGridViewMain;
		private RibbonTab ribbonTabKeywordPlanner;
		private RibbonPanel ribbonPanel3;
		private RibbonButton ribbonButtonGetAllBlock;
		private RibbonPanel rpageKeywordPlanner;
		private RibbonButton rbtnAddKWP;
		private RibbonButton rbtnImportKWP;
		private RibbonButton rbtnExport;
		private System.ComponentModel.BackgroundWorker bgwInsertKeywordPlanner;
		private RibbonPanel ribbonPanel4;
		private RibbonTextBox rbtxtKeywordPlanner;
		private RibbonButton rbbtnAddSingleKeywordPlanner;
		private ErrorProvider errorProvider1;
		private RibbonTab ribbonTabSpecialKeyword;
		private RibbonPanel ribbonPanel2;
		private RibbonTextBox ribtxtSpecialKeywordName;
		private RibbonButton ribbtnSearchSpecKeyword;
		private RibbonPanel ribbonPanel5;
		private RibbonButton ribbtnImportSpecKeyword;
	}
}