﻿using DAO;
using DAO.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace WebAPI.Controllers
{
	[Authorize]
    public class SEODomainController : ApiController
    {
		private readonly ISEODomain _seoDomainRepository = DataAccess.SEODomainDao;
		private readonly DAO.Interface.IUser _userRepository = DataAccess.UserDao;

		[Route("api/SEODomain/GetRecommendation")]
		public string GetRecommendation(string domain)
		{
			try
			{
				string apiId = User.Identity.GetUserId();

				var user = _userRepository.GetUserByUserApiId(apiId);
				if (user == null)
				{
					return null;
				}

				var recommend = _seoDomainRepository.GetRecommend(domain, user.ID);
				return recommend;
			}
			catch
			{
				return null;
			}
		}
    }
}
