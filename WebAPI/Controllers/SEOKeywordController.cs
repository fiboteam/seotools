﻿using DAO;
using DAO.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace WebAPI.Controllers
{
	[Authorize]
    public class SEOKeywordController : ApiController
    {
		private readonly ISEOKeyword _seoKeywordRepository = DataAccess.SEOKeywordDao;
		private readonly DAO.Interface.IUser _userRepository = DataAccess.UserDao;

		public int GetRank(string domainName,string keywordName)
		{
			try
			{
				int rank = 0;

				string apiId = User.Identity.GetUserId();
				//id = User.Identity.GetUserId();
				//id = RequestContext.Principal.Identity.GetUserId();

				var user = _userRepository.GetUserByUserApiId(apiId);
				if(user==null)
				{
					return -2;
				}

				rank = _seoKeywordRepository.GetRank(domainName, keywordName, user.ID);

				return rank;
			}
			catch
			{
				return -1;
			}
		}
    }
}
