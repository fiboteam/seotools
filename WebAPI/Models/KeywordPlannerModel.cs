﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
	public class KeywordPlannerModel
	{
		//public long ID { get; set; }
		public string KeywordName { get; set; }
		//public string Competition { get; set; }
		public long SetupPrice { get; set; }
		public long ChargesForMaintenance { get; set; }
		public string CurrentFormat { get; set; }
		public string Note { get; set; }
	}
}