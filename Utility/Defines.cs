﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    public enum AccountStatus:short
	{
		New=1,
		Active=2,
		Deactive=3,
		Expired=4
	}
	public enum SEODomainStatus : short
	{
		Mới_nhập = 1,	//Mới nhập trên fibo
		Đang_chờ_kiểm_duyệt,//Đang chờ kiểm duyệt
		Sẵn_sàng_để_quảng_bá, //Sẵn sàn quảng bá
		Đã_được_kích_hoạt,	//Đã kích hoạt quảng báo
		Đang_bị_ngắt,
		Đã_bị_ngắt, //Đã bị ngắt
		Đã_xoá
	}
	public enum SEOKeywordStatus:short
	{
		Mới_nhập=1,	//Mới nhập trên fibo
		Đang_chờ_kiểm_duyệt,//Đang chờ kiểm duyệt
		Kích_hoạt, //Sẵn sàn quảng bá
		Đang_bị_ngắt,
		Đã_bị_ngắt, //Đã bị ngắt,
		Đã_xoá
	}
	public enum StatusRefesh:short
	{
		Success=1,
		Fail
	}

	public enum KeywordPlanerStatus:short
	{
		New=1, //moi tao
		Refresh=2, //can refresh lại
		RefreshSuccess=3, //da refresh thanh cong
		RefreshFail=4,	//refresh thất bại
		Expired=5,
	}

	public enum SpecialKeywordStatus:short
	{
		Active=1,
		Deactive=2
	}
}
