﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using DTO;

namespace DTO.Objects
{
    public abstract class ObjectBase
    {

        private Dictionary<string, object> dynamicProperties = new Dictionary<string, object>();

        public Dictionary<string, object> ExtenstionProperties
        {
            get { return dynamicProperties; }
            set { dynamicProperties = value; }
        }

        private long id = -1;

        [ScaffoldColumn(false)]
        [Required]
        public virtual long ID
        {
            get { return id; }
            set { id = value; }
        }

        private DateTime createdDate = DateTime.Now;
        public DateTime CreatedDate
        {
            get { return this.createdDate; }
            set { this.createdDate = value; }
        }

        private DateTime updatedDate = DateTime.Now;
        public DateTime UpdatedDate
        {
            get { return this.updatedDate; }
            set { this.updatedDate = value; }
        }
    }
}
