using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace DTO.Objects
{
    [DataContract]
    public class AlertClient : ObjectBase
    {

		public AlertClient():base(){
		}

		
		public long ClientID { get; set; } 

		
		public decimal MinValue { get; set; } 

		
		public decimal MaxValue { get; set; } 

		
		public int NumberOfAlert { get; set; } 

		
		public int CurrentAlert { get; set; } 

		
		public string ListPhoneNumber { get; set; } 

		
		public int TypeOfAlarm { get; set; } 

		
		public int Interval { get; set; } 

		
		public DateTime ? StartDate { get; set; }


		public short Status { get; set; } 

		
		public int ServiceTypeID { get; set; } 


    }
}