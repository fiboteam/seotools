using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace DTO.Objects
{
    [DataContract]
    public class BlockInsertKeywordPlanner : ObjectBase
    {

		public BlockInsertKeywordPlanner():base(){
		}

		[DataMember]
		public string BlockName { get; set; } 

		[DataMember]
		public string Note { get; set; } 

		[DataMember]
		public string Remark { get; set; } 

		[DataMember]
		public int ShortCreatedDate { get; set; }
		public long UserId { get; set; }

    }
}