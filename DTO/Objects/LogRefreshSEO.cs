using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace DTO.Objects
{
    [DataContract]
	public class LogRefreshSEO : ObjectBase
    {

		public LogRefreshSEO()
			: base()
		{
		}
		public long PartnerAccountId { get; set; }
		[DataMember]
		public long ObjectID { get; set; } 

		[DataMember]
		public StatusRefesh StatusRefesh { get; set; } 

		[DataMember]
		public string LinkGetData { get; set; } 

		[DataMember]
		public string DataRefesh { get; set; } 

		[DataMember]
		public int ShortCreatedDate { get; set; } 

		[DataMember]
		public string Note { get; set; } 


    }
}