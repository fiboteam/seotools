using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace DTO.Objects
{
    [DataContract]
	public class SEOKeyword : ObjectBase
    {

		public SEOKeyword():base(){
		}

		[DataMember]
		[Browsable(false)]
		public long SEOKeywordIdOnPartner { get; set; } 

		[DataMember]
		[Browsable(false)]
		public long SEODomainId { get; set; } 

		[DataMember]
		public string SEOKeywordName { get; set; } 

		[DataMember]
		public int PricePerWeekOnPartner { get; set; } 

		[DataMember]
		public int Price { get; set; } 

		[DataMember]
		public int RankOnGoogle { get; set; } 

		[DataMember]
		public string LinkCheck { get; set; } 

		[DataMember]
		public  SEOKeywordStatus SEOKeywordStatus { get; set; } 

		[DataMember]
		public string SEOKeywordStatusStr { get; set; } 

		[DataMember]
		public int ShortCreatedDate { get; set; } 

		[DataMember]
		public string Note { get; set; } 


    }
}