using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace DTO.Objects
{
    [DataContract]
    public class Account : ObjectBase
    {

		public Account():base(){
		}

		[DataMember]
		public string UserName { get; set; } 

		[DataMember]
		public string UserPass { get; set; } 

		[DataMember]
		public string FullName { get; set; } 

		[DataMember]
		public string Email { get; set; } 

		[DataMember]
		public string Phone { get; set; } 

		[DataMember]
		public string Address { get; set; } 

		[DataMember]
		public short Status { get; set; } 

		[DataMember]
		public int ShortCreatedDate { get; set; } 

		[DataMember]
		public string Note { get; set; } 


    }
}