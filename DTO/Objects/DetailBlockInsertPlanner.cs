using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace DTO.Objects
{
    [DataContract]
    public class DetailBlockInsertPlanner : ObjectBase
    {

		public DetailBlockInsertPlanner():base(){
		}

		[DataMember]
		public long BlockID { get; set; } 

		[DataMember]
		public long KeywordPlannerId { get; set; } 

		[DataMember]
		public string Note { get; set; } 

		[DataMember]
		public string Remark { get; set; } 

		[DataMember]
		public int ShortCreatedDate { get; set; } 


    }
}