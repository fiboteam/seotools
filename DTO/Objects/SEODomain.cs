using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace DTO.Objects
{
    [DataContract]
	public class SEODomain : ObjectBase
    {

		public SEODomain():base(){
		}

		[DataMember]
		public long SEODomainIdOnPartner { get; set; }
		public long SEOPartnerAccountId { get; set; }

		[DataMember]
		public string DomainName { get; set; } 

		[DataMember]
		public int TotalSEOKeyword { get; set; } 

		[DataMember]
		public int TotalSEOKeywordOnTop { get; set; } 

		[DataMember]
		public string LinkQueryKeyword { get; set; } 

		[DataMember]
		public  SEODomainStatus SEODomainStatus { get; set; } 

		[DataMember]
		public string SEODomainStatusStr { get; set; } 

		[DataMember]
		public int ShortCreatedDate { get; set; } 

		[DataMember]
		public string Note { get; set; }
		public string Recommendations { get; set; }


    }
}