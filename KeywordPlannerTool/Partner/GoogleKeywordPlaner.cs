﻿using KeywordPlannerTool.Code;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using SEOServices.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace KeywordPlannerTool.Partner
{
	public class GoogleKeywordPlanner
	{
		//private InternetExplorerDriver _browser = null;
		private ChromeDriver _browser = null;

		public string Homepage { get; set; }
		public string UrlLogin { get; set; }
		public string Error { get; set; }
		private Xpath _xpath;

		public GoogleKeywordPlanner(string homepage,string urlLogin)
		{
			_browser = new ChromeDriver("Libs\\");
			Homepage = homepage;
			UrlLogin = urlLogin;
			LoadXpath();
		}

		public void Exit()
		{
			_browser.Quit();
			_browser.Close();
			_browser.Dispose();
		}

		public bool Login()
		{
			try
			{
				if(GoToUrl(UrlLogin))
				{
					Wailting( new TimeSpan(0, 0, 5),"Clock");
					//if(Click("//div[@id='header-links']/a[text()='Sign in']"))
					if (Click(_xpath.Signin))
					{
						Wailting(new TimeSpan(0, 0, 5), "Clock");
						//if (SendKey("//input[@id='Email']", "fibotechdepartment@gmail.com"))
						if (SendKey(_xpath.Username, _xpath.EmailAccount))
						{
							Wailting( new TimeSpan(0, 0, 5),"Clock");
							//if (Click("//input[@id='next']"))
							if (Click(_xpath.Next))
							{
								Wailting( new TimeSpan(0, 0, 5),"Clock");
								//if (SendKey("//input[@id='Passwd']", "Fibo!@#$%"))
								if (SendKey(_xpath.Password, _xpath.PasswordAccount))
								{
									Wailting(new TimeSpan(0, 0, 5), "Clock");
									//if (Click("//input[@id='signIn']"))
									if (Click(_xpath.BtnSignin))
									{
										Wailting(new TimeSpan(0, 0, 15), _xpath.GetVolume);
										//if (Click("//div[text()='Get search volume data and trends']"))
										if (Click(_xpath.GetVolume))
										{
											Wailting(new TimeSpan(0, 0, 5), "CLOCK");
											//if (SendKey("//div[@id='gwt-uid-27']/textarea[@id='gwt-debug-upload-text-box']", "vay tieu dung"))
											//if (SendKey("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[2]/div[@class='spv-f spr-g']/div[@class='spv-g']/div[@class='spv-a']/div[@id='gwt-debug-splash-panel']/div[3]/div[@class='spjc-h']/div[@class='spjc-e']/div[@id='gwt-debug-stats-selection-splash-panel-form']/div[@id='gwt-debug-upload-panel']/div[@class='sprd-c'][1]/div/div/div[@class='sppb-a']/textarea[@id='gwt-debug-upload-text-box']", "vay tieu dung"))
											//{
											//	Wailting(new TimeSpan(0, 0, 5), "CLOCK");
											//	string text = _browser.ToString();
											//	//if (Click("//div[@id='gwt-debug-stats-selection-splash-panel-form']/div[@id='gwt-debug-action-buttons']//span[@id='gwt-debug-upload-ideas-button-content']"))
											//	if (Click("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[2]/div[@class='spv-f spr-g']/div[@class='spv-g']/div[@class='spv-a']/div[@id='gwt-debug-splash-panel']/div[3]/div[@class='spjc-h']/div[@class='spjc-e']/div[@id='gwt-debug-stats-selection-splash-panel-form']/div[@id='gwt-debug-action-buttons']/div[1]/div[@id='gwt-debug-upload-ideas-button']/div[@class='goog-button-base-outer-box goog-inline-block']/div[@class='goog-button-base-inner-box goog-inline-block']/div[@class='goog-button-base-pos']/div[@class='goog-button-base-content']"))
											//	{
											//		List<KeywordPlannerViewModel> listKeywordPlaner = GetKeywordPlaner();
											//		return true;
											//	}
											//}
											return true;
										}
									}
								}
							}

							
						}
					}
					
				}
				return false;
			}
			catch(Exception ex)
			{
				Error = ex.Message;
				return false;
			}
		}

		public bool Logout()
		{
			try
			{
				if (GoToUrl("http:www.goole.com"))
				{
					return true;
				}
				return false;
			}
			catch (Exception ex)
			{
				Error = ex.Message;
				return false;
			}
		}

		public bool GoToUrl(string url)
		{
			try
			{
				_browser.Navigate().GoToUrl(url);
				//_browser.Url=url;
				return true;
			}
			catch
			{
				return false;
			}
		}

		public void Wailting(TimeSpan timeSpan,string element)
		{
			try
			{
				WebDriverWait wait = new WebDriverWait(_browser, timeSpan);
				wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(element)));
			}
			catch
			{

			}
		}

		public IWebElement WailtingElement(TimeSpan timeSpan, string element)
		{
			try
			{
				WebDriverWait wait = new WebDriverWait(_browser, timeSpan);
				return wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(element)));
			}
			catch
			{
				return null;
			}
		}

		public bool Click(string xpath)
		{
			try
			{
				IWebElement element = _browser.FindElementByXPath(xpath);
				element.Click();

				return true;
			}
			catch (Exception ex)
			{
				Error = ex.Message;
				return false;
			}
		}

		public bool SendKey(string xpath, string values)
		{
			try
			{
				IWebElement element = _browser.FindElementByXPath(xpath);
				element.Clear();
				element.SendKeys(values);

				return true;
			}
			catch(Exception ex)
			{
				Error = ex.Message;
				return false;
			}
		}

		public bool Refresh()
		{
			try
			{
				_browser.Navigate().Refresh();
				LoadXpath();
				return true;
			}
			catch (Exception ex)
			{
				Error = ex.Message;
				return false;
			}
		}
		private void LoadXpath()
		{
			_xpath = new Xpath(Directory.GetCurrentDirectory() + @"\Config\Xpath.json");
		}
		/// <summary>
		/// F5 lai trang truoc khi thuc hien
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public List<KeywordPlannerViewModel> SearchVolumeDataAndTrends(List<string> list)
		{
			//Bắt buộc login trước, đưa tới sẵn giao dien
			//There was a problem retrieving ideas, please try again.
			try
			{
				
				if(Refresh())
				{
					Wailting(new TimeSpan(0, 0, 10), _xpath.GetVolume);
					if (Click(_xpath.GetVolume))
					{
						Wailting(new TimeSpan(0, 0, 10), "CLOCK");
						//if (SendKeyWithList("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[2]/div[@class='spv-f spr-g']/div[@class='spv-g']/div[@class='spv-a']/div[@id='gwt-debug-splash-panel']/div[3]/div[@class='spjc-h']/div[@class='spjc-e']/div[@id='gwt-debug-stats-selection-splash-panel-form']/div[@id='gwt-debug-upload-panel']/div[@class='sprd-c'][1]/div/div/div[@class='sppb-a']/textarea[@id='gwt-debug-upload-text-box']", list, ","))
						if (SendKeyWithList(_xpath.TextAreaRefresh, list, ","))
						{
							Wailting(new TimeSpan(0, 0, 10), "CLOCK");
							string text = _browser.ToString();
							//if (Click("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[2]/div[@class='spv-f spr-g']/div[@class='spv-g']/div[@class='spv-a']/div[@id='gwt-debug-splash-panel']/div[3]/div[@class='spjc-h']/div[@class='spjc-e']/div[@id='gwt-debug-stats-selection-splash-panel-form']/div[@id='gwt-debug-action-buttons']/div[1]/div[@id='gwt-debug-upload-ideas-button']/div[@class='goog-button-base-outer-box goog-inline-block']/div[@class='goog-button-base-inner-box goog-inline-block']/div[@class='goog-button-base-pos']/div[@class='goog-button-base-content']"))
														//if (Click("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[2]/div[@class='spv-f spr-g']/div[@class='spv-g']/div[@class='spv-a']/div[@id='gwt-debug-splash-panel']/div[3]/div[@class='spjc-h']/div[@class='spjc-e']/div[@id='gwt-debug-stats-selection-splash-panel-form']/div[@id='gwt-debug-action-buttons']/div[1]/div[@id='gwt-debug-upload-ideas-button']/div[@class='goog-button-base-outer-box goog-inline-block']/div[@class='goog-button-base-inner-box goog-inline-block']/div[@class='goog-button-base-pos']/div[@class='goog-button-base-content']"))
							if (Click(_xpath.BtnGetSearchRefresh))
							{
								return GetKeywordPlaner();
							}
							else
							{
								Error = "Not found button";
							}
						}
						else
						{
							Error = "Not found textarea";
						}
						//else
						//{
						//	int start = 0;
						//	while(start<list.Count)
						//	{
						//		string search = string.Join(",", list.ToArray(), start,20);

						//		if (SendKeyWithList("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[@class='spib-d spib-a']/div[@id='gwt-debug-search-bar']/div[@class='spVb-a']/div[@id='gwt-debug-upload-panel']/div[@class='sprd-c'][1]/div/div[1]/div/textarea[1]", list, ","))
						//		{
						//			Wailting(new TimeSpan(0, 0, 5), "CLOCK");
						//			string text = _browser.ToString();
						//			if (Click("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[2]/div[@class='spv-f spr-g']/div[@class='spv-g']/div[@class='spv-a']/div[@id='gwt-debug-splash-panel']/div[3]/div[@class='spjc-h']/div[@class='spjc-e']/div[@id='gwt-debug-stats-selection-splash-panel-form']/div[@id='gwt-debug-action-buttons']/div[1]/div[@id='gwt-debug-upload-ideas-button']/div[@class='goog-button-base-outer-box goog-inline-block']/div[@class='goog-button-base-inner-box goog-inline-block']/div[@class='goog-button-base-pos']/div[@class='goog-button-base-content']"))
						//			{
						//				List<KeywordPlannerViewModel> listKeywordPlaner = GetKeywordPlaner();
						//			}
						//		}
						//		start += 20;
						//	}
						//	return true;
						//}
					}
					else
					{
						Error = "Not found 'Get search volume data and trends'";
					}
				}
				else
				{
					Error = "Refresh page fail";
				}

				return null;
			}
			catch (Exception ex)
			{
				Error = ex.Message;
				return null;
			}
		}
		/// <summary>
		/// Thực hiện luôn mà không cần F5
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public List<KeywordPlannerViewModel> SearchVolumeDataAndTrendsWithoutRefreshPage(List<string> list)
		{
			//Bắt buộc login trước, đưa tới sẵn giao dien
			//There was a problem retrieving ideas, please try again.
			try
			{
						//if (list.Count <= 20)
						//{

						//if (SendKeyWithList("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[@class='spib-d spib-a']/div[@id='gwt-debug-search-bar']/div[@class='spVb-a']/div[@id='gwt-debug-upload-panel']/div[@class='sprd-c'][1]/div/div[1]/div/textarea[1]", list, ","))
						if (SendKeyWithList(_xpath.TextArea, list, ","))
						{
							Wailting(new TimeSpan(0, 0, 10), "CLOCK");
							string text = _browser.ToString();
							//if (Click("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[@class='spib-d spib-a']/div[@id='gwt-debug-search-bar']/div[@class='spVb-b spVb-e']/div[@class='spVb-a']/div[@id='gwt-debug-action-buttons'][1]/div[1]/div[@id='gwt-debug-upload-ideas-button']/div[@class='goog-button-base-outer-box goog-inline-block']/div[@class='goog-button-base-inner-box goog-inline-block']/div[@class='goog-button-base-pos']/div[@class='goog-button-base-content']"))
							if (Click(_xpath.BtnGetSearch))
							{
								return GetKeywordPlaner();
							}
							else
							{
								Error = "Not found button";
							}
						}
						else
						{
							Error = "Not found textarea";
						}
						

						return null;
			}
			catch (Exception ex)
			{
				Error = ex.Message;
				return null;
			}
		}

		private bool SendKeyWithList(string xpath, List<string> list,string separator)
		{
			try
			{
				IWebElement element = _browser.FindElementByXPath(xpath);
				element.Clear();
				foreach (var keyword in list)
				{
					element.SendKeys(string.Format("{0},",keyword));
				}

				return true;
			}
			catch (Exception ex)
			{
				Error = ex.Message;
				return false;
			}
		}
		private List<KeywordPlannerViewModel> GetKeywordPlaner()
		{
			try
			{
				List<KeywordPlannerViewModel> list = new List<KeywordPlannerViewModel>();

				IWebElement flagElement = null;
				int countReset = 0;
				while (flagElement == null && countReset<3)
				{
					flagElement=WailtingElement(new TimeSpan(0, 0, 20), _xpath.TableName);
					if (flagElement == null)
					{
						//Chua xuat hien table ket qua thi bam button de get ket qua lai
						if (Click(_xpath.BtnGetSearch))
						{
							
						}
						else
						{
							//Khong tim2 dc cai button get data
							break;
						}
					}
					else
					{
						//Tim dc table roi
						break;
					}
					countReset++;
				}
				
				if(flagElement==null)
				{
					return null;
				}
				
				//IWebElement contentMid = _browser.FindElement(By.XPath("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[2]/div[@class='spr-e']/div[@id='gwt-debug-search-page']/div[@class='spKb-t']/div[@class='spKb-u']/div/div/div[@class='spKb-a']/div[@class='spKb-q']/div/div[@id='gwt-debug-keyword-table']/div[2]/div[@id='gwt-debug-table']/div[3]/div[@class='spDe-h']/div/table[@id='gwt-debug-middleTable']/tbody[1]"));
				//IWebElement contentLeft = _browser.FindElement(By.XPath("/html/body/div[@id='root']/div[@class='spr-d']/div/div/div[2]/div[@class='spr-e']/div[@id='gwt-debug-search-page']/div[@class='spKb-t']/div[@class='spKb-u']/div/div/div[@class='spKb-a']/div[@class='spKb-q']/div/div[@id='gwt-debug-keyword-table']/div[2]/div[@id='gwt-debug-table']/div[3]/table[@id='gwt-debug-leftTable']/tbody[1]"));
				IWebElement contentMid = _browser.FindElement(By.XPath(_xpath.TableInfo));
				IWebElement contentLeft = _browser.FindElement(By.XPath(_xpath.TableName));
				string[] keywordName = Regex.Split(contentLeft.Text, "\r\n");
				string[] values = Regex.Split(contentMid.Text, "\r\n");
				string textTemp = "";
				int intTemp = 0;
				float floatTemp = 0;

				if (keywordName.Length>0)
				{
					for (int i = 0; i < keywordName.Length; i++)
					{
						KeywordPlannerViewModel keywordPlaner = new KeywordPlannerViewModel();
						try
						{
							textTemp = "";
							keywordPlaner.KeywordName = keywordName[i].TrimEnd().TrimStart();

							
							if(int.TryParse(values[(i * 4)].Trim().Replace(",", "").Replace(".", ""), out intTemp))
							{
								keywordPlaner.AvgMonthlySearches = intTemp;
							}
							keywordPlaner.Competition = values[(i * 4) + 1].TrimEnd().TrimStart();
							textTemp = values[(i * 4) + 2].Replace(",", "").Replace(".", "").Trim();
								
							if(float.TryParse(textTemp.Substring(1),out floatTemp))
							{
								keywordPlaner.SuggestedBid = floatTemp;
							}
							keywordPlaner.FormatCurrency = textTemp.Substring(0, 1);
						}
						catch (Exception ex)
						{
							keywordPlaner.Remark = ex.Message;
						}
						Console.WriteLine(string.Format("{0}-{1}-{2}-{3}-{4}", keywordPlaner.KeywordName, keywordPlaner.AvgMonthlySearches, keywordPlaner.Competition, keywordPlaner.SuggestedBid,keywordPlaner.FormatCurrency));
						list.Add(keywordPlaner);
					}
				}

				return list;
			}
			catch(Exception ex)
			{
				Error = ex.Message;
				return null;
			}
		}
	}
}
