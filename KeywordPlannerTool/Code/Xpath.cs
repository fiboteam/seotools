﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace KeywordPlannerTool.Code
{
	public class Xpath
	{
		public string Name { get; set; }
		public string EmailAccount { get; set; }
		public string PasswordAccount { get; set; }
		public string Signin { get; set; }
		public string Username { get; set; }
		public string Next { get; set; }
		public string Password { get; set; }
		public string BtnSignin { get; set; }
		public string GetVolume { get; set; }
		public string TextAreaRefresh { get; set; }
		public string BtnGetSearchRefresh { get; set; }
		public string TextArea { get; set; }
		public string BtnGetSearch { get; set; }
		public string TableName { get; set; }
		public string TableInfo { get; set; }
		public Xpath(string path)
		{
			//Directory.GetCurrentDirectory() + @"\Xpath\Xpath.json"
			var jOb = JObject.Parse(File.ReadAllText(path));
			string namePartner = ConfigurationManager.AppSettings["Partner"].ToString();
			JArray xpaths = (JArray)jOb[namePartner];
			var _xpaths = xpaths.ToObject<List<Xpath>>();
			this.Name = _xpaths[0].Name;
			this.EmailAccount = _xpaths[0].EmailAccount;
			this.PasswordAccount = _xpaths[0].PasswordAccount;
			this.Signin = _xpaths[0].Signin;
			this.Username = _xpaths[0].Username;
			this.Next = _xpaths[0].Next;
			this.Password = _xpaths[0].Password;
			this.BtnSignin = _xpaths[0].BtnSignin;
			this.GetVolume = _xpaths[0].GetVolume;
			this.TextAreaRefresh = _xpaths[0].TextAreaRefresh;
			this.BtnGetSearchRefresh = _xpaths[0].BtnGetSearchRefresh;
			this.TextArea = _xpaths[0].TextArea;
			this.BtnGetSearch = _xpaths[0].BtnGetSearch;
			this.TableName = _xpaths[0].TableName;
			this.TableInfo = _xpaths[0].TableInfo;
		}
		public Xpath()
		{

		}
	}
}
