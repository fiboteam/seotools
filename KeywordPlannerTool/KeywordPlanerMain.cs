﻿using KeywordPlannerTool.Partner;
using SEOServices;
using SEOServices.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Utility;

namespace KeywordPlannerTool
{
	class KeywordPlanerMain
	{
		private readonly KeywordPlannerService _keywordPlannerService = new KeywordPlannerService();

		private static KeywordPlanerMain _keywordPlaner = null;
		private GoogleKeywordPlanner _googleKeywordPlaner = null;
		private string function = null;
		private System.Timers.Timer _timer;
		private BackgroundWorker _bgwAutoRefresh;
		private DateTime SaveLogNextTime;
		static void Main(string[] args)
		{
			_keywordPlaner = new KeywordPlanerMain();
			_keywordPlaner.Run();

			//Console.
		}

		public KeywordPlanerMain()
		{
			AppDomain.CurrentDomain.ProcessExit += new EventHandler(ProcessExit);
			_googleKeywordPlaner = new GoogleKeywordPlanner("https://adwords.google.com/KeywordPlanner", "https://adwords.google.com/KeywordPlanner");
			//_googleKeywordPlaner.GoToUrl("https://adwords.google.com/KeywordPlanner");
			function = "first";
			_timer = new System.Timers.Timer();
			_timer.Interval = 60000;
			_timer.Enabled = false;
			_timer.Elapsed += new ElapsedEventHandler(Timer_AutoRefresh);

			_bgwAutoRefresh = new BackgroundWorker();
			_bgwAutoRefresh.DoWork += new DoWorkEventHandler(bgwAutoRefresh_DoWork);

			SaveLogNextTime = new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0);
		}

		private void Timer_AutoRefresh(object sender, ElapsedEventArgs e)
		{
			if (DateTime.Now > SaveLogNextTime)
			{
				SaveLogNextTime = new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0);
				Console.Clear();
			}

			Console.WriteLine("Auto refresh...");

			if(!_bgwAutoRefresh.IsBusy)
			{
				_bgwAutoRefresh.RunWorkerAsync();
			}
			else
			{
				Console.WriteLine("Refreshing...");
			}
		}


		private void bgwAutoRefresh_DoWork(object sender, DoWorkEventArgs e)
		{
			SearchVolumeDataAndTrends();
		}

		void ProcessExit(object sender, EventArgs e)
		{
			_googleKeywordPlaner.Exit();
		}

		public void Run()
		{
			while (function != null && !function.Equals("0"))
			{
				if(!_timer.Enabled)
				{
					function = ChooseFunction();
					switch (function)
					{
						case "1":
							if (_googleKeywordPlaner.Login())
							{
								Console.WriteLine("\tLogin success");
							}
							else
							{
								Console.WriteLine("\tLogin fail");
							}
							break;
						case "2":
							_googleKeywordPlaner.Logout();
							break;
						case "3":
							SearchVolumeDataAndTrends();
							break;
						case "4":
							_googleKeywordPlaner.SearchVolumeDataAndTrendsWithoutRefreshPage(new List<string>() { "vay tieu dung", "cho vay tieu dung", "vay tiêu dùng", "vay tiêu dùng cá nhân", "vay von tieu dung", "cho vay tiêu dùng", "cho vay mua ô tô", "vay mua xe tra gop", "cho vay mua xe oto", "vay tien mua oto", "mua chung cư", "vay von mua nha", "lai suat vay mua nha", "phau thuat tham my mui", "tham my vien tphcm", "phau thuat tham my mat", "phẫu thuật thẩm mỹ mắt", "phẫu thuật thẩm mỹ mũi", "thẩm mỹ viện tphcm", "phau thuat tham khuon mat", "phẫu thuật thẩm khuôn mặt", "phau thuat tham my nhat ban", "phẫu thuật thẩm mỹ nhật bản" });
							break;
						case "5":
							_timer.Enabled = true;
							break;
					}
				}
				
			}
		}

		public string ChooseFunction()
		{
			try
			{
				string function = null;
				int index = 1;
				Console.WriteLine("\n**********Welcome to Google Keyword Planner API**********\n");
				Console.WriteLine(string.Format("{0}. Login.",index));
				index++;
				Console.WriteLine(string.Format("{0}. Logout.", index));
				index++;
				Console.WriteLine(string.Format("{0}. Get search volume data and trends from list keywords.", index));
				index++;
				Console.WriteLine(string.Format("{0}. Get search volume data and trends from list keywords Without Refresh Page.", index));
				index++;
				Console.WriteLine(string.Format("{0}. Auto Refresh.", index));
				index++;
				Console.WriteLine("0. Exit.");
				Console.Write("\tChoose function: ");
				function = Console.ReadLine();
				return function;
			}
			catch
			{
				return null;
			}
		}

		private bool SearchVolumeDataAndTrends()
		{
			try
			{
				List<string> listKeywordName = GetAllKeywordPlannerNameToRefresh();
				if (listKeywordName != null && listKeywordName.Count>0)
				{
					Console.WriteLine(string.Format("Total {0} keywords planner.", listKeywordName.Count));
					//List<string> listKeywordName = listKeyword.Select(keyword=>keyword.KeywordName).ToList();
					int part = int.Parse(ConfigurationManager.AppSettings["Part"].ToString());
					if (listKeywordName.Count > part)
					{
						//chia ra mỗi part
						int start = 0;
						int count = part;
						///lần đầu lấy data
						bool first = true;
						while(start<listKeywordName.Count)
						{
							Console.WriteLine(string.Format("Index from {0} and count {1}", start,count));
							var subList = listKeywordName.GetRange(start, count);

							if (first)
							{
								var resultList = _googleKeywordPlaner.SearchVolumeDataAndTrends(subList);
								if(resultList!=null)
								{
									Task.Factory.StartNew(()=>{
										UpdateKeywordPlannerAfterRefresh(resultList);
									});
								}
								first = false;
							}
							else
							{
								var resultList = _googleKeywordPlaner.SearchVolumeDataAndTrendsWithoutRefreshPage(subList);
								if(resultList!=null)
								{
									Task.Factory.StartNew(()=>{
										UpdateKeywordPlannerAfterRefresh(resultList);
									});
								}
							}

							start += count;
							count = (listKeywordName.Count - start < part) ? listKeywordName.Count - start : part;

							Thread.Sleep(int.Parse(ConfigurationManager.AppSettings["DelayPart"].ToString()));
						}
						
					}
					else
					{
						//không cần chia ra
						var resultList = _googleKeywordPlaner.SearchVolumeDataAndTrends(listKeywordName);
						if (resultList != null)
						{
							Task.Factory.StartNew(() =>
							{
								UpdateKeywordPlannerAfterRefresh(resultList);
							});
						}
					}
					
				}
				else
				{
					Console.WriteLine(string.Format("Not have any keyword planner.", listKeywordName.Count));
				}
				//_googleKeywordPlaner.SearchVolumeDataAndTrends(new List<string>() { "vay tieu dung", "cho vay tieu dung", "vay tiêu dùng", "vay tiêu dùng cá nhân", "vay von tieu dung", "cho vay tiêu dùng", "cho vay mua ô tô", "vay mua xe tra gop", "cho vay mua xe oto", "vay tien mua oto" });
				
				return true;
			}
			catch
			{
				return false;
			}
		}

		private List<string> GetAllKeywordPlannerNameToRefresh()
		{
			//List<KeywordPlannerViewModel> list=
			return _keywordPlannerService.GetAllKeywordPlannerNameToRefresh();
		}

		private void UpdateKeywordPlannerAfterRefresh(List<KeywordPlannerViewModel> list)
		{
			ParallelOptions options = new ParallelOptions();
			options.MaxDegreeOfParallelism = 2;
			Parallel.For(0, list.Count, options, i =>
			{
				list[i].CreateValueFromDataRefresh();
				if(_keywordPlannerService.UpdateFromRefresh(list[i]))
				{
					Console.WriteLine(string.Format("Update success {0}", list[i].KeywordName));
				}
				else
				{
					string param=string.Format("@keywordName={0} , @AvgMonthlySearches= {1}, @Competition = {2},\n @SuggestedBid = {3}, @FormatCurrency = {4}, @CompetitionIndex={5}, @ClickRate={6}, @TotalCostPerMonth={7}, @SEOFibo={8}, @ChargesForMaintenance={9}, @Setup={10},\n @HardWorkIndex={11}, @CompetitionInt={12}, @SetupCost={13},\n @KeywordPlanerStatus={14}", list[i].KeywordName, list[i].AvgMonthlySearches, list[i].Competition, list[i].SuggestedBid, list[i].FormatCurrency, list[i].CompetitionIndex, list[i].ClickRate, list[i].TotalCostPerMonth,
					list[i].SEOFibo, list[i].ChargesForMaintenance, list[i].Setup, list[i].HardWorkIndex, list[i].CompetitionInt, list[i].SetupCost, (short)list[i].KeywordPlanerStatus);
					
					Console.WriteLine(string.Format("Update fail {0} with param:\n", list[i].KeywordName));
					Console.WriteLine(param);
				}

				//Console.WriteLine(string.Format("Updated {0}-{1}-{2}-{3}-{4}", list[i].KeywordName, list[i].AvgMonthlySearches, list[i].Competition, list[i].SuggestedBid, list[i].FormatCurrency));
			});
			
		}

	}
}
