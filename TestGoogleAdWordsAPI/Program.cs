﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201601;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGoogleAdWordsAPI
{
	class Program
	{
		static void Main(string[] args)
		{
			AdWordsUser user = new AdWordsUser();
			// Get the TargetingIdeaService.

			TargetingIdeaService targetingIdeaService =
				(TargetingIdeaService)user.GetService(AdWordsService.v201601.TargetingIdeaService);

			// Create selector.
			/*
			First, prepare the TargetingIdeaSelector with parameters that specify this request is to retrieve keyword ideas:
			*/
			TargetingIdeaSelector selector = new TargetingIdeaSelector();
			selector.requestType = RequestType.STATS;
			selector.ideaType=IdeaType.KEYWORD;
			/*
			 Next, select the attributes that must be retrieved. Attributes can be thought of as individual columns/fields that are related to the keyword:
			 */
			selector.requestedAttributeTypes = new AttributeType[] {
				AttributeType.AVERAGE_CPC,
				AttributeType.COMPETITION,
				AttributeType.SEARCH_VOLUME
				//AttributeType.CATEGORY_PRODUCTS_AND_SERVICES,
				//AttributeType.EXTRACTED_FROM_WEBPAGE,
				//AttributeType.IDEA_TYPE,
				//AttributeType.KEYWORD_TEXT,
				//AttributeType.TARGETED_MONTHLY_SEARCHES,
				//AttributeType.UNKNOWN
			};

			// Language setting (optional).
			// The ID can be found in the documentation:
			//   https://developers.google.com/adwords/api/docs/appendix/languagecodes
			// Note: As of v201302, only a single language parameter is allowed.
			LanguageSearchParameter languageParameter = new LanguageSearchParameter();
			Language vietnamese = new Language();
			vietnamese.id = 1040;
			languageParameter.languages = new Language[] { vietnamese };

			// Create related to query search parameter.
			/*
			 Finally, use the RelatedToQuerySearchParameter to specify a list of seed keywords from which to generate new ideas:
			 */
			RelatedToQuerySearchParameter relatedToQuerySearchParameter =
				new RelatedToQuerySearchParameter();
			relatedToQuerySearchParameter.queries = new String[] { "vay tieu dung" };

			

			// Set selector paging (required for targeting idea service).
			Paging paging = Paging.Default;

			paging.startIndex = 0;

			paging.numberResults = 800;

			selector.paging = paging;


			TargetingIdeaPage page = new TargetingIdeaPage();

			selector.searchParameters =
			  new SearchParameter[] { relatedToQuerySearchParameter, languageParameter, };

			try
			{
				int i = 0;
				do
				{
					// Get related keywords.
					page = targetingIdeaService.get(selector);

					// Display related keywords.
					if (page.entries != null && page.entries.Length > 0)
					{
						foreach (TargetingIdea targetingIdea in page.entries)
						{
							string keyword = null;
							string categories = null;
							long averageMonthlySearches = 0;
							string average_cpc=null;
							double competition=0;
			

							foreach (Type_AttributeMapEntry entry in targetingIdea.data)
							{
								if (entry.key == AttributeType.AVERAGE_CPC)
								{
									average_cpc = (entry.value as MoneyAttribute).value.ToString();
								}

								if (entry.key == AttributeType.COMPETITION)
								{
									competition = (entry.value as DoubleAttribute).value;
								}

								if (entry.key == AttributeType.SEARCH_VOLUME)
								{
									averageMonthlySearches = (entry.value as LongAttribute).value;
								}

								if (entry.key == AttributeType.KEYWORD_TEXT)
								{
									keyword = (entry.value as StringAttribute).value;
								}

								if (entry.key == AttributeType.CATEGORY_PRODUCTS_AND_SERVICES)
								{
									IntegerSetAttribute categorySet = entry.value as IntegerSetAttribute;
									StringBuilder builder = new StringBuilder();
									if (categorySet.value != null)
									{
										foreach (int value in categorySet.value)
										{
											builder.AppendFormat("{0}, ", value);
										}
										categories = builder.ToString().Trim(new char[] { ',', ' ' });
									}
								}
							
							}
							Console.WriteLine("Keyword with text '{0}', and average monthly search volume " +
								"'{1}' was found with categories: {2}", keyword, averageMonthlySearches,
								categories);
							i++;
						}
					}
					selector.paging.IncreaseOffset();
				} while (selector.paging.startIndex < page.totalNumEntries);
				Console.WriteLine("Number of related keywords found: {0}", page.totalNumEntries);
			}
			catch (Exception e)
			{
				//throw new System.ApplicationException("Failed to retrieve related keywords.", e);
				Console.WriteLine("Exception {0}", e.Message);
			}

			Console.ReadKey();
		}

	}
}
